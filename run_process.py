#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no is_file member"
# for 'Path([...]).is_file()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member

from Eye_tracking.Smooth_pursuit.compute_correction_cross import compute_correction_cross
from Eye_tracking.Smooth_pursuit.plot_movie_data import plot_movie_data
import Production
import Response_time_value


def run_process(to_run, module_name, function_name, params):
# def run_process(to_run):
    """Perform data analysis step by step

    Args:
        to_run (str): abbreviation of the step to perform among:
                #########################################################################
                ##########################   Preprocess data   ##########################
                #########################################################################
                'X2T': Tobiixlsx2NCtsv
                'CNT': TobiiNCtsv2tsv

                #########################################################################
                ###########################   Eye_tracking   ############################
                #########################################################################
                'T2A': Tobiitsv2arff
                'CVS': compute_validity_samples
                'SVS': summarise_validity_samples
                'PS': plot_samples
                'ACC': assess_correction_cross
                'CCC': compute_correction_cross
                'PMD': plot_movie_data
                'CTA': convert Tobii AOI export

                #########################################################################
                ########################   Response_time_value   ########################
                #########################################################################
                'RTV': response_time_value

                #########################################################################
                ############################   Production   #############################
                #########################################################################
                'CUR': create_unproduced_report
                'CPT': correct_production_transcription
                'RPI': rename_production_itemid
        module_name (str): name of the module
        function_name (str): name of the function
        params (dict): parameters of the function to run
    """
    # import importlib
    import config as cfg

    script_start = cfg.datetime.now()
    print("Start time: " + str(script_start))

    if to_run == 'X2T':
        from common_functions import Tobiixlsx2NCtsv as x2t
        # https://stackoverflow.com/questions/21986194/
        # how-to-pass-dictionary-items-as-function-arguments-in-python
        x2t.Tobiixlsx2NCtsv(**params)
    # https://stackoverflow.com/questions/8790003/dynamically-import-a-method-in-a-file-from-a-string/8790051#8790051
    # https://stackoverflow.com/questions/34794634/how-to-use-a-variable-as-function-name-in-python
    # https://stackoverflow.com/questions/13184281/python-dynamic-function-creation-with-custom-names
    # https://stackoverflow.com/questions/4534438/typeerror-module-object-is-not-callable
        # function = importlib.import_module(module_name + '.' + function_name) # OK
        # function.Tobiixlsx2NCtsv(params['in_folder'],
        #         params['out_folder'],
        #         params['apply_filter']
        # )
        # globals()['Tobiixlsx2NCtsv']()
        # function(params['in_folder'],
        #         params['out_folder'],
        #         params['apply_filter']
        # )


    elif to_run == 'CNT':
        from common_functions import TobiiNCtsv2tsv as cnt
        cnt.TobiiNCtsv2tsv(**params)
    elif to_run == 'T2A':
        from Eye_tracking.Smooth_pursuit import Tobiitsv2arff as t2a
        t2a.Tobiitsv2arff(**params)
        # t2a.Tobiitsv2arff('0c_Compliant_Tobiitsv',
        #                   '3_Valid_FixCross_arff', True, '', 'TargetFadeEnd',
        #                   'Data_Sample-Cat (DC1-DC2)_alltasks-detailed/' \
        #                   + 'TargetFixCrossStart_TargetFixCrossEnd.tsv',
        #                   False)
    elif to_run == 'CVS':
        from Eye_tracking.Smooth_pursuit import compute_validity_samples as cvs
        cvs.compute_validity_samples(**params)
    elif to_run == 'SVS':
        from Eye_tracking.Smooth_pursuit import summarise_validity_samples as svs
        svs.summarise_validity_samples(**params)
    elif to_run == 'PS':
        from Eye_tracking.Smooth_pursuit import plot_samples as ps
        ps.plot_samples('0c_Compliant_Tobiitsv', \
                        'reports/Plot_samples',
                        ['MovieStart', 'MovieEnd'],
                        'RecordingName')
    elif to_run == 'ACC':
        from Eye_tracking.Smooth_pursuit import assess_correction_cross as ass
        # ass.assess_correction_cross('Labelled-ad-ICLC_min_pts=112', 'MediaName')
        ass.assess_correction_cross(**params)
    # The following functions have just been run on a sample (hence the subfolder
    # and/or participant and media)
    elif to_run == 'CCC':
        from Eye_tracking.Smooth_pursuit import compute_correction_cross as ccc
        # ccc.compute_correction_cross(base_dir, 'DC1-DC2', 'Prod', subfolder,
        #                              'Stimuli_timings.tsv', 'item')
        # ccc.compute_correction_cross('Labelled-ad-ICLC_min_pts=112',
        #                              'out_correction-cross_2019-07-23_111153.tsv',
        #                              'RecordingName', 'global', 'median')
        ccc.compute_correction_cross(**params)
    elif to_run == 'PMD':
        from Eye_tracking.Smooth_pursuit import plot_movie_data as pmd
        # pmd.plot_movie_data(base_dir, 'DC1-DC2', 'Prod', subfolder, corrected_dir,
        #                     'Ead01_DC2', 'P_B1-3_RL', True, 0.5, 0.5, 0)
        # pmd.plot_movie_data(subfolder, corrected_dir,'RecordingName',
        #                     'Ead03_DC1', media_list, True, False, 0.5, 0.5, 0)
        pmd.plot_movie_data(**params)
        # plot_movie_data()
    elif to_run == 'RST':
        from Eye_tracking.Smooth_pursuit import run_sp_tool as rst
        rst.run_sp_tool(**params)
    elif to_run == 'CTA':
        from Eye_tracking.Tobii_AOI import convert_tobii_aoi as cta
        cta.convert_tobii_aoi(**params)
    elif to_run == 'RTV':
        from Response_time_value import response_time_value as rtv
        # rtv.response_time_value('Data/Debug/0c_Compliant_Tobiitsv',
        rtv.response_time_value(**params)
    elif to_run == 'CUR':
        from Production import create_unproduced_report as cur
        cur.get_unproduced_items('Eye-tracking/0c_Compliant_Tobiitsv',
                                'Production/reports/Unproduced_items')
        cur.create_unproduced_report()
    elif to_run == 'CPT':
        from Production import correct_production_transcription as cpt
        # cpt.correct_cod_syntax('Production')
        # cpt.count_missing_itemid('Production', \
        #         '2021-07-15_183613_Cat (DC1-DC2)_unproduced.tsv')
        # cpt.insert_delete_itemid('Production', \
        #         'Insert_missing_itemID.csv', \
        #         'Delete_duplicate_itemID.csv')
        # cpt.insert_training_itemid('Production')
        # cpt.insert_transcription_itemid('Production', \
        #         'Cat_transcription-codes.txt', \
        #         'Mem_transcription-codes.txt')
    elif to_run == 'RPI':
        from Production import rename_production_itemid as rpi
        # rpi.rename_itemid('Production', \
        #         'Cat_Tobii-export_item-order_v2.txt', \
        #         'Mem_Tobii-export_item-order.txt')
        # rpi.check_renamed_itemid('Production', 'manner_and_path', True)
        rpi.get_transcribed_files('Production', 'bon')
    else:
        print("Please enter a valid name to run")

    if cfg.save_logs:
        from common_functions import save_files

        cfg.out_logs_path.mkdir(parents=True, exist_ok=True)

        save_files.save_log('config', cfg.out_logs_path, script_start)
        save_files.save_log('run_process', cfg.out_logs_path, script_start, to_run) # script_start, start_time

    # Play a beep at the end of execution (Win / MacOS)
    # https://stackoverflow.com/questions/42150309/
    # how-to-make-a-sound-in-osx-using-python-3/42150378#42150378
    try:
        # https://stackoverflow.com/questions/16573051/sound-alarm-when-code-finishes
        import winsound
    except ImportError:
        import os
        os.system('afplay /System/Library/Sounds/Sosumi.aiff')
        # print ('/a')
        # curses.beep()
    else:
        winsound.Beep(500, 500)

# subfolder = 'Labelled-ad-ICLC_min_pts=112'
# corrected_dir = 'out_correction-cross_2019-07-30_151741_global_mean'
# corrected_dir = 'out_correction-cross_2019-07-30_152200_global_median'

# run_process('X2T', 'common_functions', 'Tobiixlsx2NCtsv', {
#     'in_folder': '0a_Exports_Tobiixlsx',
#     'out_folder': '0b_Converted_Tobiixlsx2NCtsv',
#     'apply_filter': False
# })

# run_process('CNT', 'common_functions', 'TobiiNCtsv2tsv', {
#     'in_folder': '0b_Converted_Tobiixlsx2NCtsv',
#     'out_folder': '0c_Compliant_Tobiitsv',
#     'apply_filter': False
# })

# run_process('T2A', 'Eye_tracking.Smooth_pursuit', 'Tobiitsv2arff', {
#     'in_folder': '0c_Compliant_Tobiitsv',
#     'out_folder': '1_Converted_Tobiitsv2arff',
#     'apply_filters': False,
#     'export_type': 'RecordingName',
#     'end_limit': 'MovieEnd',
#     'validity_samples': None,
#     'out_plots': True
# })

# run_process('CVS', 'Eye_tracking.Smooth_pursuit', 'compute_validity_samples', {
#     'in_folder': '0c_Compliant_Tobiitsv',
#     'out_folder': 'reports/Validity_samples',
#     'apply_filters': False,
#     'limits': 'Movie',
#     'output': 'detailed'
# })

# run_process('CVS', 'Eye_tracking.Smooth_pursuit', 'compute_validity_samples', {
#     'in_folder': '0c_Compliant_Tobiitsv',
#     'out_folder': 'reports/Validity_samples',
#     'apply_filters': False,
#     'limits': 'ChoiceAction',
#     'output': 'detailed'
# })

# run_process('CVS', 'Eye_tracking.Smooth_pursuit', 'compute_validity_samples', {
#     'in_folder': '0c_Compliant_Tobiitsv',
#     'out_folder': 'reports/Validity_samples',
#     'apply_filters': False,
#     'limits': 'ChoiceFixCross',
#     'output': 'detailed'
# })

# run_process('CVS', 'Eye_tracking.Smooth_pursuit', 'compute_validity_samples', {
#     'in_folder': '0c_Compliant_Tobiitsv',
#     'out_folder': 'reports/Validity_samples',
#     'apply_filters': False,
#     'limits': 'TargetAction',
#     'output': 'detailed'
# })

# run_process('CVS', 'Eye_tracking.Smooth_pursuit', 'compute_validity_samples', {
#     'in_folder': '0c_Compliant_Tobiitsv',
#     'out_folder': 'reports/Validity_samples',
#     'apply_filters': False,
#     'limits': 'TargetFixCross',
#     'output': 'detailed'
# })

# run_process('SVS', 'Eye_tracking.Smooth_pursuit', 'compute_validity_samples', {
#     'in_folder': 'reports/Validity_samples'
# })

run_process('RST', 'Eye_tracking.Smooth_pursuit', 'run_sp_tool', {
        'in_folder': '1_Converted_Tobiitsv2arff_MediaName_MovieEnd',
        'out_folder': '2_Classified_sp_tool_raw_arff_MediaName_MovieEnd'
})

# run_process('ACC', 'Eye_tracking.Smooth_pursuit', 'assess_correction_cross', {
#         'in_folder': '2_Classified_sp_tool_raw_arff_MediaName_MovieEnd',
#         'out_folder': 'reports/Assess_correction'
# })

# run_process('CCC', 'Eye_tracking.Smooth_pursuit', 'compute_correction_cross', {
#         'in_folder': '2_Classified_sp_tool_raw_arff_MediaName_MovieEnd',
#         'out_folder': '3_Corrected_arff',
#         'export_type': '',
#         'mean_level': 'global',
#         'central_tendency': 'median',
#         'correction_file_timestamp': '2022-09-04_171153'
# })

# TODO
# run_process('PMD', 'Eye_tracking.Smooth_pursuit', 'plot_movie_data', {
        
# })
# {'raw': '1_Converted_Tobiitsv2arff_MovieEnd',
# # Data/Sample/1_Converted_Tobiitsv2arff
# # 'corrected': 'Data/Sample/4_Corrected_arff',
# 'corrected': '3_Corrected_arff',
# # 'subfolders': ['2020-09-18_180952_global_median',
# #                '2020-09-24_180952_global_median']},
# #  'subfolders': ['2020-12-09_112242_block_median']},
# #  'subfolders': ['2020-12-09_112242_block_median',
# #         '2020-12-24_081441_global_median']},
# 'subfolders': ['2020-12-09_140649_block_median',
#         '2020-12-24_102436_global_median']},
# # ['2020-09-18_180952_global_median',
# # '2020-09-24_180952_global_median']
# 'reports/Plot_movie_data',
# True,
# {'ref': (1, 0, 0),
# # '2020-12-09_112242_block_median': (0, 0, 1),
# # '2020-12-24_081441_global_median': (1, 1, 0)},
# '2020-12-09_140649_block_median': (0, 0, 1),
# '2020-12-24_102436_global_median': (1, 1, 0)},
# # {'file': [True, 1.0], 'show': [True, 1.0], 'duration': 0})
# # for ProdMem:
# {'file': [True, 1.0], 'show': [True, 1.0], 'duration': 126}


# run_process('CTA', 'Eye_tracking.Tobii_AOI', 'convert_tobii_aoi', {
#     'in_folder': '0_Exports_Tobii_AOI',
#     'out_folder': '1_Converted_Tobii_AOI2tsv'
# })


#########################################################################
########################   Response_time_value   ########################
#########################################################################

# run_process('RTV', 'Response_time_value', 'response_time_value', {
#         'in_folder': '0c_Compliant_Tobiitsv',
#         'out_folder': 'reports/Response_time_value'
# })


#########################################################################
############################   Production   #############################
#########################################################################
