#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no is_file member"
# for 'Path([...]).is_file()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member

from pathlib import Path
import pandas as pd

from config import base_dir, expe_task, print_logs, save_logs
from common_functions.common_functions import print_save_log, keep_interval_data
from common_functions.load_files import list_files, timings2df, Tobiitsv2df

def compute_validity_samples(in_folder='0c_Compliant_Tobiitsv',
                             out_folder='reports/Validity_samples',
                             apply_filters=False,
                             limits='Movie',
                             output='detailed'):
    """Create .tsv files summarizing the number of valid 'Gaze Data'
            in 'reports/Validity_samples' for .tsv files containing
            data for Cat or Mem experiment.
            Valid 'Gaze Data': ValidityLeft < 2 | ValidityRight < 2
            Generate one file per 'output' level

    Args:
        in_folder (str, optional): folder where are the .tsv files to
                process.
        out_folder (str, optional): folder where the resulting .tsv
                report files are stored (in /expe/*task*/ subfolders).
                Defaults to 'reports/Validity_samples'.
        apply_filters (bool, optional): True to process only the files
                and data specified in filter_dict (in config.py).
                Defaults to False.
        limits (str, optional): beginning of the column name of the
                stimuli timings file containing the times when to start
                and end the analysis.
                'Movie', 'TargetFixCross', 'TargetAction',
                'ChoiceFixCross' or 'ChoiceAction'.
                Defaults to 'Movie'.
        output (str, optional): 'detailed' (one file with one number
                of valid 'Gaze Data' per item per participant)
                or 'global' (one file with one global number
                of valid 'Gaze Data' per participant)
                or 'separate' (one file per participant with one number
                of valid 'Gaze Data' per item).
                Defaults to 'detailed'.
    """
    # Load the.tsv file containing event times by media
    df_timings = timings2df()

    # Create an empty list to store all data for 'detailed' and 'global'
    if (output == 'detailed') or (output == 'global'):
        all_data = []

    # List the .tsv or .xlsx files to process after checking arguments 
    # and assigning default filter values
    filenames = list_files(in_folder, '.tsv', apply_filters)

    # Browse the files listed in filenames
    for filename in filenames:
        # Print message for files not compatible with the defined limits
        if ((limits == 'TargetFixCross' or limits == 'TargetAction')
                and 'Phase2' in filename.parent.name) \
                or ((limits == 'ChoiceFixCross' \
                or limits == 'ChoiceAction')
                and not ('Cat' in filename.parent.name \
                or 'Phase2' in filename.parent.name)):
            reminder = "Reminder: " \
                + "There is no '{}' for {}. ".format(limits, filename)
            print_save_log(reminder, Path(__file__).stem, \
                    print_logs, save_logs)
        # Process files compatible with the defined limits
        else:
            # # Get the experiment name based on expe_task or filename
            # if apply_filters:
            #     include_expe = expe_task['include_expe']
            # else:
            #     include_expe = filename.parts[-3]

            # Define the subdirectory where the output file(s) are stored
            data_folder = in_folder.replace('/', '_') \
                    .split('_0c_Compliant_Tobiitsv')[0]            
            out_path = base_dir.joinpath(out_folder, data_folder \
                    + ('_' + expe_task['include_task'] \
                    if (apply_filters and expe_task['include_task']) \
                    else '_alltasks') + '_' + output)

            # Load data from the current file
            data = Tobiitsv2df(filename)

            # Get media names
            media_names = data.loc[data['StudioEvent'] \
                    == 'MovieStart', 'MediaName']

            for media_name in media_names:
                # Define limits of the time interval
                start_limit = limits + 'Start'
                end_limit = limits + 'End'
                # Keep data from the defined interval
                interval_data, _, _ = keep_interval_data(data, media_name,
                        df_timings, start_limit, end_limit)

                valid_gaze = interval_data[(interval_data['ValidityLeft'] < 2)
                        | (interval_data['ValidityRight'] < 2)]

                # Compute the percentage of valid samples on the kept samples
                # if the interval exists. It doesn't exist for, for example:
                # 'Ech01_A1_DC1' 'NVCat_B2-11C_TMTDTM' 'ChoiceFixCross'
                if len(interval_data.index) != 0:
                    valid_samples = str(int(round(len(valid_gaze.index)
                            / len(interval_data.index) * 100, 0)))
                else:
                    valid_samples = ''
                    error_interval = "There is no '{}' in '{}' " \
                            .format(limits, media_name) \
                            + "for {}".format(filename)
                    print_save_log(error_interval, Path(__file__).stem, \
                            print_logs, save_logs)

                # Retrieve studio test name and recording name to use in the
                # results file
                studio_test_name = data['StudioTestName'].iloc[0]
                recording_name = data['RecordingName'].iloc[0]

                if (output == 'detailed'):
                    all_data.append((studio_test_name, recording_name,
                            media_name, valid_samples, len(valid_gaze.index),
                            len(interval_data.index)))
                    cols = ['StudioTestName', 'RecordingName', 'MediaName',
                            'PercentValidSamples', 'NbValidSamples',
                            'NbTotalSamples']
                    all_data_df = pd.DataFrame(all_data, columns=cols)
                    # Order rows by RecordingName (this is the simplest method
                    # with pandas version 0.22)
                    all_data_df['index'] = all_data_df.index
                    all_data_df_output = all_data_df.sort_values(
                            by=['RecordingName', 'index'])
                    all_data_df_output = all_data_df_output.drop(['index'],
                            axis=1)
                elif (output == 'global'):
                    all_data.append((recording_name, len(valid_gaze.index),
                            len(interval_data.index)))
                    cols = ['RecordingName', 'NbValidSamples',
                            'NbTotalSamples']
                    all_data_df = pd.DataFrame(all_data, columns=cols)
                    all_data_df_output = all_data_df.groupby('RecordingName') \
                            .sum()[['NbValidSamples', 'NbTotalSamples']]
                    # Add new column after recording for percentage
                    all_data_df_output.insert(loc=0,
                            column='PercentValidSamples', value='')
                    all_data_df_output['PercentValidSamples'] = (
                            all_data_df_output['NbValidSamples']
                            / all_data_df_output['NbTotalSamples'] * 100) \
                            .round().astype(int)
                elif (output == 'separate'):
                    # Create the subdirectory where the output files are stored
                    out_path.mkdir(parents=True, exist_ok=True)

                    # If it is a new output file, add the header
                    out_filename = filename.stem + '_' + start_limit \
                            + '-' + end_limit + '.tsv'
                    if not out_path.joinpath(out_filename).is_file():
                        with open(out_path.joinpath(out_filename),
                            'w', encoding='utf8') as out_file:
                            out_file.write('StudioTestName\tRecordingName'
                                + '\tMediaName\tPercentValidSamples'
                                + '\tNbValidSamples\tNbTotalSamples\n')
                    # Add data to the output file
                    with open(out_path.joinpath(out_filename),
                            'a', encoding='utf8') as out_file:
                        out_file.write(studio_test_name + '\t' 
                            + recording_name + '\t' + media_name
                            + '\t' + valid_samples + '\t'
                            + str(len(valid_gaze.index)) + '\t' 
                            + str(len(interval_data.index)) + '\n')

            if (output == 'detailed') or (output == 'global'):
                # Create the subdirectory where the output file is stored
                out_path.mkdir(parents=True, exist_ok=True)

                # Write dataframe to .tsv file
                if (output == 'detailed'):
                    index = False
                if (output == 'global'):
                    index = True
                all_data_df_output.to_csv(out_path \
                        .joinpath(start_limit + '_' + end_limit + '.tsv'),
                        sep='\t', header=True, index=index, encoding='utf-8')
    