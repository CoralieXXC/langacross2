﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no 'mkdir' member"
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member

from pathlib import Path

from config import base_dir, expe_task, timestamp
from common_functions.common_functions  import get_import_types, keep_interval_data
from common_functions.load_files import timings2df, list_files, Tobiitsv2df
from Eye_tracking.Smooth_pursuit import plot_gazexy_time

def plot_samples(in_folder='0c_Compliant_Tobiitsv',
                 out_folder='reports/Plot_samples',
                 limits=['MovieStart', 'TargetFadeEnd'],
                 export_type='RecordingName'):
    """Plot as .png timestamped files x and y gaze positions
            (inferred by the eye-tracker) as a function of time.


    Args:
        in_folder (str, optional): folder where the .tsv data files are
                stored (in /expe/*task*/ subfolders).
                Recommended: '0_Exports_Tobiitsv' or
                '0c_Compliant_Tobiitsv'. Defaults to
                '0c_Compliant_Tobiitsv' (conversion, x,y not corrected).
        out_folder (str, optional): folder where the .png files are
                stored. Defaults to: 'reports/Plot_samples'.
        limits (list, optional): column names ['start', 'end'] of the
                stimuli timings file containing the times when to start
                and end the plots. Defaults to ['MovieStart',
                'TargetFadeEnd'].
        export_type (str, optional): 'MediaName' or 'RecordingName'.
                'RecordingName' is the participant name.
                'MediaName': exports will be in
                'MediaName/RecordingName.arff'.
                'RecordingName': exports will be in
                'RecordingName/MediaName.arff'.
                Defaults to 'RecordingName.
    """
    # TODO check import_type='MediaName'?

    # Load the.tsv file containing event times by media
    df_timings = timings2df()

    # List the .tsv or .xlsx files to process after checking arguments and
    # assigning default filter values
    filenames = list_files(in_folder, '.tsv', True)

    # Get (unused import_type and) import_subtype
    _, import_subtype = get_import_types(filenames)

    # Browse the files listed in filenames
    for filename in filenames:
        # Load data from the current file
        data = Tobiitsv2df(filename)

        # Get media names
        # TODO doesn't work for import_subtype = 'RecordingName'
        subnames = data.loc[data['StudioEvent'] == 'MovieStart', \
                import_subtype]

        # # Filter out the distractors
        # # TODO Keep?
        # if import_subtype == 'MediaName':
        #     subnames = [subname for subname in subnames \
        #             if not 'D_' in subname]
        # elif import_subtype == 'RecordingName':
        #     media_names = [filename.stem for filename in filenames]
        #     subnames = [subname for subname in media_names \
        #             if not 'D_' in subname]

        for subname in subnames:
            # Keep data from the defined interval
            interval_data, _, _ = keep_interval_data(data, subname, \
                    df_timings, limits[0], limits[1])

            gaze_x = interval_data['GazePointX (MCSpx)']
            gaze_y = interval_data['GazePointY (MCSpx)']

            # Get the array of sample times for the current trial
            time = interval_data['RecordingTimestamp']
            # Align time to zero
            time = time - time.min()//1

            # Create the folder that will contain the .arff files and get
            # the name of the .arff file
            if export_type == 'MediaName':
                subdirectory = interval_data['MediaName'].iloc[0] \
                        .replace('.avi', '')
            elif export_type == 'RecordingName':
                subdirectory = interval_data['RecordingName'].iloc[0]

            # Create the subdirectory where the output file(s) are stored
            out_path = base_dir.joinpath(out_folder, timestamp \
                    + '_' + expe_task['include_expe'] \
                    + '_' + expe_task['include_task'] \
                    + '_' + limits[0] + '-' + limits[1])
                    # + ('_' + task if task else '_alltasks')
            out_path.mkdir(parents=True, exist_ok=True)

            # Plot the sample traces for x and y gaze positions separately
            # for each eye using two sub plots.
            plot_gazexy_time.plot_gazexy_time(time, gaze_x, gaze_y, \
                    interval_data, out_path.joinpath(subdirectory), limits)
