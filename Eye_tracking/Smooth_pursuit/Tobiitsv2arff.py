#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To ignore numpy errors:
# pylint: disable=E1101

from pathlib import Path
import pandas as pd
import numpy as np
from numpy.lib.stride_tricks import as_strided as strided

from config import base_dir, timestamp, print_logs, save_logs
from common_functions.common_functions import get_import_types, get_export_type, \
        print_save_log, keep_interval_data, get_tsv_participant_media
from common_functions.load_files import list_files, filter_data, timings2df
from Eye_tracking.Smooth_pursuit.plot_gazexy_time import plot_gazexy_time
from common_functions.save_files import df2arff

# https://stackoverflow.com/questions/40659212/
# futurewarning-elementwise-comparison-failed-returning-scalar-but-in-the-futur
import warnings

warnings.simplefilter(action='error', category=FutureWarning)

def validity(row):
    """Adapt the Validity value of each gaze point for sp_tool
            Tobii: ok = 0, not ok >= 2; sp_tool: ok = 1, not ok = 0

    Args:
        row (DataFrame): data with columns containing
                two Tobii Validity values.

    Returns:
        val (int): one sp_tool Validity value
    """

    if (row['ValidityLeft'] >= 2) and (row['ValidityRight'] >= 2):
        val = 0
    else:
        val = 1
    return val

def mask_knans(df, min_gapsize_unfilled):
    """Create mask to apply interpolation on missing data (< ~80 ms) but
            not on blinks

    Args:
        df (DataFrame): eye-tracking data.
        min_gapsize_unfilled (int): gap that shouldn't be filled (sample
                number).

    Returns:
        result (ndarray): TODO 
    """

    df = np.asarray(df)
    k = df.size
    dfnan = np.append(np.isnan(df), [False] * (min_gapsize_unfilled - 1))
    result = np.empty(k, np.bool8)
    result.fill(True)

    dfstrides = dfnan.strides[0]
    i = np.where(strided(dfnan, (k + 1 - min_gapsize_unfilled,
            min_gapsize_unfilled), (dfstrides, dfstrides)).all(1))[0][:, None]
    i = i + np.arange(min_gapsize_unfilled)
    i = pd.unique(i[i < k])

    result[i] = False
    return result

def gaze_zero(row):
    """Set to 0 missing gaze points x, y and the corresponding
            'Validity sp_tool'

    Args:
        row (DataFrame): data to process

    Returns:
        val (tuple of (int, int, int)): (gaze_x, gaze_y, 'Validity sp_tool'):
                (0, 0, 0) for missing gaze points;
                (gaze_x, gaze_y, 1) otherwise
    """
    if ((row['Validity sp_tool'] == 0) or (pd.isna(row['GazePointX (MCSpx)'])
        and pd.isna(row['GazePointY (MCSpx)'])
        and row['Validity sp_tool'] == 1)):
        val = 0, 0, 0
    else:
        val = row['GazePointX (MCSpx)'], row['GazePointY (MCSpx)'], 1
    return val

def Tobiitsv2arff(in_folder='0c_Compliant_Tobiitsv',
                  out_folder='', apply_filters=False,
                  export_type='', end_limit='TargetFadeEnd',
                  validity_samples=None, out_plots=False):
    """Create separate .arff gaze data files from compliant Tobii .tsv
            and optionally .png images of gaze_x and gaze_y against time
 
    Args:
        in_folder (str, optional): folder where the .tsv data files are
                stored (in /expe/*task*/ subfolders).
                Defaults to '0c_Compliant_Tobiitsv'.
        out_folder (str, optional): folder where the resulting .arff
                data files are stored (in /expe/*task*/ subfolders).
                Recommended:
                '1_Converted_Tobiitsv2arff' if validity_samples is none.
                '2_Valid_FixCross_arff' if validity_samples is not none.
        apply_filters (bool, optional): True to process only the files
                and data specified in filter_dict (in config.py).
                Defaults to False.
                Recommended: True if validity_samples is not none.
        export_type (str, optional): 'MediaName' or 'RecordingName'.
                'RecordingName' is the participant name.
                'MediaName': exports will be in
                'MediaName/RecordingName.arff'.
                'RecordingName': exports will be in
                'RecordingName/MediaName.arff'.
                Defaults to import_type.
        end_limit (str, optional): name of the column of the
                stimuli_timings file containing the times when to end
                the analysis: 'MovieEnd'
                or 'TargetFixCrossEnd' or 'TargetActionStart'
                or 'TargetFadeStart' or 'TargetFadeEnd'
                or 'TargetActionEnd' or 'ChoiceFixCrossEnd'
                or 'ChoiceFadeStart' or 'ChoiceFadeEnd'
                or 'ChoiceLActionEnd' or 'ChoiceRActionEnd'.
                Defaults to 'TargetFadeEnd'.
        validity_samples (str, optional): .tsv file containing
                percentage validities per block/participant/item for
                each data set to correct, clean and convert to .arff
                Defaults to none if there is no validity to consider
                (converts the whole data set).
                TODO add
        out_plots (bool, optional): to export .png images of gaze_x and
                gaze_y against time.
                Defaults to False.

    Raises:
        SystemExit: [description]
        SystemExit: [description]
        SystemExit: [description]
    """
    # TODO check import_type='MediaName'?

    # Load the.tsv file containing event times by media
    df_timings = timings2df()

    # List the .tsv or .xlsx files to process
    filenames = list_files(in_folder, '.tsv', apply_filters)

    # Get import_type and import_subtype
    import_type, import_subtype = get_import_types(filenames)
    # Check and assign export_type value
    export_type = get_export_type(import_type, export_type)

    # Load in a DataFrame the .tsv file containing detailed validities
    # for each block/participant/item
    if validity_samples:
        tsv_validity = base_dir.joinpath('reports/Validity_samples',
                validity_samples)
        try:
            with open(tsv_validity) as tsvfile:
                validities = pd.read_csv(tsvfile, sep='\t')
        except IOError as strerror:
            print("I/O error: {0}".format(strerror))
    # Or create an empty DataFrame
    else:
        validities = pd.DataFrame(columns=[])

    # Browse the files listed in filenames
    for filename in filenames:
        # Load data from the .tsv or .xlsx files
        # https://stackoverflow.com/questions/24251219/
        # pandas-read-csv-low-memory-and-dtype-options
        dict_dtype = {'MediaName': str, 'MouseEvent': str, 'StudioEvent': str,
                'StudioEventData': str}
        with open(filename, 'r'):
            data = pd.read_csv(filename, sep='\t', header=0, encoding='UTF-8',
                               dtype=dict_dtype)

        # Assess if the participant data set is going to be processed
        # if apply_filters and import_type == 'RecordingName' \
        #         and not validity_samples:
        if apply_filters:
            participant_name = data['RecordingName'].iloc[0]
            process_file = filter_data(participant_name)
        # TODO elif apply_filters and import_type == 'MediaName'
        else: # elif not apply_filters:
            process_file = True

        # If the data pass the filter, process
        if process_file:
            # Get subnames: media names (for 'RecordingName')
            # or participant names (for 'MediaName')
            subnames = data.loc[data['StudioEvent']
                    == 'MovieStart', import_subtype]

            # Add a variable to store the order in the .arff files metadata
            stim_order = 1
            for subname in subnames:
                # Define media and participant names
                participant_name, media_name = get_tsv_participant_media(
                        import_type, filename, subname, data)
                media_avi = media_name + '.avi'
                # Rem: media_avi is the same as subname for 'RecordingName'

                # Assess if the media data set is going to be processed
                # if apply_filters and not validity_samples:
                if apply_filters:
                    process_media = filter_data(participant_name, media_name)
                else:
                    process_media = True

                # If the media passes the filter, process
                if process_media:
                    # Keep data from the defined interval
                    interval_data, start_time, _ = keep_interval_data(
                            data, media_avi, df_timings, 'MovieStart', 
                            end_limit)

                    # If validity, get the validity for this participant/item
                    if not validities.empty:
                        validities_samples = validities[
                                'PercentValidSamples'][(validities[
                                'RecordingName'] == participant_name) \
                                & (validities['MediaName'] == media_avi)]

                    # If exactly one percentage for validity, process the data:
                    if validities.empty \
                            or (len(validities_samples.index) == 1 \
                            and validities_samples.iloc[0] >= 75):
                        # Add a time column that starts at zero for 'MovieStart'
                        # and convert to microseconds
                        interval_data['RecordingTimestampZero'] = (
                                interval_data['RecordingTimestamp'] 
                                - start_time) * 1000

                        # Index on the time of each record to take into account
                        # in the interpolation the fact that the samples are
                        # not evenly spaced
                        interval_data.set_index('RecordingTimestampZero',
                                inplace=True)

                        # For a first run (converting only) get the proper
                        # validity (see validity function)
                        if validities.empty:
                            interval_data["Validity sp_tool"] = interval_data \
                                    .apply(validity, axis=1)
                        # For a second run (converting, cleaning and
                        # interpolating data) replace the data with np.nan for
                        # ValidityLeft>=2 and ValidityRight>=2 to remove the
                        # lines where, despite the poor quality, there is a
                        # value for 'GazePointX (MCSpx)' and
                        # 'GazePointY (MCSpx)'.
                        # cf 'Langacross2 Cat (DC1-DC2) ALL-2016-08-11 AOI 
                        # ALLgroups-complete_\DC1-3 _ DC2-2 Prod Expe 
                        # Block1_Fch02_A1_DC1.xlsx' for P_B1-12_MS media
                        # for example
                        else:
                            interval_data['GazePointX (MCSpx)'] \
                                    = interval_data['GazePointX (MCSpx)'] \
                                    .where(((interval_data['ValidityLeft'] < 2)
                                    | (interval_data['ValidityRight'] < 2)),
                                    other=np.nan)
                            interval_data['GazePointY (MCSpx)'] \
                                    = interval_data['GazePointY (MCSpx)'] \
                                    .where(((interval_data['ValidityLeft'] < 2)
                                    | (interval_data['ValidityRight'] < 2)),
                                    other=np.nan)

                            # Create and initialize a column "Validity sp_tool"
                            # with all valid samples
                            interval_data['Validity sp_tool'] = 1

                            # For the moment, the maximum number of samples for
                            # which we interpolate is estimated on the
                            # participant and differs between adults (11?)
                            # and children (5?), based on Tobii's sample_rate
                            # (adults: 120 Hz ; children: 60 Hz)
                            # TODO
                            # See if we can take into account the column
                            # interval_data['RecordingTimestampZero'] to
                            # evaluate the gaps' duration during the iteration?
                            if (participant_name[1:3] == 'ad'):
                                min_kept_gap = 11
                            elif (participant_name[1:3] == 'ch'):
                                min_kept_gap = 5
                            else:
                                error_name = "Error.\n" \
                                        + "The age (and therefore the " \
                                        + "sampling frequency) cannot be " \
                                        + "assessed. The 'RecordingName' " \
                                        + "field is invalid."
                                print_save_log(error_name, Path(__file__).stem,\
                                        print_logs, save_logs)

                            # Change to O the validity for data that will not
                            # be interpolated: consecutive missing data > at
                            # ~80 ms, i.e. between 5 and 11 samples
                            interval_data['Validity sp_tool'] = interval_data[
                                    'Validity sp_tool'].where(mask_knans(
                                    interval_data['GazePointX (MCSpx)'],
                                    min_kept_gap), other=0)

                            # Consecutive missing data < ~80 ms are
                            # interpolated (see above)
                            # TODO
                            # Change round(2) to round(1) when the debugging
                            # is finished? (for now, allows to easily find
                            # where the data has been interpolated)
                            # https://stackoverflow.com/questions/15933741/
                            # how-do-i-catch-a-numpy-warning-like-its
                            # -an-exception-not-just-for-testing
                            warnings.filterwarnings('error')
                            try:
                                interval_data['GazePointX (MCSpx)'] \
                                        = interval_data['GazePointX (MCSpx)'] \
                                        .interpolate(method='spline', order=3) \
                                        .where(mask_knans(interval_data[
                                        'GazePointX (MCSpx)'], min_kept_gap)) \
                                        .round(2)
                                interval_data['GazePointY (MCSpx)'] \
                                        = interval_data['GazePointY (MCSpx)'] \
                                        .interpolate(method='spline', order=3) \
                                        .where(mask_knans(interval_data[
                                        'GazePointY (MCSpx)'], min_kept_gap)) \
                                        .round(2)
                            except Warning as interpolate_warning:
                                print_save_log("\nSciPy interpolate "
                                        + "UserWarning for '{}' '{}':" \
                                        .format(participant_name, media_name)
                                        + str(interpolate_warning) + "\n",
                                        Path(__file__).stem, \
                                        print_logs, save_logs)

                            # https://stackoverflow.com/questions/32603051/
                            # pandas-dataframe-how-to-update-multiple-columns
                            # -by-applying-a-function
                            interval_data[['GazePointX (MCSpx)',
                                    'GazePointY (MCSpx)',
                                    'Validity sp_tool']] \
                                    = interval_data.apply(lambda row: \
                                    pd.Series([gaze_zero(row)[0],
                                    gaze_zero(row)[1], gaze_zero(row)[2]]),
                                    axis=1)

                        # Create the folder that will contain the .arff files
                        # and get the name of the .arff file
                        if export_type == 'RecordingName':
                            subdirectory = participant_name
                            filename_out = media_name
                        elif export_type == 'MediaName':
                            subdirectory = media_name
                            filename_out = participant_name

                        # Save the raw or "clean and interpolated" data to a
                        # .arff file
                        # Create the destination folder
                        out_path = filename.parents[3].joinpath(out_folder
                                + '_' + export_type + '_' + end_limit,
                                str(filename.parts[-3])
                                .replace('cat (dc1-dc2)', 'Cat (DC1-DC2)')
                                .replace('mem (dc3-dc4)', 'Mem (DC3-DC4)'),
                                filename.parts[-2], subdirectory)
                        out_path.mkdir(parents=True, exist_ok=True)
                        # Save the data
                        df2arff(out_path, filename_out, 0, 0, interval_data, 
                                ['GazePointX (MCSpx)', 'GazePointY (MCSpx)', \
                                'Validity sp_tool'], False, True, 'a',
                                stim_order)

                        # Create images of the plot
                        if out_plots:
                            # Define the subdirectory for the output plots
                            out_plots_path = base_dir.joinpath(
                                    'reports/Plot_samples', timestamp
                                    + '_' + str(filename.parts[-3])
                                    .replace('cat (dc1-dc2)', 'Cat (DC1-DC2)')
                                    .replace('mem (dc3-dc4)', 'Mem (DC3-DC4)') 
                                    + '_MovieStart-' + end_limit)
                                    #  + ('_' + task if task else '_alltasks')

                            plot_gazexy_time(interval_data.index.values,
                                    interval_data['GazePointX (MCSpx)'],
                                    interval_data['GazePointY (MCSpx)'], 
                                    interval_data, out_plots_path,
                                    ['MovieStart', end_limit])

                    elif len(validities_samples.index) != 1:
                        error_validities = "Error for '{}' '{}'\n".format( \
                                participant_name, media_name) \
                                + "There is not exactly one " \
                                + "percentage for the validity of the data."
                        print_save_log(error_validities, Path(__file__).stem, \
                                print_logs, save_logs)

                    elif validities_samples.iloc[0] < 75:
                        # TODO add percentage
                        error_validity = "Exclude '{}' '{}'. ".format( \
                                participant_name, media_name) \
                                + "The validity of the data is {} % ".format(
                                validities_samples.iloc[0]) \
                                + "(< 75 %)."
                        print_save_log(error_validity, Path(__file__).stem, \
                                print_logs, save_logs)

                elif not process_media:
                    excluded_media = "Exclude '{}' '{}'. ".format(
                            participant_name, media_name) \
                            + "Excluded by config.py filter_dict."
                    print_save_log(excluded_media, Path(__file__).stem,\
                            print_logs, save_logs)

                stim_order += 1

        elif not process_file:
            excluded_file = "File '{}' excluded.".format(filename)
            print_save_log(excluded_file, Path(__file__).stem, \
                    print_logs, save_logs)
