#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import fnmatch
# https://medium.com/p/11a072b58d5f?source=user_profile---------5------------------
# python-3-quick-tip-the-easy-way-to-deal-with-file-paths-on-windows-mac-and-linux-11a072b58d5f
from pathlib import Path
import pandas as pd

from config import base_dir, expe_task
from load_files import timings2df, load_arff_sp_tool
from common_functions import label_fixcross

# https://stackoverflow.com/questions/26886653/
# pandas-create-new-column-based-on-values-from-other-columns
def label_direction(row):
    """Label directions for SACCADE and SP
    
    Args:
        row (Series): [description]

    Returns:
        str: [description]
    """

    if row['EYE_MOVEMENT_TYPE'] != 'FIX':
        if abs(row['x_diff']) > abs(row['y_diff']) + 10 and row['x_diff'] > 0:
            return 'look ahead'
        if abs(row['x_diff']) > abs(row['y_diff']) + 10 and row['x_diff'] < 0:
            return 'back track'
        if abs(row['x_diff']) < abs(row['y_diff']) - 10 and row['y_diff'] > 0:
            return 'look down'
        if abs(row['x_diff']) < abs(row['y_diff']) - 10 and row['y_diff'] < 0:
            return 'look up'
        if abs(row['x_diff']) < 10 and abs(row['y_diff']) < 10:
            return 'none'
        return 'look diagonally'
    return 'none'

def aggregate_eyemovements(subfolder, stimuli_timings='Stimuli_timings.tsv'):
    """Aggregate eye movements by type in chronological order

    Args:
        subfolder (str): [description]
        stimuli_timings (str, optional): name of the .tsv file
                containing event times by media. Defaults to
                'Stimuli_timings.tsv'.
    """

    current_dir = base_dir.joinpath(in_folder, expe, task, subfolder)
    os.chdir(current_dir)

    # Initiate a variable to add the header in the Excel output only for the first .arff file
    old_media = str

    # Open .tsv file containing events time, by media
    df_timings = timings2df()

    # Loop on arff files in subdirectories
    for path, subdirs, files in os.walk(current_dir):
        for arff_file in files:
            if fnmatch.fnmatch(arff_file, '*.arff'):
                p = Path(path, arff_file)
                media = path.split('/')[-1]
                participant = arff_file.split('.')[0]
                # BLOCK = arff_file.split('_')[1].split('-')[0]

                df_data, width_px, height_px, stim_order = load_arff_sp_tool(
                    media, arff_file, 'processed_sp_tool')

                # Get clusters limit
                # https://stackoverflow.com/questions/40348541/pandas-diff-with-string
                df_data['cluster_limit'] = df_data['EYE_MOVEMENT_TYPE'].ne(
                    df_data['EYE_MOVEMENT_TYPE'].shift().bfill()).astype(int)
                # Number clusters
                df_data['cluster_id'] = df_data['cluster_limit'].cumsum()

                # Filter elements in a group smaller than 2
                # TODO check if it also works for children
                # https://pandas.pydata.org/pandas-docs/version/0.22/groupby.html#filtration
                df_data = df_data.groupby('cluster_id').filter(lambda x: len(x) > 2)

                # Get filtered clusters limit
                # https://stackoverflow.com/questions/40348541/pandas-diff-with-string
                df_data['filtered_cluster_limit'] = df_data['EYE_MOVEMENT_TYPE'].ne(
                    df_data['EYE_MOVEMENT_TYPE'].shift().bfill()).astype(int)

                # Number clusters
                df_data['filtered_cluster_id'] = df_data['filtered_cluster_limit'].cumsum()

                # Keep only fixations, saccades and smooth pursuits
                # https://stackoverflow.com/questions/28914078/
                # filter-out-rows-based-on-list-of-strings-in-pandas
                keep_eye_movements = ['FIX', 'SACCADE', 'SP']
                df_data = df_data[df_data['EYE_MOVEMENT_TYPE'].isin(keep_eye_movements)]

                # Get first row of each cluster
                df_first = df_data.groupby('filtered_cluster_id').first()
                # Get last row of each cluster
                df_last = df_data.groupby('filtered_cluster_id').last()

                df_result = df_first.copy()

                # Include metadata in data
                df_result['participant'] = participant
                df_result['language'] = df_result['participant'].apply(
                    lambda x: 'EN' if participant[0] == 'E' else 'FR')

                # Get, for each cluster, duration and distance between first and last element
                df_result['duration'] = df_last['time'] - df_first['time']
                df_result['x_diff'] = df_last['x'] - df_first['x']
                df_result['y_diff'] = df_last['y'] - df_first['y']
                df_result['distance'] = (df_result['x_diff'].pow(
                    2) + df_result['y_diff'].pow(2)).pow(0.5).astype(float).round(2)
                # Get direction for SACCADE and SP
                df_result['direction'] = df_result.apply(lambda row: label_direction(row), axis=1)

                # Label cluster's estimation before or after fixation cross end / action start
                label_fixcross(df_result, df_timings, media+'.avi')

                # Write result to Excel file
                # https://stackoverflow.com/questions/38074678/
                # append-existing-excel-sheet-with-new-dataframe-using-python-pandas#38075046
                # https://stackoverflow.com/questions/47737220/
                # append-dataframe-to-excel-with-pandas
                # https://stackoverflow.com/questions/47602533/
                # append-a-pandas-dataframe-to-an-existing-excel-table
                # https://stackoverflow.com/questions/20219254/
                # how-to-write-to-an-existing-excel-file-without-overwriting-data-using-pandas/
                # df_result.to_excel(writer)

                # Create Excel output file
                if old_media != media:
                    startrow = 0
                    old_df_size = 0
                    writer = pd.ExcelWriter(media + '.xlsx')
                    df_result.to_excel(
                        writer, index=False, header=True)
                    old_df_size = len(df_result) + 1
                # If Excel ouput file already exists, append data to it for each participant
                else:
                    startrow = startrow + old_df_size
                    df_result.to_excel(
                        writer, startrow=startrow,
                        index=False, header=False)
                    old_df_size = len(df_result)

                old_media = media

# aggregate_eyemovements('Labelled')
aggregate_eyemovements('Labelled-ad-ICLC')
