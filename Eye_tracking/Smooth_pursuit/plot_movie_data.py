﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no is_file member"
# for 'Path([...]).is_file()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member

from pathlib import Path
import collections
import matplotlib.patches as mpatches
from matplotlib import pyplot as plt
from matplotlib import animation
import pandas as pd
# https://stackoverflow.com/questions/29718238/
# how-to-read-mp4-video-to-be-processed-by-scikit-image
# https://stackoverflow.com/questions/41402550/
# raise-needdownloaderrorneed-ffmpeg-exe-needdownloaderror-need-ffmpeg-exe/44898152#44898152
# imageio.plugins.ffmpeg.download()
import imageio
import random

from config import base_dir, filter_dict, timestamp, \
        print_logs, save_logs, ffmpeg_path_config
from common_functions.common_functions import get_import_types, \
        get_arff_participant_media, print_save_log
from common_functions.load_files import list_files, filter_data, \
        load_arff_sp_tool

# TODO find why there is this new bug (2020-12-18)
# https://stackoverflow.com/questions/32032543/how-can-i-overcome-this-key-word-error/32034466
plt.rcParams['animation.ffmpeg_path'] = ffmpeg_path_config

def plot_movie_data(in_folders={'raw': '1_Converted_Tobiitsv2arff',
                                'corrected': '3_Corrected_arff',
                                'subfolders': ['']},
                    out_folder='reports/Plot_movie_data',
                    apply_filters=False,
                    color={'ref': (1, 0, 0)},
                    movie_output={'file': [False, 0.5],
                                  'show': [False, 1.0],
                                  'duration': 0}):
    """Plot a movie with one or more eye-tracking data overlays
            Optionally:
                * show stimuli movies with eye-tracking data overlay(s)
                * save .mp4 stimuli movies with eye-tracking data overlay(s)

    Adapted from http://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/
    Matplotlib Animation Example

    author: Jake Vanderplas
    email: vanderplas@astro.washington.edu
    website: http://jakevdp.github.com
    license: BSD
    Please feel free to use and modify this, but keep the above
    information. Thanks!

    Args:
        in_folders (dict,  optional): folders where the .arff
                data files are stored. Defaults to 
                {
                    'raw': '1_Converted_Tobiitsv2arff',
                    'corrected': '4_Corrected_arff',
                    'subfolders': ['']
                }
                'raw' (str): None or '' to only plot 'corrected' data
                'corrected' (str): None or '' to only plot 'raw' data
                (not corrected)
                'subfolders' (list of str): To select specific .arff
                corrected files. Defaults to '' to plot all corrected
                data in timestamp_mean-level_central-tendency subfolders.
        out_folder (str, optional): folder where the .mp4 files are 
                stored. Defaults to: 'reports/Plot_movie_data'.
        apply_filters (bool, optional): True to process only the files
                and data specified in filter_dict (in config.py).
                Defaults to False.
        color (dict, optional): color for the plots.
                Defaults to red for 'ref' plot
                {'ref': (1, 0, 0)}
                'ref' (tuple): RGB code of the color.
        movie_output (dict, optional): movie output parameters.
                Defaults to
                {
                    'file': [False, 0.5],
                    'show': [False, 1.0],
                    'duration': 0
                }
                'file' (bool, float): True to export a .mp4 movie of the
                animation at a speed of 'float' (slow down twice: 0.5;
                speed up twice: 2.0). Defaults to [False, 0.5].
                'show' (bool, float): True to show a movie of the
                animation at a speed of 'float' (slow down twice: 0.5;
                speed up twice: 2.0). Defaults to [False, 1.0].
                'duration': (int): frame number where to stop the
                showing and export; 0 to have it all. Defaults to 0.

    Raises:
        SystemExit: [description]
        SystemExit: [description]
    """
    # List the .arff files to process
    files_dict = {}
    # Assign reference .arff file list
    if in_folders['raw']:
        files_dict['ref'] = list_files(in_folders['raw'], '.arff', \
                apply_filters)
        if in_folders['corrected']:
            if in_folders['subfolders']:
                folders = [f for f in in_folders['subfolders']]
                files_dict['corrected'] = [list_files(in_folders['corrected'] \
                        + '/' + f, '.arff', apply_filters) for f in folders]
                _ = 1
            else:
                # https://stackoverflow.com/questions/973473/
                # getting-a-list-of-all-subdirectories-in-the-current-directory
                in_corrected = base_dir.joinpath(in_folders['corrected'])
                folders = [f for f in in_corrected.iterdir() if f.is_dir()]
                files_dict['corrected'] = [list_files(f, '.arff', \
                        apply_filters) for f in folders]
                _ = 2
    elif in_folders['corrected']:
        if in_folders['subfolders']:
            folders = [f for f in in_folders['subfolders']]
            files_dict['ref'] = list_files(in_folders['corrected'] + '/' \
                    + folders[0], '.arff', apply_filters)
            files_dict['corrected'] = [list_files(in_folders['corrected'] \
                    + '/' + f, '.arff', apply_filters) \
                    for f in folders[1:]]
            _ = 3
        else:
            in_corrected = base_dir.joinpath(in_folders['corrected'])
            folders = [f for f in in_corrected.iterdir() if f.is_dir()]
            files_dict['ref'] = list_files(folders[0], '.arff', apply_filters)
            files_dict['corrected'] = [list_files(f, '.arff', apply_filters) \
                    for f in folders[1:]]
            _ = 4
    else:
        raise SystemExit("Error. Please define at least one folder " \
                + "containing '.arff' files.")

    if len(files_dict['ref']) == 0:
        raise SystemExit("Error. There is no '.arff' file in the specified " \
                + "folder.")

    # Get import_type
    # Transform dict to list
    # https://stackoverflow.com/questions/1679384/
    # converting-dictionary-to-list
    filenames_dict_values = list(files_dict.values())
    # https://stackoverflow.com/questions/952914/
    # how-to-make-a-flat-list-out-of-list-of-lists
    # https://stackoverflow.com/questions/11963711/
    # what-is-the-most-efficient-way-to-search-nested-lists-in-python
    filenames_sublist = [item for sublist in filenames_dict_values \
            for item in sublist]
    filenames_corrected_sublist = [item for sublist in files_dict['corrected'] \
            for item in sublist]
    # https://stackoverflow.com/questions/2158395/
    # flatten-an-irregular-list-of-lists
    def flatten(l):
        for el in l:
            if isinstance(el, collections.Iterable) \
                    and not isinstance(el, (str, bytes)):
                yield from flatten(el)
            else:
                yield el

    filenames = list(flatten(filenames_sublist))
    # Unnecessary? (filenames_corrected_sublist already 'flat'?)
    filenames_corrected = list(flatten(filenames_corrected_sublist))

    import_type, _ = get_import_types(filenames)

    # Create the subdirectory where the output file is stored
    out_path = base_dir.joinpath(out_folder)

    # Load Validity_samples for TargetAction to process only files >= 75 %
    validity_samples = base_dir.joinpath('reports/Validity_samples', \
            '0c_Compliant_Tobiitsv_alltasks_detailed', \
            'TargetActionStart_TargetActionEnd.tsv')

    try:
        with open(validity_samples) as tsvfile:
            df_valid_samples = pd.read_csv(tsvfile, sep='\t')
    except IOError as strerror:
        print("I/O error: {0}".format(strerror))

    for filename in files_dict['ref']:
        df_data_dict = {}
        data_length_dict = {}
        x = {}
        y = {}
        imgplot = []
        my_animation = []

        # List corresponding corrected filenames
        same_filenames_corrected = [item for item in filenames_corrected \
                if item.parts[-2] == filename.parts[-2] \
                and item.name == filename.name]

        # Get media and participant names
        participant_name, media_name = get_arff_participant_media( \
                import_type, filename)

        percent_valid_samples = df_valid_samples[( \
                df_valid_samples['RecordingName'] == participant_name) \
                & (df_valid_samples['MediaName'] == (media_name + '.avi'))] \
                .iloc[0]['PercentValidSamples']

        if apply_filters:
            process_file = filter_data(participant_name, media_name)
        else: # elif not apply_filters:
            process_file = True

        # If the data pass the filter, process
        if process_file and percent_valid_samples >= 75:
            # # initialization function: plot the background of each frame
            # def init():
            #     line.set_data([], [])
            #     return line,

            # Get the ref data (corrected or not) and dimensions of the plot
            if in_folders['raw']:
                df_data_dict['ref'], width_px, height_px, _ = load_arff_sp_tool(
                    filename, 'raw')
            else:
                df_data_dict['ref'], width_px, height_px, _ = load_arff_sp_tool(
                    filename, 'processed_sp_tool')

            # Get the corrected data (if any)
            for f in same_filenames_corrected:
                df_data_dict[str(f.parts[-5])], _, _, _ = load_arff_sp_tool( \
                        f, 'raw')

            # !!! https://stackoverflow.com/questions/23176161/
            # animating-pngs-in-matplotlib-using-artistanimation
            # First set up the figure and the axis, then plot elements to animate
            fig = plt.figure()
            ax = plt.axes(xlim=(0, width_px), ylim=(height_px, 0))
            ax.xaxis.tick_top()
            # ax.set_position(up)
            # line, = ax.plot([], [], lw=20)
            # http://python.physique.free.fr/animations.html for representation
            # as a dot
            # line, = ax.plot([], [], 'o', color='r', markersize=40)

            # Get the correspondance between filename and stimuli_name
            correspondance_dict = {'DC1-2 DC2-3 MUTED Cat Expe Block1':
                    'DC1-2 NVCat', \
                    'DC1-2 DC2-3 MUTED Cat Expe Block2': 'DC1-2 NVCat', \
                    'DC1-2 DC2-3 MUTED Cat Expe Block3': 'DC1-2 NVCat', \
                    'DC1-3 DC2-2 Prod Expe Block1': 'DC1-3 DC2-2 Prod', \
                    'DC1-3 DC2-2 Prod Expe Block2': 'DC1-3 DC2-2 Prod', \
                    'DC1-3 DC2-2 Prod Expe Block3': 'DC1-3 DC2-2 Prod', \
                    'DC3-2 NVMem Phase1': 'DC3-2 Phase1 NVMem', \
                    'DC3-2 Phase2 DC4-2 Phase2': \
                    'DC3-2 Phase2 NVMem et DC4-2 Phase2 VMem', \
                    'DC3-3 DC4-2 Phase1 Prod': 'DC3-3 Prod et DC4-2 Phase1 VMem'}
            # stimuli_name = stimuli_dir.joinpath(correspondance_dict[filename \
            #         .parts[-3]], media_name + '.avi')
            # stimuli: folder where the stimuli movie files are stored
            stimuli_name = base_dir.joinpath('stimuli', correspondance_dict[filename \
                    .parts[-3]], media_name + '.avi')
            # Open the video and get fps
            vid = imageio.get_reader(str(stimuli_name), 'ffmpeg')
            vid_fps = vid.get_meta_data()['fps']

            # # In case we need video frame number (but we don't as data are
            # # shorter than videos)
            # meta_data = vid.get_meta_data()
            # num_frames = meta_data['nframes']

            # Compute the frequency in seconds for each image, from fps,
            # to resample data accordingly
            vid_t = 1/vid_fps
            resample_t = str(vid_t) + 'S'

            # color = {}
            # TODO add if df_data_dict
            for key in df_data_dict:
                # Resample the data from 60 or 120 Hz to 25 Hz (to suit video fps)
                df_data_dict[key]['date-time'] = pd.to_datetime( \
                        df_data_dict[key]['time'], unit='us')
                # http://benalexkeen.com/resampling-time-series-data-with-pandas/
                df_data_dict[key] = df_data_dict[key].resample(resample_t, \
                        on='date-time').last() # first() # mean()
                data_length_dict[key] = df_data_dict[key].shape[0]

                # # If there are 2 keys, plot the data in blue
                # if key != 'ref' and len(df_data_dict.keys()) == 2:
                #     color[key] = (0, 0, 1)
                # # If there are 3 keys, plot the data in blue
                # if key != 'ref' and len(df_data_dict.keys()) == 3:
                #     color[key] = (0, 0, 1)
                # # If there are more than 2 keys, create random color 
                # # for other plots than 'ref'
                # # TODO assign the same color to the same type of data
                # elif key != 'ref' and len(df_data_dict.keys()) > 2:
                #     # https://www.kite.com/python/answers/
                #     # how-to-generate-a-random-color-for-a-matplotlib-plot-in-python
                #     r = random.random()
                #     b = random.random()
                #     g = random.random()
                #     color[key] = (r, g, b)

            # https://www.w3resource.com/python-exercises/dictionary/
            # python-data-type-dictionary-exercise-15.php
            key_min = min(data_length_dict.keys(), key=( \
                    lambda k: data_length_dict[k]))
            data_length_min = data_length_dict[key_min]

            #TODO Check if df_data.shape[0] == df_data_corr.shape[0]?

            # Loop through images
            if movie_output['duration'] != 0:
                data_length_min = min(movie_output['duration'], data_length_min)
                # print(data_length_min)
            for i in range(1, data_length_min): # TODO check 0 or 1 for start
                image = vid.get_data(i)
                imgplot1 = plt.imshow(image)
                # my_animation.append([imgplot1])

                # TODO add if df_data_dict
                imgplot = {}
                for key in df_data_dict:
                    x[key] = df_data_dict[key]['x'][i]
                    y[key] = df_data_dict[key]['y'][i]
                    # https://stackoverflow.com/questions/61529897/
                    # python-error-sequence-index-is-not-an-int-slice-or-instance-with-index
                    if (x[key] != 0 and y[key] != 0):
                        imgplot[key], = plt.plot(x[key], y[key], 'o', \
                                color=color[key])  # , markersize=20
                    else:
                        imgplot[key], = plt.plot(x[key], y[key], 'o', \
                                color=color[key], alpha=0)  # , markersize=20

                # Append AxesImage object to the list
                # https://stackoverflow.com/questions/17853680/
                # animation-using-matplotlib-with-subplots-and-artistanimation
                plt.legend([imgplot[key] for key in df_data_dict], \
                        [key for key in df_data_dict])
                img_all = list(flatten([imgplot1, \
                        [imgplot[key] for key in df_data_dict]]))
                my_animation.append(img_all)

            # Call the animator. blit=True means only re-draw the parts that
            # have changed.
            # interval: delay between frames in ms:
            anim = animation.ArtistAnimation(fig, my_animation, \
                    interval=vid_t*1000/movie_output['show'][1], \
                    blit=True, repeat=False)

            if movie_output['file'][0] == True:
                # Set up formatting for the movie files
                Writer = animation.writers['ffmpeg']
                writer = Writer(fps=vid_fps*movie_output['file'][1], \
                        metadata=dict(artist='C. Vincent'), bitrate=1024)

                (out_path.joinpath(timestamp, participant_name)).mkdir( \
                        parents=True, exist_ok=True)
                # https://stackoverflow.com/questions/32032543/
                # how-can-i-overcome-this-key-word-error
                # ffmpeg added to Windows PATH
                anim.save(str(out_path.joinpath(timestamp, participant_name, \
                        participant_name + '_' + media_name + '.mp4')), \
                        writer=writer)

                # Save the animation as an mp4. This requires ffmpeg or mencoder
                # to be installed. The extra_args ensure that the x264 codec is
                # used, so that the video can be embedded in html5. You may need
                # to adjust this for your system: for more information, see
                # http://matplotlib.sourceforge.net/api/animation_api.html
                # anim.save('basic_animation.mp4', fps=30,
                #       extra_args=['-vcodec', 'libx264'])
                # mywriter = animation.FFMpegWriter()
                # anim.save('mymovie.mp4',writer=mywriter)

            if movie_output['show'][0]:
                plt.show(block=False)
                plt.pause(1)

            plt.clf()
            plt.close()

        elif not process_file or percent_valid_samples < 75:
            excluded_file = "File '{}' excluded.".format(filename)
            if process_file and percent_valid_samples < 75:
                excluded_file = excluded_file + ' Percent valid samples < 75%.'
            print_save_log(excluded_file, Path(__file__).stem, \
                    print_logs, save_logs)