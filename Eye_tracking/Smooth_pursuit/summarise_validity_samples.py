#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no is_file member"
# for 'Path([...]).is_file()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member

from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

from config import base_dir, expe_task, print_logs, save_logs
from common_functions.common_functions import print_save_log
from common_functions.load_files import Tobiitsv2df

def summarise_validity_samples(in_folder='reports/Validity_samples'):
    """Plot a .png figure summarising the number of valid items
            for reports for Cat or Mem experiment.

    Args:
        in_folder (str, optional): folder where the .tsv files containing
                samples validity are.
    """
    # Load validity samples reports
    target_fixcross_validity = base_dir.joinpath(Path(in_folder), \
            '0c_Compliant_Tobiitsv_alltasks_detailed', \
            'TargetFixCrossStart_TargetFixCrossEnd.tsv')
    try:
        with open(target_fixcross_validity) as tsvfile:
            df_target_fixcross = pd.read_csv(tsvfile, sep='\t')
            # Add prefix to distinct columns coming from different files
            # https://stackoverflow.com/questions/39772896/
            # add-prefix-to-specific-columns-of-dataframe
            df_target_fixcross.columns = ['target_fixcross_' + col \
                    if col == 'PercentValidSamples' \
                    or col == 'NbValidSamples' \
                    or col == 'NbTotalSamples' \
                    else col for col in df_target_fixcross.columns]
    except IOError as strerror:
        print("I/O error: {0}".format(strerror))

    target_action_validity = base_dir.joinpath(Path(in_folder), \
            '0c_Compliant_Tobiitsv_alltasks_detailed', \
            'TargetActionStart_TargetActionEnd.tsv')
    try:
        with open(target_action_validity) as tsvfile:
            df_target_action = pd.read_csv(tsvfile, sep='\t')
            # Add prefix to distinct columns coming from different files
            df_target_action.columns = ['target_action_' + col \
                    if col == 'PercentValidSamples' \
                    or col == 'NbValidSamples' \
                    or col == 'NbTotalSamples' \
                    else col for col in df_target_action.columns]
    except IOError as strerror:
        print("I/O error: {0}".format(strerror))

    # Basic counts on df_target_action
    target_sup_75 = df_target_action[df_target_action['target_action_PercentValidSamples'] >= 75].count()
    target_inf_75 = df_target_action[df_target_action['target_action_PercentValidSamples'] < 75].count()
    target_empty = df_target_action.isnull().sum()

    print(target_sup_75['target_action_PercentValidSamples'])
    print(target_inf_75['target_action_PercentValidSamples'])
    print(target_empty['target_action_PercentValidSamples'])
    print(target_sup_75['target_action_PercentValidSamples'] \
            + target_inf_75['target_action_PercentValidSamples'] \
            + target_empty['target_action_PercentValidSamples'])

    # Merge columns from both DataFrames to a single DataFrame
    df_target_all_validity = pd.merge(df_target_fixcross, df_target_action, \
            how='inner', on=['StudioTestName', 'RecordingName', 'MediaName'])

    df_target_all_validity.loc[( \
            df_target_all_validity['target_fixcross_PercentValidSamples'] >= 75) \
            & (df_target_all_validity['target_action_PercentValidSamples'] >= 75), \
            'mask_PercentValidSamples'] = True

    df_target_all_validity.loc[( \
            df_target_all_validity['target_fixcross_PercentValidSamples'] < 75) \
            | (df_target_all_validity['target_action_PercentValidSamples'] < 75), \
            'mask_PercentValidSamples'] = False

    df_target_all_validity['mask_PercentValidSamples'] = \
            df_target_all_validity['mask_PercentValidSamples'].astype('bool')

    df_target_all_validity['both_PercentValidSamples'] = \
            (round(((df_target_all_validity['target_fixcross_NbValidSamples'] \
            + df_target_all_validity['target_action_NbValidSamples']) \
            / (df_target_all_validity['target_fixcross_NbTotalSamples'] \
            + df_target_all_validity['target_action_NbTotalSamples'])) * 100, 0)) \
            .astype(int)

    df_target_all_validity.loc[( \
            df_target_all_validity['mask_PercentValidSamples'] == True) \
            & (df_target_all_validity['both_PercentValidSamples'] >= 75), \
            'both_mask_PercentValidSamples'] \
            = df_target_all_validity['both_PercentValidSamples']

    # df_target_all_validity['both_mask_PercentValidSamples'] = \
    #         df_target_all_validity[['mask_PercentValidSamples', \
    #         'both_PercentValidSamples']] *= 1

    print(df_target_all_validity.head())

    # df_target_all_validity.plot.hist(bins=5)
    # https://stackoverflow.com/questions/49291382/
    # overlay-histograms-in-one-plot/49292189
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.hist([df_target_all_validity['target_fixcross_PercentValidSamples'], \
            df_target_all_validity['target_action_PercentValidSamples'], \
            df_target_all_validity['both_mask_PercentValidSamples']],
            label=('target fixcross', 'target action', \
            'target fixcross, action and whole >= 75 %'), bins=20, \
            range=[0, 100])
    ax.legend()
    plt.savefig(str(target_fixcross_validity.parent.joinpath( \
            'target_PercentValidSamples.png')))
