#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms

def plot_gazexy_time(time, gaze_x, gaze_y, filtered_data, out_path,
        limits=None):
    """Plot as .png files x and y gaze positions (inferred by the eye-tracker)
            as a function of time.
            Adapted from PyGazeAnalyser's plot_gazexy_time

    Args:
        time (Series): time.
        gaze_x (Series): gaze position on the x axis.
        gaze_y (Series): gaze position on the y axis.
        filtered_data (DataFrame): eye-tracking data.
        out_path (str): path where to save the plot.
        limits (list, optional): column names ['start', 'end'] of the
                stimuli timings file containing the times when to start
                and end the plots. Defaults to [].
    """

    recording_res_col = filtered_data.columns.get_loc("RecordingResolution")
    recording_res_x = int(filtered_data.iloc[0, recording_res_col] \
            .split(" x ")[0])/2
    recording_res_y = int(filtered_data.iloc[0, recording_res_col] \
            .split(" x ")[1])/2
    recording_res_max = max(recording_res_x, recording_res_y)
    # MediaNameCol = filtered_data.columns.get_loc("MediaName")
    # media_name = str(filtered_data.iloc[0, MediaNameCol])[0:-4]
    media_name = filtered_data["MediaName"].iloc[0][:-4] # [:-4] SWAET
    # Get the participant name to insert in the plot filename
    # ParticipantNameCol = data.columns.get_loc("ParticipantName")
    # participant_name = data.iloc[0, ParticipantNameCol]
    participant_name = filtered_data["ParticipantName"].iloc[0][0:9] # [0:9] SWAET
    
    # Create a 12x4 inch figure
    fig = plt.figure(figsize=(12, 8)) # figsize=(12, 8)
    # Create a subplot for the left eye, 2,1,1 means the subplot
    # grid will be 2 rows and 1 column, and we are about to create
    # the subplot for row 1 of 2
    # "Left Eye Position"
    # gaze_axis = fig.add_subplot(2, 1, 1)
    gaze_axis = fig.add_subplot(1, 1, 1)
    time = time/1000 # Added for SWAET to get ms instead of µs
    gaze_axis.plot(time, gaze_x, label="X Gaze")
    gaze_axis.plot(time, gaze_y, label="Y Gaze")
    gaze_axis.axhline(y=recording_res_max, color='b', linestyle='--')
    # TODO CHECK!!! Changed for SWAET
    # gaze_axis.axhline(y=recording_res_y, color='g', linestyle='--')
    gaze_axis.axhline(y=360, color='g', linestyle='--')
    # plt.xticks(np.arange(tmin,tmax,0.5),rotation='vertical')
    gaze_axis.set_ylabel("Position (pixels)")
    gaze_axis.set_ylim([0, 900])
    gaze_axis.set_title("Gaze Position", fontsize=12)
    # Fill in missing eye data areas of the plot with a vertical bar the full
    # height of the sub plot.
    trans = mtransforms.blended_transform_factory(
        gaze_axis.transData, gaze_axis.transAxes)
    gaze_axis.fill_between(time, 0, 1, where=((filtered_data.ValidityLeft >= 2)
        & (filtered_data.ValidityRight >= 2)), facecolor="DarkRed",
        alpha=0.5, transform=trans)
    # Added for SWAET
    # gaze_axis.axvline(x=(5200),color='k',linestyle='--')

    # fixation_axis = fig.add_subplot(
    #    2, 1, 2, sharex=gaze_axis, sharey=gaze_axis)
    #fixation_axis.plot(time, fixation_gaze_x, label="X Fixation")
    #fixation_axis.plot(time, fixation_gaze_y, label="Y Fixation")
    # fixation_axis.axhline(
    #    y=recording_res_x, color='b', linestyle='--')
    # fixation_axis.axhline(
    #    y=recording_res_y, color='g', linestyle='--')
    #fixation_axis.set_xlabel("Time (ms)")
    #fixation_axis.set_ylabel("Position (pixels)")
    #fixation_axis.set_ylim([0, 900])
    # fixation_axis.set_title(
    #    "Fixation Position - "+FixationFilter, fontsize=12)
    # Fill in missing eye data areas of the plot with a vertical bar the full
    # height of the sub plot.
    # trans = mtransforms.blended_transform_factory(
    #        fixation_axis.transData, fixation_axis.transAxes)
    # fixation_axis.fill_between(time, 0, 1, where=((filtered_data[ \
    #         "FixationPointX (MCSpx)"] == 657) & (filtered_data[ \
    #         "FixationPointY (MCSpx)"] == 410)), facecolor="DarkRed", \
    #         alpha=0.5, transform=trans)
    # fixation_axis.fill_between(time, 0, 1, where=((filtered_data[ \
    #         "FixationPointX (MCSpx)"].isnull()) & (filtered_data[ \
    #         "FixationPointY (MCSpx)"].isnull())), facecolor="DarkRed", \
    #         alpha=0.5, transform=trans)

    # fig.suptitle("Eye Sample Data For %s - %s" %
    #                 (media_name, participant_name), fontsize=14)
    # fig.suptitle(("{} - {}\n {}_{}").format(media_name, participant_name,
    #         limits[0], limits[1]), fontsize=14)
    fig.suptitle((participant_name + ' - ' + media_name) \
            + ('\n' + limits[0] + '_' + limits[1] if limits else ''), \
            fontsize=14, va='top')

    # Create the out_path where the output file(s) are stored
    Path(out_path).mkdir(parents=True, exist_ok=True)
    fig.savefig(str(Path(out_path).joinpath(participant_name + '_' + media_name \
            # + ('_' + limits[0] + '-' + limits[1] if limits else '') \
            + '.png')))

    plt.close(fig)
