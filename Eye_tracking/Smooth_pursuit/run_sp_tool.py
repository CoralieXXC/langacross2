#!/usr/bin/env python
# -*- coding: utf8 -*-
from pathlib import Path
import os
import sys

# For run_detection usage, see "# For example," in
# https://github.com/beersoccer/sp_tool/blob/master/run_detection.py

# Import sp_tool to find where run_detection is located and import it
import sp_tool
# https://stackoverflow.com/questions/247770/
# how-to-retrieve-a-modules-path
sp_tool_path = Path(sp_tool.__file__).parent
# https://stackoverflow.com/questions/46191044/
# python-cant-import-a-function-from-another-py-file
sys.path.insert(0, str(sp_tool_path))
import run_detection

# Avoid RuntimeWarning (though with np.errstate() would be better)
# https://stackoverflow.com/questions/34955158/
# what-might-be-the-cause-of-invalid-value-encountered-in-less-equal-in-numpy
import warnings
warnings.filterwarnings("ignore",category =RuntimeWarning)

from config import base_dir

def run_sp_tool(in_folder, out_folder):
    """Classify eye movements with sp_tool (for Python 3)
            https://github.com/beersoccer/sp_tool

    Args:
        in_folder (str): folder where the raw or corrected .arff files are stored
        out_folder (str): folder where the .arff files with classification are stored.
    """

    in_path = base_dir.joinpath('data', 'eye-tracking', in_folder)
    out_path = base_dir.joinpath('data', 'eye-tracking', out_folder)

    # Parameters to compute min_pts
    min_params = {'cat_ad': {'obs_nb': 84, 'sampling_freq': 120},
                'cat_ch': {'obs_nb': 152, 'sampling_freq': 60},
                'mem_mem_ad': {'obs_nb': 89, 'sampling_freq': 120},
                'mem_prod_ad': {'obs_nb': 88, 'sampling_freq': 120},
                'mem_view_ad': {'obs_nb': 45, 'sampling_freq': 120},
                'mem_mem_ch': {'obs_nb': 144, 'sampling_freq': 60}, # DC3-2 Phase2 DC4-2 Phase2
                'mem_prod_ch': {'obs_nb': 145, 'sampling_freq': 60}, # DC3-3 DC4-2 Phase1 Prod
                'mem_view_ch': {'obs_nb': 72, 'sampling_freq': 60}  # DC3-2 NVMem Phase1
    }

    # Compute min_pts for each condition/set of observers:
    # min_pts = 160 * (obs_nb / 46.9) * (sampling_freq / 250)
    # https://github.com/beersoccer/sp_tool (in 2. Adjust the config file)
    min_arg = {}
    for key in min_params:
        min_arg[key] = [ round( 160 * ( min_params[key]['obs_nb'] / 46.9 ) \
                * ( min_params[key]['sampling_freq'] / 250 ))]

    # Keep default sp_tool config parameters except for:
    # input_folder, gaze_file_pattern, output_folder and min_pts
    all_params = [
        # Adults parameters for Cat (DC1-DC2)
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block1/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block1/'),
            min_pts=int(str(min_arg['cat_ad'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block2/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block2/'),
            min_pts=int(str(min_arg['cat_ad'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block3/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block3/'),
            min_pts=int(str(min_arg['cat_ad'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block1/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block1/'),
            min_pts=int(str(min_arg['cat_ad'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block2/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block2/'),
            min_pts=int(str(min_arg['cat_ad'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block3/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block3/'),
            min_pts=int(str(min_arg['cat_ad'][0]))
        ),
        # Adults parameters for Mem (DC3-DC4)
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Mem (DC3-DC4)/DC3-2 NVMem Phase1/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Mem (DC3-DC4)/DC3-2 NVMem Phase1/'),
            min_pts=int(str(min_arg['mem_view_ad'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Mem (DC3-DC4)/DC3-2 Phase2 DC4-2 Phase2/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Mem (DC3-DC4)/DC3-2 Phase2 DC4-2 Phase2/'),
            min_pts=int(str(min_arg['mem_mem_ad'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Mem (DC3-DC4)/DC3-3 DC4-2 Phase1 Prod/'),
            gaze_file_pattern='*ad*.arff',
            output_folder=out_path.joinpath('Mem (DC3-DC4)/DC3-3 DC4-2 Phase1 Prod/'),
            min_pts=int(str(min_arg['mem_prod_ad'][0]))
        ),
        # Children parameters for Cat (DC1-DC2)
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block1/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block1/'),
            min_pts=int(str(min_arg['cat_ch'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block2/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block2/'),
            min_pts=int(str(min_arg['cat_ch'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block3/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-2 DC2-3 MUTED Cat Expe Block3/'),
            min_pts=int(str(min_arg['cat_ch'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block1/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block1/'),
            min_pts=int(str(min_arg['cat_ch'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block2/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block2/'),
            min_pts=int(str(min_arg['cat_ch'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block3/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Cat (DC1-DC2)/DC1-3 DC2-2 Prod Expe Block3/'),
            min_pts=int(str(min_arg['cat_ch'][0]))
        ),
        # Children parameters for Mem (DC3-DC4)
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Mem (DC3-DC4)/DC3-2 NVMem Phase1/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Mem (DC3-DC4)/DC3-2 NVMem Phase1/'),
            min_pts=int(str(min_arg['mem_view_ch'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Mem (DC3-DC4)/DC3-2 Phase2 DC4-2 Phase2/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Mem (DC3-DC4)/DC3-2 Phase2 DC4-2 Phase2/'),
            min_pts=int(str(min_arg['mem_mem_ch'][0]))
        ),
        run_detection.create_parameters(
            input_folder=in_path.joinpath('Mem (DC3-DC4)/DC3-3 DC4-2 Phase1 Prod/'),
            gaze_file_pattern='*ch*.arff',
            output_folder=out_path.joinpath('Mem (DC3-DC4)/DC3-3 DC4-2 Phase1 Prod/'),
            min_pts=int(str(min_arg['mem_prod_ch'][0]))
        )
    ]

    for params in all_params:
        run_detection.run_detection(params)
