#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no is_file member"
# for 'Path([...]).is_file()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member

# https://medium.com/p/11a072b58d5f?source=user_profile---------5------------------
# python-3-quick-tip-the-easy-way-to-deal-with-file-paths-on-windows-mac-and-linux-11a072b58d5f
from pathlib import Path
import pandas as pd

from config import base_dir, timestamp, print_logs, save_logs
from common_functions.common_functions import get_import_types, \
        print_save_log, get_arff_participant_media
from common_functions.load_files import list_files, timings2df, \
        load_arff_sp_tool

def assess_correction_cross(in_folder='3_Classified_sp_tool_arff',
                            out_folder='reports/Assess_correction'):
    """Export a .tsv file containing an evaluation of the correction to
            apply on gaze points per participant per media

    Args:
        in_folder (str, optional): folder where the .arff data files are
                stored (in /expe/*task*/ subfolders).
                Defaults to '3_Classified_sp_tool_arff'.
        out_folder (str, optional): folder where the resulting .tsv file
                is stored. Defaults to 'reports/Assess_correction'.
    """

    # TODO add task?

    # Load the.tsv file containing event times by media
    df_timings = timings2df()

    # List the .arff files to process after checking arguments and assigning
    # default filter values
    filenames = list_files(in_folder, '.arff', False)

    # Get import_type (and unused import_subtype)
    import_type, _ = get_import_types(filenames)

    # Create the subdirectory where the output file is stored
    out_path = base_dir.joinpath(out_folder)
    out_path.mkdir(parents=True, exist_ok=True)

    # Browse the files listed in filenames
    for filename in filenames:
        # If it is a new output file, add the header
        out_filename = timestamp + '_' + filename.parents[2].name + '.tsv'
                # + ('_' + task if task else '_alltasks')
        if not out_path.joinpath(out_filename).is_file():
            with open(out_path.joinpath(out_filename),
                    'w', encoding='utf8') as out_file:
                out_file.write('RecordingName\tMediaName\tBlock\t' \
                        + 'stim_order\tx_corr\ty_corr\n')

        # Get media and participant names
        participant_name, media_name = get_arff_participant_media(
                import_type, filename)
        media_avi = media_name + '.avi'
        block = media_name.split('_')[1].split('-')[0][1]

        df_data, width_px, height_px, stim_order = load_arff_sp_tool(
                filename, 'processed_sp_tool')
        # df_data, width_px, height_px, stim_order = load_arff_sp_tool(
        #     path.split('/')[-1], filename, 'raw')

        # Get the end of the fixation cross for the target and convert it to µs
        if not pd.isnull(df_timings.loc[df_timings['MovieName']== media_avi]
                .iloc[0]['TargetFixCrossEnd']):
            target_fixcross_end = int(df_timings.loc[df_timings['MovieName']
                    == media_avi].iloc[0]['TargetFixCrossEnd'])*1000
            target_action_start = int(df_timings.loc[df_timings['MovieName']
                    == media_avi].iloc[0]['TargetActionStart'])*1000
        # For 'Mem' task, get the end of the fixation cross for the choices
        else:
            target_fixcross_end = int(df_timings.loc[df_timings['MovieName']
                    == media_avi].iloc[0]['ChoiceFixCrossEnd'])*1000
            target_action_start = min(int(df_timings.loc[df_timings['MovieName']
                    == media_avi].iloc[0]['ChoiceLActionStart']),
                    int(df_timings.loc[df_timings['MovieName']
                    == media_avi].iloc[0]['ChoiceRActionStart']))*1000

        # Get the start time of the first saccade between the fixation cross or
        # action start + 200000 and the fixation cross or action start + 400000
        df_saccade_fixcross = df_data.loc[(df_data['EYE_MOVEMENT_TYPE']
                == 'SACCADE') & (df_data['time'] \
                > target_fixcross_end + 200000) & (df_data['time'] \
                < target_fixcross_end + 400000)]
        df_saccade_action = df_data.loc[(df_data['EYE_MOVEMENT_TYPE']
                == 'SACCADE') & (df_data['time'] \
                > target_action_start + 200000) & (df_data['time'] \
                < target_action_start + 400000)]

        if (df_saccade_fixcross.empty and df_saccade_action.empty):
            no_saccade_message = "{}\t{}\t".format(participant_name, \
                    media_avi) + "no saccade"
            print_save_log(no_saccade_message, Path(__file__).stem, \
                    print_logs, save_logs)

            # Save to an output text file the data about the (no) correction
            with open(out_path.joinpath(timestamp + '_' 
                    + filename.parents[2].name
                    # + ('_' + task if task else '_alltasks')
                    + '.tsv'), 'a', encoding='utf8') as out_file:
                out_file.write(participant_name + '\t' + media_avi + '\t' 
                        + block + '\t' + str(stim_order) + '\tNaN\tNaN' + '\n')
        else:
            if not df_saccade_fixcross.empty:
                df_saccade = df_saccade_fixcross
            else:
                df_saccade = df_saccade_action
            
            # This copy is to avoid a warning
            df_data = df_data.copy()

            # Select rows that contain the EYE_MOVEMENT_TYPE defined below
            eye_movement_type = 'FIX'
            df_before_saccade = df_data.loc[(df_data['EYE_MOVEMENT_TYPE'] \
                    == eye_movement_type) & (df_data['time'] \
                    < df_saccade['time'].iloc[0])]
            saccade_message = "{}\t{}\t".format(participant_name, \
                    media_avi) + "saccade (in µs)\t" + str(df_saccade['time']
                    .iloc[0])
            print_save_log(saccade_message, Path(__file__).stem, \
                    print_logs, save_logs)

            # https://stackoverflow.com/questions/32853043/
            # pandas-filtering-consecutive-rows
            # https://stackoverflow.com/questions/40979760/
            # compare-2-consecutive-rows-and-assign-increasing-value-if-different-using-panda
            df_before_saccade = df_before_saccade.copy()
            df_before_saccade['Index'] = df_before_saccade.index.to_series()
            # Center data to (0, 0) to simplify further manipulation
            df_before_saccade['x'] = df_before_saccade['x'] - width_px/2
            df_before_saccade['y'] = df_before_saccade['y'] - height_px/2

            # Group by consecutive eye_movement_type samples
            group_name = eye_movement_type + '_GROUPS'
            df_before_saccade[group_name] = (
                    (df_before_saccade['Index'] - df_before_saccade['Index']
                    .shift(1)) != 1).cumsum()
            df_grouped = df_before_saccade.groupby(group_name)

            # Get mean and standard deviation for each group
            mean_groups = df_grouped['x', 'y'].mean()
            mean_groups = mean_groups.rename(columns={'x': 'mean_x'})
            mean_groups = mean_groups.rename(columns={'y': 'mean_y'})
            std_groups = df_grouped['x', 'y'].std()
            std_groups = std_groups.rename(columns={'x': 'std_x'})
            std_groups = std_groups.rename(columns={'y': 'std_y'})

            # Put results in the same dataframe
            # https://pandas.pydata.org/pandas-docs/stable/merging.html
            groups_data = pd.concat([mean_groups, std_groups], axis=1)
            groups_data['participant'] = participant_name
            groups_data['media'] = media_avi
            groups_data['SIZE'] = df_grouped.size()
        
            # Define long enough fixation's duration (based on sample rate)
            if (participant_name.startswith('Ead')
                    or participant_name.startswith('Fad')):
                min_fix_size = 6
            elif (participant_name.startswith('Ech')
                    or participant_name.startswith('Fch')):
                min_fix_size = 3
            else:
                error_message = "{}\t{}\terror: " \
                        .format(participant_name, media_avi) \
                        + "The age (and therefore the sampling frequency) " \
                        + "cannot be retrieved. " \
                        + "The 'RecordingName' field is invalid."
                print_save_log(error_message, Path(__file__).stem, \
                        print_logs, save_logs)

            # Get the mean_y for the last long enough fixation's duration
            # if it exists
            if not any(groups_data['SIZE'] >= min_fix_size):
                no_fixation_message = "{}\t{}\t".format(participant_name,
                        media_avi) + "no fixation before the " \
                        + "saccade in the explored time"
                print_save_log(no_fixation_message, Path(__file__).stem, \
                        print_logs, save_logs)

                # Add to an output text file the data about the (no) correction
                with open(out_path.joinpath(timestamp + '_'
                        + filename.parents[2].name
                        # + ('_' + task if task else '_alltasks')
                        + '.tsv'), 'a', encoding='utf8') as out_file:
                    out_file.write(participant_name + '\t' + media_avi + '\t' \
                            + block + '\t' + str(stim_order) \
                            + '\tNaN\tNaN' + '\n')

            else:
                lastlongfix_x = groups_data[groups_data['SIZE'] \
                        >= min_fix_size].iloc[-1]['mean_x']
                lastlongfix_y = groups_data[groups_data['SIZE'] \
                        >= min_fix_size].iloc[-1]['mean_y']
                
                # If distance on y axis is more than threshold, subtract it,
                # except during BLINK
                if lastlongfix_y > 20:
                    df_data['y'] = df_data.apply(lambda row: row['y'] \
                            if (row['EYE_MOVEMENT_TYPE'] == 'BLINK') 
                            else (row['y'] - lastlongfix_y), axis=1).round(2)

                # Add to an output text file the data about the (no) correction
                with open(out_path.joinpath(timestamp + '_'
                        + filename.parents[2].name
                        # + ('_' + task if task else '_alltasks')
                        + '.tsv'), 'a', encoding='utf8') as out_file:
                    out_file.write(participant_name + '\t' + media_avi + '\t' \
                            + block + '\t' + str(stim_order) + '\t' \
                            + str(lastlongfix_x.round(2)) + '\t' \
                            + str(lastlongfix_y.round(2)) + '\n')
