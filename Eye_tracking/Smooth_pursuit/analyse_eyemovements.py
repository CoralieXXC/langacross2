#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import fnmatch
# https://medium.com/p/11a072b58d5f?source=user_profile---------5------------------
# python-3-quick-tip-the-easy-way-to-deal-with-file-paths-on-windows-mac-and-linux-11a072b58d5f
from pathlib import Path
import pandas as pd
import numpy as np

base_dir = Path('C:/Users/cvincent/Documents/Projets/Langacross_2/2019-Smooth_pursuit')

# Reasoning to go from ° to pixels :
# The display was about 60 cm from the participants
# Display size (according to manual): 40.8*25.5cm (! in files.xconf : 40.8*24.5cm -> error)
# In Tobii files: RecordingResolution = 1280*800 px
# Note: the videos are 1280*720
# So according to Pythagoras:
# For angle = 5° to 60 cm, tan(x) ~ x. With x (5°) in rad ~0.087266
# tan(x) = distance / dist_disp (~ x : for 5° : 5*2*np.pi/360 * 60)
# distance = tan(x) * dist_disp
# TODO check with http://cossincalc.com/

def segment_id(row, action_start):
    """Label 4 segments from beginning of action
            A: 0 <= A < 750000
            B: 750000 <= B < 1500000
            C: 1500000 <= C < 2250000
            D: 2250000 <= D < 3000000
            E: 3000000 <= E < end
    
    Args:
        row {Series} -- [description]
        action_start {int} -- [description]
    
    Returns:
        str: segment label ID (A, B, C, D or E)
    """

    if (row['time'] - action_start >= 0) & (row['time'] - action_start < 750000):
        return 'A'
    elif (row['time'] - action_start >= 750000 and row['time'] - action_start < 1500000):
        return 'B'
    elif (row['time'] - action_start >= 1500000 and row['time'] - action_start < 2250000):
        return 'C'
    elif (row['time'] - action_start >= 2250000 and row['time'] - action_start < 3000000):
        return 'D'
    elif row['time'] - action_start >= 3000000:
        return 'E'
    else:
        return ''

def long_vs_short(row):
    #TODO CHECK that an angle of 40° matches 1579 pixels
    sacc_40deg_px = round(np.tan(40*2*np.pi/360) * 60 * 1280 / 40.8).astype(int)

    if row['fixcross'] == 'action':
        if (row['EYE_MOVEMENT_TYPE'] == 'FIX') & (row['duration'] >= 180000):
            return 'FIX_LONG'
        elif (row['EYE_MOVEMENT_TYPE'] == 'FIX') & (
                row['duration'] < 180000) & (row['duration'] >= 90000):
            return 'FIX_SHORT'
        elif (row['EYE_MOVEMENT_TYPE'] == 'SACCADE') & (row['distance'] < 165):
            return 'SACC_SHORT'
        # TODO CHECK (modified but not tested)
        elif (row['EYE_MOVEMENT_TYPE'] == 'SACCADE') & (row['distance'] >= 165) & (
                row['distance'] < sacc_40deg_px):
            return 'SACC_LONG'
        elif (row['EYE_MOVEMENT_TYPE'] == 'FIX') & (
            row['duration'] < 90000):
            return 'FIX_DISC'
        elif (row['EYE_MOVEMENT_TYPE'] == 'SACCADE') & (row['distance'] >= sacc_40deg_px): 
            return 'SACC_DISC'        
        else:
            return ''
    else:
        return ''

def amb_vs_focal(row):
    """Label certain fixations as ambient (FIX_AMBIENT) or focal (FIX_FOCAL)
            FIX_AMBIENT: fixation duration < 180 ms, followed by a saccade >= 5°
            FIX_FOCAL: fixation duration >= 180 ms, followed by a saccade < 5°
    
    Args:
        row (Series): [description]
    
    Returns:
        str: label (FIX_AMBIENT, FIX_FOCAL or '')
    """

    #TODO CHECK that an angle of 5° matches 165 pixels
    sacc_5deg_px = round(np.tan(5*2*np.pi/360) * 60 * 1280 / 40.8).astype(int)

    # https://stackoverflow.com/questions/37967070/
    # using-a-shift-function-within-an-apply-function-to-compare-rows-in-a-pandas-da
    #TODO CHECK as things can happen (noise, blinks) between the fixation and the saccade.
    if (row['fixcross'] == 'action') & (row['EYE_MOVEMENT_TYPE'] == 'FIX'):
        if (row['duration'] < 180000) & (row['next_EMT'] == 'SACCADE') & (row['next_distance'] >= 165):
            return 'FIX_AMBIENT'
        elif (row['duration'] >= 180000) & (row['next_EMT'] == 'SACCADE') & (row['next_distance'] < 165):
            return 'FIX_FOCAL'
        else:
            return ''
    else:
        return ''

def analyse_eyemovements(base_dir, expe, task, subfolder):
    """Analyse eye movements (description to complete)

    Args:
        base_dir (str): path where the data files are stored (in /in_folder/expe/*task*/ subfolders)
        expe (str): 'Cat (DC1-DC2)' (NVCat-VCat) or 'Mem (DC3-DC4)' (NVMem-VMem)
        task (str): 'Cat' or 'Mem' or 'Prod' or 'View' ('View': NVMem 1st phase)
                Defaults to '' (all tasks selected)
        subfolder (str): [description]

    Raises:
        SystemExit: [description]
        SystemExit: [description]
    """

    if (expe != 'DC1-DC2') and (expe != 'DC3-DC4'):
        print("Please specify a valid experiment.")
        raise SystemExit
    if (task != 'Cat') and (task != 'Mem') and (task != 'Prod'):
        print("Please specify a valid task.")
        raise SystemExit

    # Create paths according to experiment, task and subfolder
    current_dir = base_dir.joinpath(in_folder, expe, task, subfolder)
    os.chdir(current_dir)

    # Loop on .xlsx files
    for path, subdirs, files in os.walk(current_dir):
        # Prepare dataframe to append data from one MEDIA for all participants
        appended_data = []

        for XLSX_FILE in files:
            if fnmatch.fnmatch(XLSX_FILE, '*.xlsx'):
                p = Path(path, XLSX_FILE)
                MEDIA = XLSX_FILE.split('.')[0]
                print(MEDIA)

                data = pd.read_excel(XLSX_FILE, header=0, encoding='UTF-8')

                # Create a participants list
                participants = data['participant'].unique()
                
                #TODO optimize code to avoid for loop
                # # https://engineering.upside.com/a-beginners-guide-to-optimizing-pandas-code-for-speed-c09ef2c6a4d6
                # # https://stackoverflow.com/questions/40348541/
                # # pandas-diff-with-string/40348884#40348884
                # https://stackoverflow.com/questions/52263451/
                # loop-through-subsets-of-rows-in-a-dataframe
                # https://stackoverflow.com/questions/16476924/
                # how-to-iterate-over-rows-in-a-dataframe-in-pandas
                # data['change'] = data['fixcross'].ne(data['fixcross'].shift().bfill()).astype(int)
                # # data['from_action'] = data[data['fixcross'] == 'action' and data['participant'] == participant].iloc[0]['time']
                
                # Iterate on participants
                for participant in participants:
                    data_part = data.copy()
                    data_part = data_part[data_part['participant'] == participant]

                    # Get beginning of action
                    # https://stackoverflow.com/questions/17322109/
                    # get-dataframe-row-count-based-on-conditions
                    if len(data_part[data_part['fixcross'] == 'action']) > 0:
                        action_start = data_part[data_part['fixcross'] == 'action'].iloc[0]['time']
                        # print(data_part['time'] - action_start)

                        # Label 4 segments from beginning of action:
                        data_part['segment_id'] = data_part.apply(
                            lambda row: segment_id(row, action_start), axis=1)

                        data_part['long_vs_short'] = data_part.apply(
                            lambda row: long_vs_short(row), axis=1)

                        # Add 2 columns to get the following EYE_MOVEMENT_TYPE (EMT) and distance
                        # to assess fixations ambient vs focal (only during 'action', not 'cross')
                        data_part['next_EMT'] = data_part[data_part['fixcross'] == 'action']['EYE_MOVEMENT_TYPE'].shift(-1)
                        data_part['next_distance'] = data_part[data_part['fixcross'] == 'action']['distance'].shift(-1)

                        data_part['amb_vs_focal'] = data_part.apply(
                            lambda row: amb_vs_focal(row), axis=1)

                        # Insert a column with MEDIA name right before the participant column
                        # https://stackoverflow.com/questions/18674064/
                        # how-do-i-insert-a-column-at-a-specific-column-index-in-pandas
                        # https://stackoverflow.com/questions/13021654/
                        # get-column-index-from-column-name-in-python-pandas
                        data_part.insert(loc=data_part.columns.get_loc(
                            'participant'), column='MEDIA', value=MEDIA)

                    else:
                        data_part.insert(loc=data_part.columns.get_loc(
                            'participant'), column='MEDIA', value=MEDIA)

                    # https://stackoverflow.com/questions/28669482/
                    # appending-pandas-dataframes-generated-in-a-for-loop
                    appended_data.append(data_part)

        # To get ordered columns after concat
        # https://stackoverflow.com/questions/39046931/
        # column-order-in-pandas-concat
        ordered = [c for c in data_part.columns]
        appended_data = pd.concat(appended_data)
        appended_data = appended_data[ordered]
        appended_data.to_excel('results.xlsx', index=False)


# analyse_eyemovements(base_dir, 'DC1-DC2', 'Prod', 'Labelled')
analyse_eyemovements(base_dir, 'DC1-DC2', 'Prod', 'Labelled-ad-ICLC')
