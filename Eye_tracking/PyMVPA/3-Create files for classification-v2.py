﻿#!/usr/bin/python
# vim : set fileencoding=utf-8 :
"""Ce script Python permet, depuis les fichiers interpolés générés (par média, un fichier par participant
pour la classification).

Il nécessite d'avoir installé Python 2.7 (ou ultérieur) sur son ordinateur :
https://www.python.org/download/releases/2.7/

Ce script comporte XX étapes :
1) 
2) 

Fichier nécessaire en entrée (dans les sous-répertoires de l'endroit où est situé ce script) :
- exports formatés ".tsv". Ex. "TDVDTM/1.tsv"
"""

import os.path
pathname = os.path.dirname(os.path.abspath(__file__))
from fnmatch import fnmatch

# Définition des sous-répertoire où sont stockés les fichiers après traitement.
outdirectory = 'outformvpa2'
try:
        os.mkdir(outdirectory)
except Exception:
        pass

p = 0
prev_media = ''

# Parcours des fichiers contenus dans le sous-dossier "outspline" du dossier où est le script
for path, subdirs, tsvlist in os.walk(os.path.join(pathname, "outspline")):
        for f in tsvlist:
                with open(os.path.join(path, f), 'r') as finput:
                        dirmedia = path[-13:-7]
                        dirlang = path [-6:-4]
                        dirDC = path[-3:]

                        if (dirmedia != prev_media):
                                p = 1

                                try:
                                        os.mkdir(outdirectory+'/'+dirmedia)
                                except Exception:
                                        pass

                        else:
                                p = p+1

                        savepath_gaze = os.path.join(outdirectory+'/'+dirmedia+'/'+'gaze_coords.txt')
                        savepath_trial = os.path.join(outdirectory+'/'+dirmedia+'/'+'trial_attrs.txt')
                        
                        entete = finput.readline()
                        data = finput.readlines()

                        with open(savepath_gaze,'a') as fgaze, open(savepath_trial,'a') as ftrial:
                                for line in data:
                                        line = line.split("\t")
                                        fgaze.write(line[1]+"\t"+line[2]+"\t"+str(p)+"\n")

##                                        if (dirDC == "DC1"): # Pour limiter à une DC
##                                                 fgaze.write(line[1]+"\t"+line[2]+"\t"+str(p)+"\n")

                                if (dirlang == "EN"):
                                        ftrial.write("1\t1\n")
                                elif (dirlang == "FR"):
                                        ftrial.write("2\t2\n")

##                                if (dirDC == "DC1"):
##                                        ftrial.write("1\t1\n")
##                                elif (dirDC == "DC2"):
##                                        ftrial.write("2\t2\n")

##                                if (dirlang == "EN" and dirDC == "DC2"):
##                                        ftrial.write("1\t1\n")
##                                        p = p + 1
##                                elif (dirlang == "FR" and dirDC == "DC2"):
##                                        ftrial.write("2\t2\n")
##                                        p = p + 1
                                
                        prev_media = dirmedia
