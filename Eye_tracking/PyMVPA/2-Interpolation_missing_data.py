﻿#!/usr/bin/python
# vim : set fileencoding=utf-8 :
"""Ce script Python permet, depuis les fichiers générés (par média, un fichier par participant
pour la classification).

Il nécessite d'avoir installé Python 2.7 (ou ultérieur) sur son ordinateur :
https://www.python.org/download/releases/2.7/

Ce script comporte XX étapes :
1) 
2) 

Fichier nécessaire en entrée (dans les sous-répertoires de l'endroit où est situé ce script) :
- exports formatés ".tsv". Ex. "TDVDTM/1.tsv"
"""

import os.path
pathname = os.path.dirname(os.path.abspath(__file__))
from fnmatch import fnmatch

from pandas import DataFrame, read_csv

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Définition des sous-répertoire où sont stockés les fichiers après traitement.
outdirectory = 'outspline'
try:
        os.mkdir(outdirectory)
except Exception:
        pass

# Parcours des fichiers contenus dans le sous-dossier "out" du dossier où est le script
for path, subdirs, tsvlist in os.walk(os.path.join(pathname, "out")):
        for f in tsvlist:
                dirmedia = path[-13:-7]
                dirlang = path [-6:-4]
                dirDC = path[-3:]
                try:
                        os.mkdir(outdirectory+'/'+dirmedia)
                except Exception:
                        pass

                try:
                        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirlang)
                except Exception:
                        pass

                try:
                        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirlang+'/'+dirDC)
                except Exception:
                        pass
                
                df = DataFrame.from_csv(os.path.join(path, f), sep='\t')
                df = df.interpolate(method='spline', order=2).ffill().bfill()
                df = df.apply(np.round)
                df.to_csv(os.path.join(pathname, outdirectory, dirmedia, dirlang, dirDC, f), sep='\t')

        
