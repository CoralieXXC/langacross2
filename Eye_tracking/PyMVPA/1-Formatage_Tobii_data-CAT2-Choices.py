﻿#!/usr/bin/python
# vim : set fileencoding=utf-8 :
"""Ce script Python permet, depuis l'export Tobii des données oculométriques de
tous les participants pour un média de générer un fichier par participant
pour la classification.

Il nécessite d'avoir installé Python 2.7 (ou ultérieur) sur son ordinateur :
https://www.python.org/download/releases/2.7/

Ce script comporte XX étapes :
1) 
2) 

Fichier nécessaire en entrée (dans le répertoire où est situé ce script) :
- export Tobii ".tsv". Ex. "Langacross2 Cat (DC1-DC2) - EN - adults all 2014-01-16_DC1-2 _ DC2-3 MUTED Cat Expe Block1_NVCat_B1-1_TDVDTM.avi.tsv"
"""

import os.path
pathname = os.path.dirname(os.path.abspath(__file__))

# Définition des sous-répertoire où sont stockés les fichiers après traitement.
outdirectory = 'out'
try:
        os.mkdir(outdirectory)
except Exception:
        pass

# Liste des fichiers .tsv contenus dans le dossier où est le script
import glob
tsvlist = glob.glob('*.tsv')

# Parcours de ces fichiers .tsv
for f in tsvlist:
        
        # Définition des sous-répertoire où sont stockés les fichiers après traitement.
        dirmedia = f[-14:-8]
        try:
                os.mkdir(outdirectory+'/'+dirmedia)
        except Exception:
                pass
        
        #dirDC = "DC1"
        #try:
        #        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirDC)
        #except Exception:
        #        pass # à vérifier

        #dirDC = "DC2"
        #try:
        #        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirDC)
        #except Exception:
        #        pass # à vérifier
       
               
        with open(f,'r') as data:
                prev_rec = ''
                prev_media = ''
                prev_timestamp = ''
                prev_event = ''
                
                first_timestamp = ''
                n = 0

                begin_time = 5719
                #end_time = 9720
                end_time = 6300
                
                entete = data.readline().rstrip('\t\n\r').split("\t")

                RecordingName = entete.index('RecordingName')
                MediaName = entete.index('MediaName')
                RecordingTimestamp = entete.index('RecordingTimestamp')
                StudioEvent = entete.index('StudioEvent')
                GazePointX = entete.index('GazePointX (MCSpx)')
                GazePointY = entete.index('GazePointY (MCSpx)')
                
                for current_line in data:
                        fields = current_line.rstrip('\n\r').split("\t")
                        current_rec = fields[RecordingName]
                        current_media = fields[MediaName]
                        current_timestamp = fields[RecordingTimestamp]
                        current_event = fields[StudioEvent]
                        current_gazepointX = fields[GazePointX]
                        current_gazepointY = fields[GazePointY]

                        lang = current_rec[0]
                        if lang == "E":
                                dirlang = "EN"
                                try:
                                        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirlang)
                                except Exception:
                                        pass # à vérifier

                                dirDC = "DC1"
                                try:
                                        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirlang+'/'+dirDC)
                                except Exception:
                                        pass # à vérifier

                                dirDC = "DC2"
                                try:
                                        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirlang+'/'+dirDC)
                                except Exception:
                                        pass # à vérifier
                        elif lang == "F":
                                dirlang = "FR"
                                try:
                                        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirlang)
                                except Exception:
                                        pass # à vérifier
                                
                                dirDC = "DC1"
                                try:
                                        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirlang+'/'+dirDC)
                                except Exception:
                                        pass # à vérifier

                                dirDC = "DC2"
                                try:
                                        os.mkdir(outdirectory+'/'+dirmedia+'/'+dirlang+'/'+dirDC)
                                except Exception:
                                        pass # à vérifier
                        else:
                                print ("Problème dans le RecordingName : langue indéterminée")

                        age = current_rec[1:3]
                        if (age != "ad" and age != "ch"):
                                print ("Problème dans le RecordingName : âge indéterminé") 
                        
                        DC = current_rec[-3:]

                        #savepath = os.path.join('.'+DC, str(n)+'.tsv')
                        savepath = os.path.join(outdirectory+'/'+dirmedia+'/'+dirlang+'/'+DC, str(n)+'.tsv')
                       
                        #with open(current_rec+'-'+current_media+'.tsv','a') as fdata:
                        #with open(str(n)+'.tsv','a') as fdata:
                        with open(savepath,'a') as fdata:
                                if (current_event == 'MovieStart'):
                                        p = 1
                                        current_line = data.readline() # Saut de la ligne qui suit MovieStart
                                        fields = current_line.rstrip('\n\r').split("\t")
                                        first_timestamp = fields[RecordingTimestamp]
                                        # écriture de l'en-tête du fichier
                                elif ((int(current_timestamp) - int(first_timestamp) > begin_time) and (int(current_timestamp) - int(first_timestamp) < end_time)):
                                        #fdata.write(fields[GazePointX]+'\t'+fields[GazePointY]+'\t'+str(int(current_timestamp)-int(first_timestamp))+'\n')
                                        diff_timestamp = int(current_timestamp) - int(prev_timestamp)
                                        if p == 1:
                                                fdata.write('\tX\tY\tSubject\n')
                                        fdata.write(current_timestamp+'\t'+current_gazepointX+'\t'+current_gazepointY+'\t'+str(n)+'\n')
                                        #fdata.write(str(p)+'\t'+current_gazepointX+'\t'+current_gazepointY+'\n')
                                        p = p+1

                        if (current_rec != prev_rec):
                                # suppression des fichiers vides (bad programming, pas trouvé comment corriger l'erreur)
                                check_empty_file = os.stat(savepath).st_size
                                if check_empty_file == 0:
                                        os.remove(savepath)                                       
                                n = n+1

                        prev_rec = current_rec
                        prev_media = current_media
                        prev_timestamp = current_timestamp
