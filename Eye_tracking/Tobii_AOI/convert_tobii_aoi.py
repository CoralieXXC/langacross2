#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no 'glob' member"
# for 'Path([...]).glob()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member
# https://docs.python.org/3.6/library/pathlib.html#pathlib.Path.glob

import csv

from config import base_dir

def convert_tobii_aoi(in_folder,
                      out_folder='1_Converted_Tobii_AOI2tsv',):
    """Convert unhandleable .txt Tobii AOI exports to handleable .tsv files

    Args:
        in_folder (str): folder where the .txt Tobii AOI exports are stored
        out_folder (str, optional): folder where the resulting .tsv files are stored.
                Defaults to 'stimuli/1_Converted_Tobii_AOI2tsv'.
    """
    stimuli_path = base_dir.joinpath('stimuli')

    filenames = [f for f in stimuli_path.joinpath(in_folder) \
            .glob('*/*{}'.format('_All_AOIs.txt'))]

    for filename in filenames:
        # https://stackoverflow.com/questions/125703/how-to-modify-a-text-file
        with open(filename, 'r') as f:
            x_y = []
            i = 0
            kf_all = []

            for line in f:
                if line.startswith('AOI name: '):
                    aoi_name = line.split( )[-1]
                elif line.startswith('Number of Vertices: '):
                    vertices_nb = int(line.split( )[-1])
                elif line.startswith('Keyframe timestamp = '):
                    kf_timestamp = line.split( )[-1]
                elif line.startswith('Active: '):
                    aoi_status = line.split( )[-1]
                # https://stackoverflow.com/questions/5577501/
                # how-to-tell-if-string-starts-with-a-number-with-python
                elif line[0].isdigit():
                    x_y.append(line.replace(',', '.').split('\t')[0])
                    x_y.append(line.replace(',', '.').rstrip().split('\t')[-1])
                    i += 1
                    if i == vertices_nb:
                        i = 0
                        kf_info = filename.stem + '\t' + aoi_name + '\t' + str(vertices_nb) \
                                + '\t' + kf_timestamp + '\t' + aoi_status + '\t' + '\t'.join(x_y)

                        kf_all.append([kf_info])
                        x_y = []

        # Create folders to store resulting .tsv files
        # https://stackoverflow.com/questions/273192/
        # how-can-i-safely-create-a-nested-directory
        # Use replace() trick to fix lower case pathlib/Windows bug
        # https://stackoverflow.com/questions/56873491/
        # why-does-path-rglob-returns-filenames-in-lower-case-if-whole-name
        # -is-specified
        out_path = stimuli_path.joinpath(out_folder,
                str(filename.parts[-2]).replace('cat (dc1-dc2)',
                'Cat (DC1-DC2)').replace('mem (dc3-dc4)', 'Mem (DC3-DC4)'))
        out_path.mkdir(parents=True, exist_ok=True)

        # https://stackoverflow.com/questions/14037540/
        # writing-a-python-list-of-lists-to-a-csv-file
        with open(out_path.joinpath(filename.stem + '.tsv'), "w", newline="") as f:
            writer = csv.writer(f)
            writer.writerows(kf_all)
