#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no 'glob' member"
# for 'Path([...]).glob()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member
# https://docs.python.org/3.6/library/pathlib.html#pathlib.Path.glob

from pathlib import Path
import pandas as pd

from config import base_dir, print_logs, save_logs, timestamp, \
        E_manner_dict, E_path_dict, E_distr_dict, \
        F_manner_dict, F_path_dict, F_distr_dict, \
        corrected_dict, missed_dict, producedlater_dict, unintelligible_dict
from common_functions.common_functions import print_save_log

def rename_itemid(in_folder, cat_order, mem_order):
    # Load the .txt files listing the transcription codes for Cat and Mem
    cat_order_file = base_dir.joinpath(in_folder, cat_order)
    df_cat_order = pd.read_csv(cat_order_file, sep='\t', \
            header=None, names=['RecordingName', 'item_code'], \
            engine='python', encoding='utf-8')

    mem_order_file = base_dir.joinpath(in_folder, mem_order)
    df_mem_order = pd.read_csv(mem_order_file, sep='\t', \
            header=None, names=['RecordingName', 'item_code'], \
            engine='python', encoding='utf-8')

    # List the _cod.cha files
    filenames = [f for f in base_dir.joinpath(in_folder) \
                .glob('*inserted_transcription_itemID*/*{}'.format('_cod.cha'))]

    # Browse the files listed in filenames
    for filename in filenames:
        out_path = filename.parents[1].joinpath(str(filename.parts[-2])[ \
                0:3] + '_renamed_production_itemID_' + timestamp)
        out_path.mkdir(parents=True, exist_ok=True)
        out_file = out_path.joinpath(filename.parts[-1])

        if filename.parts[-2][0:3] == 'Cat':
            df_order_participant = df_cat_order[df_cat_order[ \
                    'RecordingName'] == filename.stem[:-4]]
            code_start = 2
            code_end = -4
        elif filename.parts[-2][0:3] == 'Mem':
            df_order_participant = df_mem_order[df_mem_order[ \
                    'RecordingName'] == filename.stem[:-4]]
            code_start = 8
            code_end = -4

        replaced_nb = 0

        with open(filename) as fs:
            with open(out_file, 'w') as fd:
                for line in fs:
                    if line.startswith('@G:\t') \
                            and line != '@G:\tTraining_item\n':
                        code_line = '@G:\t' + df_order_participant['item_code'] \
                                .values[replaced_nb][code_start:code_end] + '\n'
                        fd.write(code_line)
                        replaced_nb += 1
                        message = "'{}' modified: '{}' replaced by '{}'".format( \
                                filename.parts[-1], line[:-1], code_line[:-1])
                        print_save_log(message, Path(__file__).stem, \
                                print_logs, save_logs)
                    else:
                        fd.write(line)

def check_renamed_itemid(in_folder, component='manner_and_path', log_filter=True):
    """Check if strings in dict in config.py were produced for relevant items

    Args:
        in_folder (str): folder where the Production transcription files are stored
        component (str, optional): component(s) of the motion event to focus on:
                'manner', 'path' or 'manner_and_path'.
                Defaults to 'manner_and_path'.
        log_filter (bool, optional): only output problems messages in the log.
                Otherwise, ouput all messages in the log.
                Defaults to True.
    """
    # List the _cod.cha files
    filenames = [f for f in base_dir.joinpath(in_folder) \
                .glob('*renamed_production_itemID*/*{}'.format('_cod.cha'))]

    # Browse the files listed in filenames
    for filename in filenames:
        with open(filename, 'r', encoding='utf8') as f:
            i = 1
            item = ''
            item_type = ''
            manner = ''
            manner_list = []
            path = ''
            path_list = []
            corrected_item = False
            missed_item = False
            producedlater_item = False
            unintelligible_item = False

            for line in f:
                # For lines containing the item code (except Training_item)
                if line.startswith('@G:\t') and line != '@G:\tTraining_item\n':
                    item = line[4:-1]
                    # For experimental and control items
                    if line[-5] != 'D':
                        item_type = 'exp_item'
                        manner = line[-3]
                        path = line[-2]
                    # For distractors
                    else:
                        item_type = 'distractor'
                elif line.startswith('*SUJ:\t') and (manner != '' \
                        or item_type == 'distractor'):
                    # For English participants
                    if str(filename.parts[-1])[0] == 'E':
                        # Get manner and path string list
                        if item_type == 'exp_item':
                            manner_list = E_manner_dict[manner]
                            path_list = E_path_dict[path]
                        # Get distractor string list
                        else:
                            distr_list = E_distr_dict[item]
                    # For French participants
                    elif str(filename.parts[-1])[0] == 'F':
                        # Get manner and path string list
                        if item_type == 'exp_item':
                            manner_list = F_manner_dict[manner]
                            path_list = F_path_dict[path]
                        # Get distractor string list
                        else:
                            distr_list = F_distr_dict[item]

                    if item_type == 'exp_item':
                        # https://www.geeksforgeeks.org/python-test-if-string-contains-element-from-list/
                        # Using list comprehension to check if string contains list element
                        manner_in_line = [ele for ele in manner_list if (ele.lower() in line.lower())]
                        path_in_line = [ele for ele in path_list if (ele.lower() in line.lower())]
                    else:
                        distr_in_line = [ele for ele in distr_list if (ele.lower() in line.lower())]

                    if item_type == 'distractor':
                        string_found = bool(distr_in_line)
                    elif component == 'manner':
                        string_found = bool(manner_in_line)
                    elif component == 'path':
                        string_found = bool(path_in_line)
                    elif component == 'manner_and_path':
                        string_found = bool(manner_in_line) or bool(path_in_line)

                    if not string_found:
                        if str(filename.parts[-1]) in corrected_dict:
                            # Check if the item hasn been corrected
                            corrected_list = corrected_dict[str(filename.parts[-1])]
                            corrected_item = [ele for ele in corrected_list if item in ele]
                        if str(filename.parts[-1]) in missed_dict:
                            # Check if the item has been produced
                            missed_list = missed_dict[str(filename.parts[-1])]
                            missed_item = [ele for ele in missed_list if item in ele]
                        if str(filename.parts[-1]) in producedlater_dict:
                            # Check if the item is intelligible
                            producedlater_list = producedlater_dict[str(filename.parts[-1])]
                            producedlater_item = [ele for ele in producedlater_list if item in ele]
                        if str(filename.parts[-1]) in unintelligible_dict:
                            # Check if the item is intelligible
                            unintelligible_list = unintelligible_dict[str(filename.parts[-1])]
                            unintelligible_item = [ele for ele in unintelligible_list if item in ele]

                        # If the item is not to be ignored for error messages
                        if not(bool(corrected_item) or bool(missed_item) \
                                or bool(producedlater_item) or bool(unintelligible_item)):
                            # https://www.pythonforbeginners.com/basics/convert-a-list-to-string-in-python
                            message = str(filename.parts[-1]) + '\t' + item \
                                    + '\tproblem: "' \
                                    + line[:-1] + '"'
                                    # + '\tline ' + str(i) \
                                    # + ','.join(manner_list) + '" not in "' \
                            print_save_log(message, Path(__file__).stem, \
                                    print_logs, save_logs)
                    else:
                        if not log_filter:
                            message = str(filename.parts[-1]) + ' line ' + str(i) \
                                    + ' ok: "' + ' '.join(manner_list) + '" in "' + line[:-1] + '"'
                            print_save_log(message, Path(__file__).stem, \
                                    print_logs, save_logs)
                        item = ''
                        item_type = ''
                        manner = ''
                        manner_list = []
                        path = ''
                        path_list = []
                        corrected_item = False
                        producedlater_item = False
                        unintelligible_item = False
                        missed_item = False

                i += 1

def get_transcribed_files(in_folder, transcriber):
    # List the _cod.cha files
    filenames = [f for f in base_dir.joinpath(in_folder) \
                .glob('*renamed_production_itemID*/*{}'.format('_cod.cha'))]

    # Browse the files listed in filenames
    for filename in filenames:
        with open(filename, 'r', encoding='utf8') as f:
            for line in f:
                # For files transcribed by transcriber
                if line.startswith('@Transcriber:\t') and transcriber in line:
                    message = str(filename.parts[-1]) + ' transcribed by ' \
                            + line[(line.find('\t') + 1):-1]
                    print_save_log(message, Path(__file__).stem, \
                            print_logs, save_logs)                    