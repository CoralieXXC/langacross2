#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no 'glob' member"
# for 'Path([...]).glob()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member
# https://docs.python.org/3.6/library/pathlib.html#pathlib.Path.glob

from pathlib import Path
import pandas as pd
import numpy as np

from config import base_dir, print_logs, save_logs, timestamp
from common_functions.common_functions import print_save_log

def correct_cod_syntax(in_folder):
    filenames = [f for f in base_dir.joinpath(in_folder) \
            .glob('*/*{}'.format('_cod.cha'))]

    for filename in filenames:
        filename.parents[1].joinpath(filename.parent.name \
                + '_corrected_' + timestamp).mkdir(parents=True, exist_ok=True)
        out_file = filename.parents[1].joinpath(filename.parent.name \
                + '_corrected_' + timestamp, filename.name)

        # Get the number of lines for the current file
        line_nb = len(open(filename).readlines())

        # https://stackoverflow.com/questions/125703/how-to-modify-a-text-file
        with open(filename, 'r') as f:
            # Line iterator
            i = 1
            # Line breaks count in transcription lines
            j = 1
            prev_line = ''

            for line in f:
                # Clean leading and trailing whitespaces and tabs
                # (no message, not saved to log)
                # (as in "Ech15_A1_DC2_cod.cha" line 33)
                line = line.rstrip('\n').rstrip(' ').rstrip('\t').rstrip(' ') + '\n'
                if line.startswith('\t'):
                    line = '\t' + line.lstrip('\t').lstrip(' ').lstrip('\t')

                # Correct %cod lines (containing '$') so they start with '%cod:\t$'
                # (as in "Ead01_DC2_cod.cha" line 23)
                if '$' in line \
                        and not line.startswith(r'%cod:' + '\t$'):
                    orig_line = line
                    start_cod = line.find('$')
                    line = r'%cod:' + '\t' + line[start_cod:]
                    message = str(filename.parts[-1]) + ' line ' + str(i) \
                            + ' changed: "' + orig_line[0:start_cod] + '" replaced by "' \
                            + r'%cod:' + '\t"'
                    print_save_log(message, Path(__file__).stem, \
                            print_logs, save_logs)

                # Correct *SUJ lines (as in Ead21_DC1_cod.cha, Ead22_DC2_cod.cha,
                # Ead21_DC4_cod.cha, Fch13_A1_DC2_cod.cha)
                # Untranscribed Training_item is: "line == '*SUJ:\n'"
                if line.startswith('*') and not (line.startswith('*SUJ:\t') \
                        or line.startswith('*EXP:\t') or line == '*SUJ:\n'):
                    line = line.replace('*EXP: \t', '*EXP:\t', 1)
                    line = line.replace('*SUB:\t', '*SUJ:\t', 1)
                    line = line.replace('*SUJ: \t', '*SUJ:\t', 1)
                    if not (line.startswith('*SUJ:\t') \
                            or line.startswith('*EXP:\t') or line == '*SUJ:\n'):
                        message = str(filename.parts[-1]) + ' line ' + str(i) \
                            + ' problem. The transcription line does not begin with ' \
                            + '"*SUJ:\t" or "*EXP:\t" or is not "*SUJ:"'
                        print_save_log(message, Path(__file__).stem, \
                                print_logs, save_logs)

                # Delete line breaks in transcription lines
                if prev_line.startswith('*SUJ') or prev_line.startswith('*EXP'):
                    # If the transcription has more than one line
                    if line.startswith('\t'):
                        prev_line = prev_line.replace('\n', ' ' + line[1:], 1)
                        message = str(filename.parts[-1]) + ' line ' + str(i) + ' deleted'
                        print_save_log(message, Path(__file__).stem, \
                                print_logs, save_logs)
                        j += 1
                    # If line contains cod (if the production is finished for this item)
                    # and if the transcription has more than one line
                    elif (line.startswith('%') or line.startswith('@')) and j > 1:
                        k = i - j
                        message = str(filename.parts[-1]) + ' line ' + str(k) \
                                + ' to ' + str(i - 1) \
                                + ' merged: line breaks deleted in "' \
                                + prev_line.replace('\n','"')
                        print_save_log(message, Path(__file__).stem, \
                                print_logs, save_logs)
                        with open(out_file, 'a+') as destination:
                            destination.write(prev_line)
                        prev_line = line
                        j = 1
                    # If the transcription only has one line
                    else:
                        with open(out_file, 'a+') as destination:
                            destination.write(prev_line)                        
                        prev_line = line
                        j = 1

                # Copy other (not empty) lines
                # https://stackoverflow.com/questions/2967194/
                # open-in-python-does-not-create-a-file-if-it-doesnt-exist
                elif i > 1:
                    with open(out_file, 'a+') as destination:
                        # To not write empty lines
                        if prev_line != '\n':
                            destination.write(prev_line)

                # Detect if this is the last line
                if i == line_nb:
                    with open(out_file, 'a+') as destination:
                        # to not log empty lines
                        if line != '\n':
                            destination.write(line)

                # Store the content of the current line as previous line
                # except for transcription lines with line breaks
                if not (prev_line.startswith('*SUJ') \
                        or prev_line.startswith('*EXP')):
                    prev_line = line

                i += 1

def count_missing_itemid(in_folder, unproduced_items):
    # Define folder to store resulting .csv files
    out_path = base_dir.joinpath(in_folder, 'reports')
    out_path.mkdir(parents=True, exist_ok=True)

    # Create an empty list to store data
    df = []

    # List the _cod.cha files
    filenames = [f for f in base_dir.joinpath(in_folder)
                .glob('*corrected*/*{}'.format('_cod.cha'))]

    # Browse the files listed in filenames
    for filename in filenames:
        # Count the number of @G (identifying items) in each transcription file
        # https://stackoverflow.com/questions/23232248/
        # python-3-4-counting-occurrences-in-a-txt-file
        item_nb = open(str(filename), 'r').read().count('@G')
        # Populate the list
        # https://stackoverflow.com/questions/28056171/
        # how-to-build-and-fill-pandas-dataframe-from-for-loop/42670389#42670389
        df.append((filename.stem[:-4], item_nb))

    # Create a DataFrame containing the data with the column names
    df = pd.DataFrame(df, columns=('RecordingName', 'item_nb'))

    # Load the .tsv file listing the unproduced items
    unproduced_file = base_dir.joinpath(in_folder, \
            'reports', 'Unproduced_items', unproduced_items)
    df_unproduced_items = pd.read_csv(unproduced_file, sep='\t', \
            header=0, encoding='utf-8')
    # Group by RecordingName with the grouping column not becoming an index
    # https://stackoverflow.com/questions/32059397/
    # pandas-groupby-without-turning-grouped-by-column-into-index
    df_unproduced_items = df_unproduced_items.groupby(['RecordingName'], \
            as_index=False).count()
    df = pd.merge(df, df_unproduced_items, how='left', on=['RecordingName'])
    df = df.drop_duplicates()
    df = df.drop(columns=['item_duration'])
    df.rename(columns={'MediaName': 'unproduced_item_nb'}, inplace=True)
    # https://stackoverflow.com/questions/21287624/
    # convert-pandas-column-containing-nans-to-dtype-int
    df['unproduced_item_nb'] = df['unproduced_item_nb'].fillna(0).astype(int)
    df['item_nb_with_unproduced'] = df['item_nb'] + df['unproduced_item_nb']
    out_item_count = out_path.joinpath(timestamp + '_count_items.csv')
    df.to_csv(out_item_count, sep="\t", encoding='utf-8', index=False)

    df = df.groupby(['item_nb_with_unproduced'], as_index=False).count()
    df = df.drop(columns=['item_nb', 'unproduced_item_nb'])
    out_group_count = base_dir.joinpath(in_folder, 'reports', \
            timestamp + '_count_missing_itemID.csv')
    df.to_csv(out_group_count, sep="\t", encoding='utf-8', index=False)

def insert_delete_itemid(in_folder, itemid2insert, itemid2delete):
    # Load the .csv file containing the item IDs to insert
    insert_itemid_file = base_dir.joinpath(in_folder, itemid2insert)
    delete_itemid_file = base_dir.joinpath(in_folder, itemid2delete)

    df_insert = pd.read_csv(insert_itemid_file, sep='\t', \
            header=0, encoding='utf-8')
    df_delete = pd.read_csv(delete_itemid_file, sep='\t', \
            header=0, encoding='utf-8')
    # Replace the double backslashes with single backslashes
    # https://stackoverflow.com/questions/44227748/
    # removing-newlines-from-messy-strings-in-pandas-dataframe-cells
    df_insert = df_insert.replace(to_replace=[r'\\t', r'\\n'], \
            value=['\t', '\n'], regex=True)
    df_delete = df_delete.replace(to_replace=[r'\\t', r'\\n'], \
            value=['\t', '\n'], regex=True)
    # Create a list containing the content of RecordingName column
    # https://stackoverflow.com/questions/22341271/
    # get-list-from-pandas-dataframe-column-or-row
    insert_list = df_insert['RecordingName'].tolist()
    delete_list = df_delete['RecordingName'].tolist()

    # List the _cod.cha files
    filenames = [f for f in base_dir.joinpath(in_folder) \
                .glob('*corrected*/*{}'.format('_cod.cha'))]

    # Browse the files listed in filenames
    for filename in filenames:
        out_path = filename.parents[1].joinpath( \
                str(filename.parts[-2])[0:3] \
                + '_inserted_deleted_itemID_' + timestamp)
        out_path.mkdir(parents=True, exist_ok=True)
        out_file = out_path.joinpath(filename.parts[-1])

        # Copy without modification the files not listed in insert_list
        # https://stackoverflow.com/questions/15343743/
        # copying-from-one-text-file-to-another-using-python/32977473
        if filename.stem[:-4] not in insert_list \
                and filename.stem[:-4] not in delete_list:
            with open(filename) as fs:
                with open(out_file, 'w') as fd:
                    for line in fs:
                        fd.write(line)
        # Copy with insertion of item IDs the files listed in insert_list
        else:
            # Load the transcription to a DataFrame with a dummy separator
            # (each line is loaded in only one column)
            # https://stackoverflow.com/questions/45443406/
            # python-pandas-read-csv-not-recognizing-t-in-tab-delimited-file
            df_transcription = pd.read_csv(filename, sep='\\\\t', \
                    header=None, names=['original_text'],
                    engine='python', encoding='utf-8')
            # Check that there is only one column in the DataFrame
            # https://stackoverflow.com/questions/20297332/
            # how-do-i-retrieve-the-number-of-columns-in-a-pandas-data-frame
            if len(df_transcription.columns) != 1:
                raise SystemExit("There is more than one column in your DataFrame.")
            # Add column with identical line cumulative count to get occurrence to replace
            # https://stackoverflow.com/questions/40900195/
            # pandas-cumulative-count
            df_transcription['occurrence_nb'] = df_transcription.groupby( \
                    'original_text').cumcount() + 1
            if filename.stem[:-4] in insert_list:
                # Keep only the rows corresponding to the filename
                df_corresponding_itemid = df_insert[df_insert[ \
                        'RecordingName'] == filename.stem[:-4]]
                # Merge the transcription DataFrame and the item IDs to insert DataFrame
                df = pd.merge(df_transcription, df_corresponding_itemid, how='left', on=[ \
                        'original_text', 'occurrence_nb'], ).drop(['RecordingName'], axis=1)
                # Create a column containing the final text
                # https://kanoki.org/2019/08/17/pandas-coalesce-replace-value-from-another-column/
                df['final_text'] = df['replacing_text'].mask( \
                        pd.isnull, df['original_text'])
            elif filename.stem[:-4] in delete_list:
                # Keep only the rows corresponding to the filename
                df_corresponding_itemid = df_delete[df_delete[ \
                        'RecordingName'] == filename.stem[:-4]]
                # Delete duplicate items
                # https://stackoverflow.com/questions/13851535/
                # how-to-delete-rows-from-a-pandas-dataframe-based-on-a-conditional-expression
                df = df_transcription.drop( \
                        df_transcription[(df_transcription['original_text'] \
                        == df_corresponding_itemid['original_text'].values[0]) \
                        & (df_transcription['occurrence_nb'] \
                        == df_corresponding_itemid['occurrence_nb'].values[0])].index)
                df = df.rename(columns={'original_text': 'final_text'})
            # Write the replaced final text to a .cha file
            # (used NumPy savetxt instead of Pandas to_csv to avoid quoting issues)
            # https://stackoverflow.com/questions/31247198/
            # python-pandas-write-content-of-dataframe-into-text-file
            np.savetxt(out_file, df['final_text'].values, fmt='%s', \
                    encoding='utf-8')

def insert_training_itemid(in_folder):
    # List the _cod.cha files
    filenames = [f for f in base_dir.joinpath(in_folder)
                .glob('*inserted_deleted_itemID*/*{}'.format('_cod.cha'))]

    # Browse the files listed in filenames
    for filename in filenames:
        out_path = filename.parents[1].joinpath(str(filename.parts[-2])[
                0:3] + '_inserted_training_itemID_' + timestamp)
        out_path.mkdir(parents=True, exist_ok=True)
        out_file = out_path.joinpath(filename.parts[-1])

        flag = False

        with open(filename) as fs:
            # Count the number of item IDs (lines beginning with '@G;')
            # https://stackoverflow.com/questions/18439709/
            # how-to-find-a-string-in-a-text-file-python
            itemid_nb = fs.read().count('\n@G:')

        with open(filename) as fs:
            with open(out_file, 'w') as fd:
                for line in fs:
                    # Process files for Cat or Mem experiment with training item
                    if (filename.parts[-2].startswith('Cat') and itemid_nb == 37) \
                            or (filename.parts[-2].startswith('Mem') and itemid_nb == 11):
                        # Correct training item typo if necessary
                        if line.startswith('@G:\tTraining') \
                                and line != '@G:\tTraining_item\n':
                            line_training = '@G:\tTraining_item\n'
                            fd.write(line_training)
                        # Copy without change otherwise
                        else:
                            fd.write(line)
                    # Copy with insertion of training item ID
                    else:
                        # Insert training item ID before first item ID
                        if line == '@G:\n' and not flag:
                            line = '@G:\n' + line
                            fd.write(line)
                            flag = True
                        elif line == '@G:\tTarget1\n' and not flag:
                            line = '@G:\tTraining_item\n' + line
                            fd.write(line)
                            flag = True
                        else :
                            fd.write(line)

        # Check number of item IDs
        with open(out_file, 'r') as fd:
            itemid_nb_after_modif = fd.read().count('\n@G:')
            if (filename.parts[-2].startswith('Cat') \
                    and itemid_nb_after_modif != 37) \
                    or (filename.parts[-2].startswith('Mem') \
                    and itemid_nb_after_modif != 11):
                print(filename.stem + ' ' + str(itemid_nb_after_modif))

def insert_transcription_itemid(in_folder, cat_codes, mem_codes):
    # Load the .txt files listing the transcription codes for Cat and Mem
    cat_codes_file = base_dir.joinpath(in_folder, cat_codes)
    df_cat_codes = pd.read_csv(cat_codes_file, sep='\\\\t', \
            header=None, names=['transcription_codes'], \
            engine='python', encoding='utf-8')

    mem_codes_file = base_dir.joinpath(in_folder, mem_codes)
    df_mem_codes = pd.read_csv(mem_codes_file, sep='\\\\t', \
            header=None, names=['transcription_codes'], \
            engine='python', encoding='utf-8')

    # List the _cod.cha files
    filenames = [f for f in base_dir.joinpath(in_folder) \
                .glob('*inserted_training_itemID*/*{}'.format('_cod.cha'))]

    # Browse the files listed in filenames
    for filename in filenames:
        out_path = filename.parents[1].joinpath(str(filename.parts[-2])[ \
                0:3] + '_inserted_transcription_itemID_' + timestamp)
        out_path.mkdir(parents=True, exist_ok=True)
        out_file = out_path.joinpath(filename.parts[-1])

        replaced_nb = 0

        if filename.parts[-2][0:3] == 'Cat':
            df_codes = df_cat_codes
            transcription_nb = 37
        elif filename.parts[-2][0:3] == 'Mem':
            df_codes = df_mem_codes
            transcription_nb = 11

        with open(filename) as fs:
            with open(out_file, 'w') as fd:
                for line in fs:
                    if line == '@G:\n':
                        line = df_codes['transcription_codes'][ \
                                replaced_nb] + '\n'
                        fd.write(line)
                        replaced_nb += 1
                    else:
                        fd.write(line)

        if replaced_nb != 0 and replaced_nb != transcription_nb:
            message = "Error for '{}'. {} changed instead of {}".format( \
                    filename.stem[:-4], replaced_nb, transcription_nb)
            print_save_log(message, Path(__file__).stem, \
                    print_logs, save_logs)
        elif replaced_nb == transcription_nb:
            message = "Transcription itemIDs inserted for '{}'".format( \
                    filename.parts[-1])
            print_save_log(message, Path(__file__).stem, \
                    print_logs, save_logs)

        # Count the number of @G:\tTraining_item in each transcription file
        # https://stackoverflow.com/questions/23232248/
        # python-3-4-counting-occurrences-in-a-txt-file
        training_nb = open(str(out_file), 'r').read().count('@G:\tTraining_item\n')
        if training_nb != 1:
            message = "Error for '{}'. There is {} Training_item".format( \
                    filename.stem[:-4], training_nb)
            print_save_log(message, Path(__file__).stem, \
                    print_logs, save_logs)
