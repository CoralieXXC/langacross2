#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no is_file member"
# for 'Path([...]).is_file()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member

from pathlib import Path
import pandas as pd

from config import base_dir, timestamp
from common_functions import common_functions as cf
from common_functions import load_files as lf

def get_unproduced_items(in_folder='Eye-tracking/0c_Compliant_Tobiitsv',
                         out_folder='reports/Unproduced_items',
                         apply_filters=False):
    """Create .tsv files with the duration of production items.
            Generate one file per experiment.

    Args:
        in_folder (str, optional): folder where are the .tsv files to
                process.
        out_folder (str, optional): folder where the resulting .tsv
                report files are stored (in /expe/*task*/ subfolders).
                Defaults to 'reports/'.
        apply_filters (bool, optional): True to process only the files
                and data specified in filter_dict (in config.py).
                Defaults to False.
    """
    # Load the.tsv file containing event times by media
    df_timings = lf.timings2df()

    # Create an empty list to store all data
    all_data = []

    # List the .tsv or .xlsx files to process after checking arguments 
    # and assigning default filter values
    filenames = lf.list_files(in_folder, '.tsv', apply_filters)
    # Keep only 'Cat' and 'Mem' tasks
    filenames = [f for f in filenames if 'Prod' in str(f.parts[-2])]

    # Browse the files listed in filenames
    for filename in filenames:
        # # Get the experiment name based on expe_task or filename
        # if apply_filters:
        #     include_expe = expe_task['include_expe']
        # else:
        #     include_expe = filename.parts[-3]

        # Load data from the current file
        data = lf.Tobiitsv2df(filename)

        # Delete unused columns
        data = data.drop(columns=['[Age_subgroup]Value',
                '[Condition]Value',
                '[Language]Value', '[Prod]Value', '[ProdET]Value',
                '[Validity]Value', 'RecordingResolution',
                'LocalTimeStamp', 'GazePointX (MCSpx)',
                'GazePointY (MCSpx)', 'ValidityLeft', 'ValidityRight'])
        if 'DC1-3 DC2-2' in str(filename.parts[-2]):
            data = data.drop(columns=['[Cat]Value', '[CatET]Value'])
        elif 'DC3-3 DC4-2' in str(filename.parts[-2]):
            data = data.drop(columns=['[Mem]Value', '[MemET]Value'])

        # Get media names
        media_names = data.loc[data['StudioEvent'] \
                == 'MovieStart', 'MediaName']

        for media_name in media_names:
            # Define limits of the time interval
            start_limit = 'MovieStart'
            end_limit = 'MovieEnd'
            # Keep data from the defined interval
            interval_data, _, _ = cf.keep_interval_data(
                    data, media_name, df_timings, start_limit, end_limit,
                    'not_strict', 'Prod')

            # Compute item duration between movie start and movie end
            movie_start_timestamp = interval_data['RecordingTimestamp'].iloc[0]
            movie_end_timestamp = interval_data['RecordingTimestamp'].iloc[-1]
            item_duration = int(movie_end_timestamp) - int(movie_start_timestamp)

            recording_name = data['RecordingName'].iloc[0]

            all_data.append((recording_name, media_name, item_duration))
        
        print(str(filename) + ' done')

    cols = ['RecordingName', 'MediaName', 'item_duration']
    data = pd.DataFrame(all_data, columns=cols)
    # Order dataframe to get the 3 blocks of each participant following each other
    # https://stackoverflow.com/questions/20277358/
    # sort-pandas-dataframe-both-on-values-of-a-column-and-index/50872695#50872695
    data = data.set_index('RecordingName', append=True).sort_index( \
            level=1).reset_index(level=1)

    # Create distinct DataFrames for each expe, one with all data, 
    # the other with unproduced data
    cat_data_df = data[data['MediaName'].str.contains('P_')]
    cat_data_unproduced_df = cat_data_df[cat_data_df['item_duration'] <= 4800]
    mem_data_df = data[data['MediaName'].str.contains('ProdMem_')]
    mem_data_unproduced_df = mem_data_df[mem_data_df['item_duration'] <= 4800]

    if cat_data_df.shape[0] + mem_data_df.shape[0] != data.shape[0]:
        print('Error: There is a problem to split the results between' \
                + 'Cat and Mem expe')

    # Create the subdirectory where the output file is stored
    out_path = base_dir.joinpath(out_folder)
    out_path.mkdir(parents=True, exist_ok=True)

    # Write DataFrames to .tsv file
    if cat_data_df.shape[0] != 0:
        cat_data_df.to_csv(out_path \
                .joinpath(timestamp + '_Cat (DC1-DC2).tsv'),
                sep='\t', header=True, index=False, encoding='utf-8')
    if cat_data_unproduced_df.shape[0] != 0:
        cat_data_unproduced_df.to_csv(out_path \
                .joinpath(timestamp + '_Cat (DC1-DC2)_unproduced.tsv'),
                sep='\t', header=True, index=False, encoding='utf-8')
    if mem_data_df.shape[0] != 0:
        mem_data_df.to_csv(out_path \
                .joinpath(timestamp + '_Mem (DC3-DC4).tsv'),
                sep='\t', header=True, index=False, encoding='utf-8')
    if mem_data_unproduced_df.shape[0] != 0:
        mem_data_unproduced_df.to_csv(out_path \
                .joinpath(timestamp + '_Mem (DC3-DC4)_unproduced.tsv'),
                sep='\t', header=True, index=False, encoding='utf-8')

def create_unproduced_report(in_folder='Production/reports/Unproduced_items',
                            out_folder='Production/reports/Unproduced_items'):
    """Create .tsv files with unproduced items (viewing time too short)

    Args:
        in_folder (str, optional): [description].
                Defaults to 'Production/reports/Unproduced_items'.
        out_folder (str, optional): [description].
                Defaults to 'Production/reports/Unproduced_items'.
    """

    # List the .tsv files to process containing all data (not 'unproduced')
    filenames = [f for f in base_dir.joinpath(in_folder)
                .glob('*).tsv')]

    # Browse the files listed in filenames
    for filename in filenames:
        # Load data from the current file
        data = lf.Tobiitsv2df(filename)
        data['prev_rec'] = data['RecordingName'].shift(1)
        data['prev_media'] = data['MediaName'].shift(1)
        data['prev_duration'] = data['item_duration'].shift(1)
        # Add original_text from second row
        # https://stackoverflow.com/questions/65946746/
        # df-apply-but-skip-the-first-row
        data['original_text'] = data[1:].apply(lambda row: '@G:\t' + row['MediaName'][ \
                row['MediaName'].find('_')+1:-4] \
                if row['RecordingName'] == row['prev_rec'] \
                else '@End', axis=1)
        # Add column with RecordingName cumulative count to get occurrence to replace
        # https://stackoverflow.com/questions/40900195/
        # pandas-cumulative-count
        data['item_occurrence'] = data.groupby('RecordingName').cumcount() + 1
        # Add replacing_text from second row for unseen items (too short)
        data['replacing_text'] = data[1:].apply(lambda row: '@G:\t' + row['prev_media'][ \
                row['prev_media'].find('_')+1:-4] + '\n' + row['original_text'] \
                if row['prev_duration'] <= 4800 else '', axis=1)
        # Rearrange columns ('occurrence' as 2nd column)
        # https://stackoverflow.com/questions/35321812/
        # move-column-in-pandas-dataframe
        data = data[['RecordingName', 'item_occurrence'] + [ \
                col for col in data if col not in [ \
                'RecordingName', 'item_occurrence']]]

        # Create a DataFrame with unproduced data:
        data_unproduced = data[data['prev_duration'] <= 4800]

        # Create the subdirectory where the output file is stored
        out_path = base_dir.joinpath(out_folder)
        out_path.mkdir(parents=True, exist_ok=True)

        # Write DataFrames to .tsv file
        data.to_csv(out_path \
                .joinpath(filename.stem + '_to_replace.tsv'),
                sep='\t', header=True, index=False, encoding='utf-8')
        if data_unproduced.shape[0] != 0:
            data_unproduced.to_csv(out_path \
                    .joinpath(filename.stem + '_unproduced_to_replace.tsv'),
                    sep='\t', header=True, index=False, encoding='utf-8')
