#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no is_file member"
# for 'Path([...]).is_file()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member

from pathlib import Path
import pandas as pd

from config import base_dir, timestamp, manner_dict, path_dict, mem_choice_dict
from common_functions import common_functions as cf
from common_functions import load_files as lf

def get_item_type(media_name):
    """Return the item type from the name of the media

    Args:
        media_name (str): name of the media.

    Returns:
        item_type (str): 'Contrôle', 'Distracteur' or 'Stimulus'
    """
    if media_name.startswith('NVCat'):
        if media_name.split('_')[1][-1] == 'C':
            item_type = 'Contrôle'
        elif media_name.split('_')[1][-1] == 'D':
            item_type = 'Distracteur'
        else:
            item_type = 'Stimulus'
    else:
        item_type = 'Contrôle'
    return item_type

def get_manner_path(filename, media_name, item_type):
    """Return Manner and Path for target and choices for Cat and Mem

    Args:
        filename (Path): Path of the .tsv file.
        media_name (str): name of the media.
        item_type (str): type of the item.

    Returns:
        manner (dict): dictionary of the manner for target and choices
        path (dict): dictionary of the path for target and choices
    """
    manner = {}
    path = {}
    if item_type == 'Distracteur':
        # No Manner and Path for 'Distracteur'
        manner['target'] = ''
        path['target'] = ''
        manner['choiceL'] = ''
        path['choiceL'] = ''
        manner['choiceR'] = ''
        path['choiceR'] = ''
    # Get Manner and Path for 'Stimulus' and 'Contrôle'
    elif item_type == 'Stimulus' or item_type == 'Contrôle':
        # For target ('Cat')
        if 'Cat Expe' in str(filename.parts[-2]):
            manner['target'] = manner_dict[media_name.split('_')[2][-10]]
            path['target'] = path_dict[media_name.split('_')[2][-9]]
        elif 'Phase2' in str(filename.parts[-2]):
            manner['target'] = manner_dict[mem_choice_dict[media_name] \
                    .split('_')[2][-6]]
            path['target'] = path_dict[mem_choice_dict[media_name] \
                    .split('_')[2][-5]]
        else:
            manner['target'] = 'Error'
            path['target'] = 'Error'
        # For choices ('Cat' and 'Mem')
        manner['choiceL'] = manner_dict[media_name.split('_')[2][-8]]
        path['choiceL'] = path_dict[media_name.split('_')[2][-7]]
        manner['choiceR'] = manner_dict[media_name.split('_')[2][-6]]
        path['choiceR'] = path_dict[media_name.split('_')[2][-5]]
    return manner, path

def get_mem_match(filename, manner, path):
    """Return matching element for Mem

    Args:
        filename (Path): Path of the .tsv file.
        manner (dict): dictionary of the manner for target and choices
        path (dict): dictionary of the path for target and choices

    Returns:
        mem_match (str): 'Man_match' (resp. 'Path_match') if manner
                (resp. path) is the same between the target and the
                erroneous choice
    """
    if 'Cat Expe' in str(filename.parts[-2]):
        mem_match = ''
    elif 'Phase2' in str(filename.parts[-2]):
        if (manner['choiceL'] == manner['target']) \
                and (manner['choiceR'] == manner['target']):
            mem_match = 'Man_match'
        elif (path['choiceL'] == path['target']) \
                and (path['choiceR'] == path['target']):
            mem_match = 'Path_match'
    else:
        mem_match = 'Error'
    return mem_match

def get_response_value_time(media_name, interval_data, item_type, manner,
        path, mouse_timestamp, df_timings):
    """Return mouse click, response and control value and response time
            from end of choice fixation cross

    Args:
        media_name (str): name of the media.
        interval_data (DataFrame): data between MovieStart and MovieEnd.
        item_type (str): type of the item.
        manner (dict): dictionary of the manner for target and choices.
        path (dict): dictionary of the path for target and choices.
        mouse_timestamp (None or int64): mouse timestamp.
        df_timings (DataFrame): event times by media.

    Returns:
        mouse_click (str): 'Left', 'Right', 'Early click' or 'No click'
        response_value (str): 'Manner', 'Path' (Cat Stimulus),
                'Man_err', 'Path_err' (Mem Contrôle),
                '' (Cat or Mem Contrôle, Cat Distracteur), 'Error'
        control_value (int): 1 if the control is validated, 0 otherwise
        response_time (int): response time from ChoiceFixCrossEnd (ms)
    """
    mouse_click = interval_data['MouseEvent'].iloc[-1]
    if mouse_timestamp is not None \
            and (mouse_click == 'Left'
            or mouse_click == 'Right'):
        response_time = int(mouse_timestamp - interval_data[
                'RecordingTimestamp'].iloc[0] - df_timings[df_timings[
                'MovieName']==media_name]['ChoiceFixCrossEnd'] \
                .values[0])
        if response_time <= 0:
            mouse_click = 'Early click'
            response_value = ''
            control_value = 0
            response_time = ''
        elif item_type == 'Stimulus' \
                and ((mouse_click == 'Left' \
                and manner['target'] == manner['choiceL'])
                or (mouse_click == 'Right' \
                and manner['target'] == manner['choiceR'])):
            response_value = 'Manner'
            control_value = 0
        elif item_type == 'Stimulus' \
                and ((mouse_click == 'Left' \
                and path['target'] == path['choiceL'])
                or (mouse_click == 'Right' \
                and path['target'] == path['choiceR'])):
            response_value = 'Path'
            control_value = 0
        elif item_type == 'Contrôle' \
                and ((mouse_click == 'Left'
                and manner['target'] == manner['choiceL'] \
                and path['target'] == path['choiceL']) \
                or (mouse_click == 'Right'
                and manner['target'] == manner['choiceR'] \
                and path['target'] == path['choiceR'])):
            # # if media_name.startswith('Mem'):
            # if manner['choiceL'] == manner['choiceR']:
            #     response_value = 'Man_sim'
            # elif path['choiceL'] == path['choiceR']:
            #     response_value = 'Path_sim'
            # # elif media_name.startswith('NVCat'):
            response_value = ''
            control_value = 1
        elif item_type == 'Contrôle' \
                and ((mouse_click == 'Left' \
                and manner['target'] == manner['choiceL'])
                or (mouse_click == 'Right'
                and manner['target'] == manner['choiceR'])):
            response_value = 'Path_err'
            control_value = 0
        elif item_type == 'Contrôle' \
                and ((mouse_click == 'Left' \
                and path['target'] == path['choiceL'])
                or (mouse_click == 'Right'
                and path['target'] == path['choiceR'])):
            response_value = 'Man_err'
            control_value = 0
        elif item_type == 'Distracteur':
            response_value = ''
            control_value = 0
        else:
            response_value = 'Error'
            control_value = 0
    elif mouse_timestamp is not None \
            and mouse_click == '0':
        mouse_click = 'Late click'
        response_value = ''
        control_value = 0
        response_time = ''
    elif mouse_timestamp is None \
            and mouse_click == '0':
        mouse_click = 'No click'
        response_value = ''
        control_value = 0
        response_time = ''
    else:
        response_value = 'Error'
        control_value = 0
        response_time = ''
    return mouse_click, response_value, control_value, response_time

def get_control_status_age_condition_language(row):
    """Add control, status, age, condition and language columns

    Args:
        row (Series): data for one row from DataFrame

    Returns:
        row (Series): row with additional columns
    """
    if 'NVCat' in row['MediaName']:
        row['Control_%'] = round(row['sum'] / (row['count'] / 6) * 100, 2)
    elif 'Mem' in row['MediaName']:
        row['Control_%'] = row['sum'] / row['count'] * 100
    if row['Control_%'] > 50:
        row['Status'] = 'Included'
    elif row['Control_%'] <= 50:
        row['Status'] = 'Excluded'

    if 'A1' in row['RecordingName']:
        row['Age'] = 'A1'
    elif 'A2' in row['RecordingName']:
        row['Age'] = 'A2'
    elif 'ad' in row['RecordingName']:
        row['Age'] = 'A3'
    else:
        row['Age'] = 'Error'

    if 'DC1' in row['RecordingName'] or 'DC3' in row['RecordingName']:
        row['Condition'] = 'NonVerbal'
    elif 'DC2' in row['RecordingName'] or 'DC4' in row['RecordingName']:
        row['Condition'] = 'Verbal'
    else:
        row['Condition'] = 'Error'

    if row['RecordingName'].startswith('E'):
        row['Language'] = 'English'
    elif row['RecordingName'].startswith('F'):
        row['Language'] = 'French'
    else:
        row['Language'] = 'Error'

    return row

def response_time_value(in_folder='0c_Compliant_Tobiitsv',
                             out_folder='reports/Response_time_value',
                             apply_filters=False):
    """Create .tsv files summarizing the response times and values
            in 'reports/Response_time_value' for .tsv files containing
            data for Cat or Mem experiment.
            Valid 'Gaze Data': ValidityLeft < 2 | ValidityRight < 2
            Generate one file per expe (tasks 'Cat' and 'Mem')

    Args:
        in_folder (str, optional): folder where are the .tsv files to
                process.
        out_folder (str, optional): folder where the resulting .tsv
                report files are stored (in /expe/*task*/ subfolders).
                Defaults to 'reports/Validity_samples'.
        apply_filters (bool, optional): True to process only the files
                and data specified in filter_dict (in config.py).
                Defaults to False.
    """
    # Load the.tsv file containing event times by media
    df_timings = lf.timings2df()

    # Create an empty list to store all data
    all_data = []

    # List the .tsv or .xlsx files to process after checking arguments 
    # and assigning default filter values
    filenames = lf.list_files(in_folder, '.tsv', apply_filters)
    # Keep only 'Cat' and 'Mem' tasks
    filenames = [f for f in filenames if 'Cat Expe' in str(f.parts[-2])
            or 'Phase2' in str(f.parts[-2])]

    # Browse the files listed in filenames
    for filename in filenames:
        # # Get the experiment name based on expe_task or filename
        # if apply_filters:
        #     include_expe = expe_task['include_expe']
        # else:
        #     include_expe = filename.parts[-3]

        # Load data from the current file
        data = lf.Tobiitsv2df(filename)

        # Delete unused columns
        data = data.drop(columns=['[Age_subgroup]Value',
                '[Condition]Value',
                '[Language]Value', '[Prod]Value', '[ProdET]Value',
                '[Validity]Value', 'RecordingResolution',
                'LocalTimeStamp', 'GazePointX (MCSpx)',
                'GazePointY (MCSpx)', 'ValidityLeft', 'ValidityRight'])
        if 'Cat Expe' in str(filename.parts[-2]):
            data = data.drop(columns=['[Cat]Value', '[CatET]Value'])
        elif 'Phase2' in str(filename.parts[-2]):
            data = data.drop(columns=['[Mem]Value', '[MemET]Value'])

        # Get media names
        media_names = data.loc[data['StudioEvent'] \
                == 'MovieStart', 'MediaName']

        for media_name in media_names:
            # Define limits of the time interval
            start_limit = 'MovieStart'
            end_limit = 'MovieEnd'
            # Keep data from the defined interval
            interval_data, _, mouse_timestamp = cf.keep_interval_data(
                    data, media_name, df_timings, start_limit, end_limit,
                    'not_strict')

            ######### Add useful columns #########
            # Get item type
            item_type = get_item_type(media_name)

            # Get Manner and Path for 'Stimulus' and 'Contrôle'
            manner, path = get_manner_path(filename, media_name, item_type)

            # Get match for 'Mem'
            mem_match = get_mem_match(filename, manner, path)

            # Compute response value and time from end of choice fixation cross
            mouse_click, response_value, control_value, response_time = \
                    get_response_value_time(media_name, interval_data,
                    item_type, manner, path, mouse_timestamp, df_timings)
            ######### End add useful columns #########

            recording_name = data['RecordingName'].iloc[0]
            target_name = (mem_choice_dict[media_name] if 'Phase2' in str(filename.parts[-2]) else '')

            all_data.append((recording_name,
            target_name, media_name, item_type,
            manner['target'], path['target'], 
            manner['choiceL'], path['choiceL'],
            manner['choiceR'], path['choiceR'], mem_match,
            mouse_click, response_value, int(control_value), response_time))

    cols = ['RecordingName', 'Target_name', 'MediaName', 'Item_type',
    'Man_target', 'Path_target', 'Man_left', 'Path_left',
    'Man_right', 'Path_right', 'Mem_match', 'Mouse_click',
    'Response_value', 'Control_value', 'Response_time']
    all_data_df = pd.DataFrame(all_data, columns=cols)

    # Create an empty DataFrame to store total of control items per participant
    controle_df = pd.DataFrame()
    # Compute total of valid control items per participant
    # https://stackoverflow.com/questions/38174155/
    # group-dataframe-and-get-sum-and-count
    controle_df = all_data_df.groupby('RecordingName')['Control_value'].agg([
            'sum','count']).reset_index()
    # Add to all_data_df columns with 1) the total of valid control items
    # and 2) the number of items
    all_data_df = pd.merge(all_data_df, controle_df, how='left', on=['RecordingName'])

    # Add participant
    # 1) percentage success for control, status (Include or Exclude)
    # https://stackoverflow.com/questions/26886653/
    # pandas-create-new-column-based-on-values-from-other-columns-apply-a-function-o
    # 2) age, condition and language
    # https://stackoverflow.com/questions/23586510/
    # return-multiple-columns-from-pandas-apply
    all_data_df = all_data_df.apply(
            get_control_status_age_condition_language, axis=1)

    # Specify columns 'sum' and 'count' name
    all_data_df = all_data_df.rename(columns={'sum': 'Control_tot',
            'count': 'Item_nb'})

    # Create distinct DataFrames for each expe:
    cat_data_df = all_data_df[all_data_df['MediaName'].str.contains('NVCat')]
    mem_data_df = all_data_df[all_data_df['MediaName'].str.contains('Mem')]

    if cat_data_df.shape[0] + mem_data_df.shape[0] != all_data_df.shape[0]:
        print('Error: There is a problem to split the results between' \
                + 'Cat and Mem expe')

    # Create the subdirectory where the output file is stored
    out_path = base_dir.joinpath(out_folder)
    out_path.mkdir(parents=True, exist_ok=True)

    # Write DataFrames to .tsv file
    if cat_data_df.shape[0] != 0:
        cat_data_df.to_csv(out_path \
                .joinpath(timestamp + '_Cat (DC1-DC2).tsv'),
                sep='\t', header=True, index=False, encoding='utf-8')
    if mem_data_df.shape[0] != 0:
        mem_data_df.to_csv(out_path \
                .joinpath(timestamp + '_Mem (DC3-DC4).tsv'),
                sep='\t', header=True, index=False, encoding='utf-8')
