#!/usr/bin/env python
# -*- coding: utf-8 -*-

# From "How to organize your Python data science project"
# https://gist.github.com/ericmjl/27e50331f24db3e8f957d1fe7bbbe510
# Also useful
# https://docs.python.org/3/tutorial/modules.html

""" Project structure
base_dir (relative path of the project)
├───code
│   ├───common_functions
│   ├───Eye_tracking
│   ├───Production
│   ├───Response_time_value
│   ├───.gitignore
│   ├───config.py (this file)
│   ├───README.md
│   └───run_process.py (perform data analysis step by step)
├───data (stored in /*in_folder*/include_expe/*include_task*/ subfolders)
│   ├───audio
│   ├───eye-tracking
│   ├───transcriptions
│   └───.gitignore (for transcriptions)
├───logs
├───reports
└───stimuli (.avi movie files stored in /include_task/ subfolders)
"""

import os
from pathlib import Path  # pathlib is seriously awesome!
from datetime import datetime

# https://stackoverflow.com/questions/2235173/
# file-name-path-name-base-name-naming-standard-for-pieces-of-a-path
base_dir = Path('..')

ffmpeg_path_config = 'C:/Program Files (x86)/ffmpeg/bin/ffmpeg.exe'

###############################################################################
#############################   Load and filter   #############################
###############################################################################
# expe_task (dict): to include experiment and task
    # 'include_expe': 'Cat (DC1-DC2)' (NVCat-VCat)
    # or 'Mem (DC3-DC4)' (NVMem-VMem)
    # 'include_task' (optional): 'Cat' or 'Mem' or 'Prod'
    # or 'View' ('View': NVMem 1st phase)
expe_task = {
    'include_expe': 'Mem (DC3-DC4)', # Cat (DC1-DC2)
    'include_task': 'Prod' # 'Cat', 'Mem', Prod', 'View'
}

# filter_dict (dict): to exclude or include categories (age, condition, 
# language) and/or list of Media or Participant (in a .tsv file
# or in list values)
    # 'status': '' (no filter whatever values follow) or 'exclude' or 'include'
    # 'age': '' (no age) or 'adults' or 'children'
    # 'condition': '' (no condition) or 'NonVerbal' or 'Verbal'
    # 'language': '' (no language) 'english' or 'french'
    # 'file_list': .tsv filename containing media and/or participants name
    # Should be located in the base_dir folder
    # Prevails on 'participant_list' and 'media_list'
    # 'participant_list': list of participants,
    # 'media_list': list of media,
filter_dict = {
    'status': 'include',
    # 'age': 'adults',
    # 'condition': 'NonVerbal',
    # 'language': 'english',
    # 'file_list': 'Filtered_data.tsv',
    # 'participant_list': ['Ead13_DC1', 'Ech04_A1_DC1', 'Ech04_A1_DC2', 'Ech05_A1_DC1', \
    #       'Ech05_A1_DC2', 'Ech07_A2_DC2', 'Ech09_A2_DC2', 'Ech13_A2_DC1', 'Ech15_A2_DC1', \
    #       'Fad08_DC1', 'Fad17_DC2', 'Fad20_DC1', 'Fch02_A1_DC1'], # 'Ead01_DC1'
    'participant_list': [], # 'Ead01_DC1'
    # 'media_list': ['P_B1-2D_MS', 'P_B1-8D_MS', 'P_B2-2D_ML', 'P_B2-8D_MT', 'P_B3-2D_PT', 'P_B3-8D_MC']
    # 'media_list': ['NVCat_B1-2D_MSESME', 'NVCat_B1-8D_MSESMB', \
    #         'NVCat_B2-2D_MLELMP', 'NVCat_B2-8D_MTMTET', \
    #         'NVCat_B3-2D_PTPVTT', 'NVCat_B3-8D_MCMTEC']
    # 'media_list': ['NVCat_B1-2D_MSESME', 'NVCat_B1-8D_MSESMB']
    # 'media_list': ['NVCat_B2-2D_MLELMP', 'NVCat_B2-8D_MTMTET']
    'media_list': [ #'ProdMem_G1-2_VM', 'ProdMem_G1-8_SE']
            # 'ProdMem_G2-2_VD', 'ProdMem_G2-8_SS']
            # 'ProdMem_G3-2_TT', 'ProdMem_G3-8_ST']
            'ProdMem_G4-2_TD', 'ProdMem_G4-8_MT']
}

###############################################################################
###################################   Save   ##################################
###############################################################################
# timestamp (str): timestamp used in timestamped saved filenames
timestamp = datetime.now().strftime('%Y-%m-%d_%H%M%S')
# save_logs (bool, optional): True to export .log files (config, run_process 
# and optionnaly stdout of the running script)
print_logs = True
save_logs = True
# out_logs_path (Path): path where the logs are saved
out_logs_path = base_dir.joinpath('logs')

###############################################################################
###########################   Useful dictionaries   ###########################
###############################################################################
# manner_dict (dict): dictionary with full word from Manner abbreviation
manner_dict = {'C': 'Courir', 'M': 'Marcher', 'R': 'Rollers',
    'S': 'Sauter', 'T': 'Trottinette', 'V': 'Vélo'
}
# path_dict (dict): dictionary with full word from Path abbreviation
path_dict = {'D': 'Descendre', 'E': 'Entrer', 'L': 'Longer',
    'M': 'Monter', 'S': 'Sortir', 'T': 'Traverser'
}
mem_choice_dict = {
    'Mem_G1-1_RETE.avi': 'NVMem_G1-1_TE.avi',
    'Mem_G1-2_VDVM.avi': 'NVMem_G1-2_VM.avi',
    'Mem_G1-3_VSTS.avi': 'NVMem_G1-3_VS.avi',
    'Mem_G1-4_RTRL.avi': 'NVMem_G1-4_RL.avi',
    'Mem_G1-5_CSMS.avi': 'NVMem_G1-5_MS.avi',
    'Mem_G1-6_MLMT.avi': 'NVMem_G1-6_ML.avi',
    'Mem_G1-7_SLCL.avi': 'NVMem_G1-7_SL.avi',
    'Mem_G1-8_SESS.avi': 'NVMem_G1-8_SE.avi',
    'Mem_G1-9_SMCM.avi': 'NVMem_G1-9_CM.avi',
    'Mem_G1-10_MDMM.avi': 'NVMem_G1-10_MD.avi',
    'Mem_G2-1_TMRM.avi': 'NVMem_G2-1_TM.avi',
    'Mem_G2-2_VMVD.avi': 'NVMem_G2-2_VD.avi',
    'Mem_G2-3_TEVE.avi': 'NVMem_G2-3_VE.avi',
    'Mem_G2-4_VTRT.avi': 'NVMem_G2-4_RT.avi',
    'Mem_G2-5_MSME.avi': 'NVMem_G2-5_ME.avi',
    'Mem_G2-6_CLML.avi': 'NVMem_G2-6_CL.avi',
    'Mem_G2-7_MTCT.avi': 'NVMem_G2-7_CT.avi',
    'Mem_G2-8_SSSE.avi': 'NVMem_G2-8_SS.avi',
    'Mem_G2-9_CDSD.avi': 'NVMem_G2-9_CD.avi',
    'Mem_G2-10_MMMD.avi': 'NVMem_G2-10_MM.avi',
    'Mem_G3-1_TSTE.avi': 'NVMem_G3-1_TS.avi',
    'Mem_G3-2_TTRT.avi': 'NVMem_G3-2_TT.avi',
    'Mem_G3-3_VTVL.avi': 'NVMem_G3-3_VL.avi',
    'Mem_G3-4_TDRD.avi': 'NVMem_G3-4_RD.avi',
    'Mem_G3-5_RSRE.avi': 'NVMem_G3-5_RE.avi',
    'Mem_G3-6_CEME.avi': 'NVMem_G3-6_CE.avi',
    'Mem_G3-7_MDMM.avi': 'NVMem_G3-7_MM.avi',
    'Mem_G3-8_STCT.avi': 'NVMem_G3-8_ST.avi',
    'Mem_G3-9_SDSM.avi': 'NVMem_G3-9_SD.avi',
    'Mem_G3-10_CSMS.avi': 'NVMem_G3-10_MS.avi',
    'Mem_G4-1_TLTT.avi': 'NVMem_G4-1_TL.avi',
    'Mem_G4-2_VDTD.avi': 'NVMem_G4-2_TD.avi',
    'Mem_G4-3_VTVL.avi': 'NVMem_G4-3_VT.avi',
    'Mem_G4-4_RMVM.avi': 'NVMem_G4-4_RM.avi',
    'Mem_G4-5_RERS.avi': 'NVMem_G4-5_RS.avi',
    'Mem_G4-6_SSCS.avi': 'NVMem_G4-6_CS.avi',
    'Mem_G4-7_MMMD.avi': 'NVMem_G4-7_MD.avi',
    'Mem_G4-8_MLMT.avi': 'NVMem_G4-8_MT.avi',
    'Mem_G4-9_SMSD.avi': 'NVMem_G4-9_SM.avi',
    'Mem_G4-10_MECE.avi': 'NVMem_G4-10_ME.avi'
}
E_manner_dict = {
    'C': [' jog', ' ran ', 'run', ' sprint'],
    'M': [' cross', '\tcross', ' strid', ' stroll', ' walk', '\twalk'],
    'R': ["I don't know", ' roll', '\troll', ' skat', '\tskat'],
    'S': [' bounc', ' bunnyhop', ' hop', '\thop', ' jump', '\tjump', ' kang', ' leap', ' skip'],
    'T': [' scoot', '\tscoot', '-scoot'],
    'V': [' bic', ' bik', ' cycl', '\tcycl', ' riding ']
}
E_path_dict = {
    'D': [' down '],
    'E': [' back home', ' exit', ' in ', ' into ', ' leaving',
            ' out of a room ', ' through a door'],
    'L': [' along ', ' down a path', ' down a pavement ',
            ' through a park ', ' through the park '],
    'M': [' ascend', ' climbing', 'to the top', ' up '],
    'S': [' enter', ' exit', ' into a room ', ' leaving', ' out ', ' through a door'],
    'T': [' across ', ' crosses', ' crossing', ' perpendicular to a path ']
}
E_distr_dict = {
    'B1-2D_MS': [' cup ', ' pour', ' put', ' salt ', 'sugar'],
    'B1-8D_MS': [' bag ', ' car ', ' plac', ' put', ' toy '],
    'B2-2D_ML': [' block', ' brick ', ' duplo ', ' lego', ' two '],
    'B2-8D_MT': [' circ', ' disc ', 'hoop', ' on top of the others ',
            ' ring', ' stack ', ' stick ', ' toy '],
    'B3-2D_PT': [' push', ' train '],
    'B3-8D_MC': ['cup ', ' lid ', ' pot ']
}

F_manner_dict = {
    'C': [' cour', '\tcour', ' en trottinant', ' footing', ' gambad',
            ' jogging', ' pressé ', ' trottine ', ' vite '],
    'M': ['march', 'se prom', ' travers'],
    'R': [' patin', ' roll', '\troll'],
    'S': ['à cloche pied', ' bond', ' kang', ' pieds joints', ' saut', '\tsaut'],
    'T': [' coupe ', ' patinette', ' roule ', ' trott'],
    'V': [' bic', ' pédal', ' roul', ' vélib', ' vélo']
}
F_path_dict = {
    'D': ['desc', ' déval'],
    'E': [' aller', ' arriv', ' couloir ', 'entr', ' franchi', ' pass', 'sort', ' travers', ' va '],
    'L': ['allée ', ' avanc', ' long', ' parc '],
    'M': [' escalad', ' gravit', ' grimp', ' mont', ' remont'],
    'S': [' chang', 'entr', ' franchi', ' pass', "s'en va", 'sort'],
    'T': ['allée ', "d'un jardin à l'autre", ' pass', ' trav', '\ttrav', ' va ']
}
F_distr_dict = {
    'B1-2D_MS': ['dans une tasse',' poudre ', ' sel ', ' sucre'],
    'B1-8D_MS': ['dans une boîte',' panier', ' pannier', ' sac', ' voiture'],
    'B2-2D_ML': [' brique', ' duplo', ' légo'],
    'B2-8D_MT': [' anneau', ' cerceau', ' cercle', ' embo', 'empile', ' rond'],
    'B3-2D_PT': [' locomotive', ' train', ' wagon'],
    'B3-8D_MC': [' bouchon', ' casserole', ' couvercle', ' dinette', 'ferme', ' tasse']
}

corrected_dict = {
    'Ead02_DC2_cod.cha': ['B3-6_VL;07:55'],
    'Ead03_DC2_cod.cha': ['B1-5C_SL;01:59', 'B3-4_ST;06:57', 'B3-4_ST;07:03'],
    'Ead05_DC1_cod.cha': ['B1-3_RL;00:55'],
    'Ead13_DC1_cod.cha': ['B1-3_RL;00:51'],
    'Ead17_DC2_cod.cha': ['B2-1_CL;03:49'],
    'Ead18_DC2_cod.cha': ['B2-1_CL;04:15'],
    'Ech01_A1_DC1_cod.cha': ['B2-9_MS;03:04', 'B2-6_TT;03:16', 'B2-8D_MT;04:01', 'B2-11C_TM;04:35'],
    'Fch02_A1_DC1_cod.cha': ['B2-7_SD;05:29'],
    'Ech01_A2_DC3_cod.cha': ['G1-1_TE;00:06', 'G1-4_RL;01:02'],
    'Fch13_A1_DC3_cod.cha': ['G3-7_MM;01:05']
}
missed_dict = {

}
producedlater_dict = {
    'Ead16_DC1_cod.cha': ['B1-5C_SL;02:24'],
}
unintelligible_dict = {
    'Ech01_A1_DC1_cod.cha': 'B1-12_MS;01:41',
}
