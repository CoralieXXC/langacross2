#!/usr/bin/env python
# -*- coding: utf-8 -*-
# To avoid Pylint Message "Instance of 'PurePath' has no 'glob' member"
# for 'Path([...]).glob()'
# http://pylint-messages.wikidot.com/messages:e1101
# pylint: disable=no-member
# https://docs.python.org/3.6/library/pathlib.html#pathlib.Path.glob

from pathlib import Path
import pandas as pd
from scipy.io.arff import loadarff

from config import base_dir, expe_task, filter_dict

def list_files(in_folder, import_extension, apply_filter):
    """List the .tsv, .xlsx or .arff files to process after checking
            arguments and assigning default filter values

    Args:
        in_folder (str): folder where the data files are stored.
        import_extension (str): '.tsv', '.xlsx' or '.arff'.
        apply_filter (bool): True to process only
                the files of the specified expe/task.

    Raises:
        SystemExit: [description]
        SystemExit: [description]
        SystemExit: [description]
        SystemExit: [description]

    Returns:
        filenames (list): list of '.tsv', '.xlsx' or '.arff' files
    """
    # Check extension
    if (import_extension != '.tsv') and (import_extension != '.xlsx') \
            and (import_extension != '.arff'):
        raise SystemExit("Please specify a valid extension: '.tsv', "
                + "'.xlsx' or '.arff'.")

    # If no filter, list all the .tsv, .xlsx or .arff files
    if not apply_filter:
        filenames = [f for f in base_dir.joinpath('data', 'eye-tracking', in_folder)
                .glob('*/*{}/*{}'.format(('/*' if import_extension == '.arff' \
                        else ''), import_extension))]
    # If filter
    elif apply_filter:
        # Check arguments and assign default values
        if (expe_task['include_expe'] != 'Cat (DC1-DC2)') \
                and (expe_task['include_expe'] != 'Mem (DC3-DC4)'):
            raise SystemExit("Please specify a valid 'include_expe': "
                    + "'Cat (DC1-DC2)' or 'Mem (DC3-DC4)'.")
        if expe_task['include_task']:
            if (expe_task['include_expe'] == 'Cat (DC1-DC2)') \
                    and (expe_task['include_task'] != 'Cat') \
                    and (expe_task['include_task'] != 'Prod'):
                raise SystemExit("Please specify a valid 'include_task' "
                        + "for 'Cat (DC1-DC2)': 'Cat' or 'Prod'.")
            if (expe_task['include_expe'] == 'Mem (DC3-DC4)') \
                    and (expe_task['include_task'] != 'Mem') \
                    and (expe_task['include_task'] != 'Prod') \
                    and (expe_task['include_task'] != 'View'):
                raise SystemExit("Please specify a valid 'include_task' "
                        + "for 'Mem (DC3-DC4)': 'Mem' or 'Prod' or 'View'.")
        if expe_task['include_task'] == 'Mem':
            filter_task = 'Phase2'
        elif expe_task['include_task'] == 'View':
            filter_task = 'NVMem'
        else:
            filter_task = expe_task['include_task']

        # List the .tsv, .xlsx or .arff files from the specified expe/task
        filenames = [f for f in base_dir.joinpath('data', 'eye-tracking', in_folder)
                .glob('{}/*{}*{}/*{}'
                .format(expe_task['include_expe'], filter_task,
                ('/*' if import_extension == '.arff' else ''),
                import_extension))]
    
    return filenames

def filter_data(participant_name, media_name=''):
    """Define if the data are going to be processed or not

    Args:
        participant_name (str): name of the participant
        media_name (str, optional): name of the media.
                Defaults to ''.

    Raises:
        SystemExit: [description]
        SystemExit: [description]
        SystemExit: [description]
        SystemExit: [description]

    Returns:
        process_data (bool): True if the data are to be processed
    """
    # If status is not set, process the data
    if (not 'status' in filter_dict) \
            or ('status' in filter_dict) and (filter_dict['status'] == ''):
        process_data = True
    # If status is set:
    else:
        # Check filter_dict
        if ('status' in filter_dict) and (len(filter_dict['status']) != 0) \
                and (filter_dict['status'] != 'exclude' \
                and filter_dict['status'] != 'include'):
            raise SystemExit("Please specify a valid 'status' in filter_dict: "
                    + "'' or 'exclude' or 'include'.")
        if ('language' in filter_dict) and (len(filter_dict['language']) \
                != 0) and (filter_dict['language'] != 'english' \
                and filter_dict['language'] != 'french'):
            raise SystemExit("Please specify a valid 'language' in "
                    + "filter_dict: '' or 'english' or 'french'.")
        if ('age' in filter_dict) and (len(filter_dict['age']) != 0) \
                and (filter_dict['age'] != 'adults' \
                and filter_dict['age'] != 'children'):
            raise SystemExit("Please specify a valid 'age' in filter_dict: "
                    + "'adults' or 'children'.")
        if ('condition' in filter_dict) and (len(filter_dict['condition']) \
                != 0) and (filter_dict['condition'] != 'NonVerbal' \
                and filter_dict['status'] != 'Verbal'):
            raise SystemExit("Please specify a valid 'condition' in "
                    + "filter_dict: 'NonVerbal' or 'Verbal'.")
        # Assign condition
        if ('condition' in filter_dict) and (filter_dict['condition'] \
                == 'NonVerbal'):
            condition = 1
            # NonVerbal condition finishes with an odd number (DC1 or DC3)
        elif ('condition' in filter_dict) and (filter_dict['condition'] \
                == 'Verbal'):
            condition = 0
            # Verbal condition finishes with an even number (DC2 or DC4)

        # Load in a DataFrame the .tsv file listing Participants and Media
        # to exclude or include
        if ('file_list' in filter_dict) \
                and (len(filter_dict['file_list']) != 0):
            file_list = base_dir.joinpath('data', 'eye-tracking', filter_dict['file_list'])
            try:
                with open(file_list) as tsvfile:
                    df_files = pd.read_csv(tsvfile, sep='\t', header=None)
            except IOError as strerror:
                print("I/O error: {0}".format(strerror))
        # Or load in a DataFrame filter_dict['participant_list']
        # and filter_dict['media_list']
        elif len(filter_dict['participant_list']) != 0 \
                or len(filter_dict['media_list']) != 0:
            df_files = pd.DataFrame(filter_dict['participant_list']
                    + filter_dict['media_list'])
        # Or create an empty DataFrame
        else:
            df_files = pd.DataFrame(columns=[])

        # Assess 'participant_name' against the filter_dict
        if ('language' not in filter_dict or filter_dict['language'] == ''\
                or participant_name[0] == filter_dict['language'][0].upper()) \
                and ('age' not in filter_dict or filter_dict['age'] == ''\
                or participant_name[1:3] == filter_dict['age'][0:2]) \
                and ('condition' not in filter_dict
                or filter_dict['condition'] == '' \
                or (int(participant_name[-1]) % 2) == condition) \
                and (len(df_files) == 0 \
                or participant_name in df_files.values):
            # Assess the media_name (if any) and the filter_dict
            if media_name and ((len(df_files) == 0 \
                    or media_name in df_files.values)):
                _ = 1
                in_dict = True
            elif media_name:
                _ = 2
                in_dict = False
            elif not media_name:
                _ = 3
                in_dict = True
        # Assess 'media_name' against the filter_dict
        # TODO check (modified for plot_movie_data)
        elif (media_name in df_files.values):
            _ = 4
            in_dict = True
        else:
            _ = 5
            in_dict = False

        # Take the status into account
        if filter_dict['status'] == 'include':
            process_data = in_dict
        elif filter_dict['status'] == 'exclude':
            process_data = not in_dict

    return process_data

def timings2df(stimuli_timings='Stimuli_timings.tsv'):
    """Load the .tsv file containing event times by media in a DataFrame

    Args:
        stimuli_timings (str, optional): name of the .tsv file
                containing event times by media. Defaults to
                'Stimuli_timings.tsv'.

    Returns:
        df_timings (DataFrame): DataFrame containing event times by media
    """
    # Open the.tsv file containing event times by media
    tsv_timings = base_dir.joinpath('stimuli', stimuli_timings)

    try:
        with open(tsv_timings) as tsvfile:
            df_timings = pd.read_csv(tsvfile, sep='\t')
    except IOError as strerror:
        print("I/O error: {0}".format(strerror))

    return df_timings

def get_arff_commented_metadata(file, string):
    """Get the value stored in the commented @METADATA line containing
            'string' in the .arff 'file'

    Args:
        file (TextIOWrapper): .arff file
        string (str): string to look for in @METADATA lines

    Returns:
        value (str): value stored in the commented @METADATA line
    """
    for line in file:
        matched_line = [l for l in line if string in line]

        if (matched_line and (string == "width_px" or string \
                == "height_px" or string == "stim_order")) :
            value = float(''.join(matched_line).strip('\n').split(' ')[2])
        elif (matched_line and (string == "EYE_MOVEMENT_TYPE")):
            value = ''.join(matched_line).strip('\n').split(' ')[2]

    file.seek(0, 0)

    return value

def load_arff_sp_tool(arff_file, arff_type):
    """Load arff data to a pandas DataFrame, get media width, height,
			and stim order
    https://stackoverflow.com/questions/46401209/
    how-to-convert-the-arff-object-loaded-from-a-arff-file-into-a-dataframe-format

    Args:
        arff_file (string): .arff file containing raw or processed gaze data
        arff_type (string): 'raw' for unprocessed .arff files
                'processed_sp_tool' for processed .arff files

    Returns:
        df_data (DataFrame): DataFrame containing Tobii eye-tracking
                raw or classified data
        width_px (float): screen width in pixels
        height_px (float): screen height in pixels
        stim_order (int): order in which the stimuli has been displayed
                (from 1 to 12)
    """
    with open(arff_file) as f:
        # Get the width and height of the MEDIA (in px)
        width_px = get_arff_commented_metadata(f, 'width_px')
        height_px = get_arff_commented_metadata(f, 'height_px')
        stim_order = get_arff_commented_metadata(f, 'stim_order')

        # https://stackoverflow.com/questions/48573169/
        # how-to-import-csv-or-arff-to-scikit
        # Read the arff file as a dictionary with the data as a list of
        # lists at key 'data'
        raw_data = loadarff(f)
        df_data = pd.DataFrame(raw_data[0])

        # Make sure 'time' is an integer and not a float
        df_data['time'] = df_data['time'].astype(int)

        # Make sure 'EYE_MOVEMENT_TYPE' strings are properly read
        if arff_type == 'processed_sp_tool':
            df_data['EYE_MOVEMENT_TYPE'] = df_data['EYE_MOVEMENT_TYPE'] \
                    .str.decode('utf-8')

    return df_data, width_px, height_px, int(stim_order)

def Tobiitsv2df(filename):
    """Load the .tsv file containing Tobii eye-tracking data in a DataFrame

    Args:
        filename (Path): Path of the .tsv file

    Returns:
        df_data (DataFrame): eye-tracking data
    """
    # Load data from the current file
    dict_dtype = {'MediaName': str, 'MouseEvent': str,
            'StudioEvent': str, 'StudioEventData': str}

    with open(filename, "r", encoding="utf8") as current_file:
        df_data = pd.read_csv(filename, sep='\t', header=0,
                encoding='UTF-8', dtype=dict_dtype)
        current_file.close() # TODO check if necessary

    return df_data