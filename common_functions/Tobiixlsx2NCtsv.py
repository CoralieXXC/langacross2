#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import pandas as pd

from common_functions import load_files as lf

def Tobiixlsx2NCtsv(in_folder='0a_Exports_Tobiixlsx',
                    out_folder='0b_Converted_Tobiixlsx2NCtsv',
                    apply_filter=False):
    """Converts .xlsx files to .tsv non-compliant files
            containing raw gaze data from Tobii exports

    Args:
        in_folder (str, optional): subfolder named after the step.
                Defaults to '0a_Exports_Tobiixlsx'.
        out_folder (str, optional): subfolder named after the next step.
                Defaults to '0b_Converted_Tobiixlsx2NCtsv'.
        apply_filter (bool, optional): True to process only
                the files of the specified expe/task.
                Defaults to False.
    """
    # List the .tsv or .xlsx files to process
    filenames = lf.list_files(in_folder, '.xlsx', apply_filter)

    # Browse the files listed in filenames
    for filename in filenames:
        # Create folders to store resulting .tsv files
        # https://stackoverflow.com/questions/273192/
        # how-can-i-safely-create-a-nested-directory
        # Use replace() trick to fix lower case pathlib/Windows bug
        # https://stackoverflow.com/questions/56873491/
        # why-does-path-rglob-returns-filenames-in-lower-case-if-whole-name
        # -is-specified
        out_path = filename.parents[3].joinpath(out_folder,
                str(filename.parts[-3]).replace('cat (dc1-dc2)',
                'Cat (DC1-DC2)').replace('mem (dc3-dc4)', 'Mem (DC3-DC4)'),
                str(filename.parts[-2]))
        out_path.mkdir(parents=True, exist_ok=True)

        # Load .xlsx to DataFrame and save to .tsv
        data_xlsx = pd.read_excel(filename)
        data_xlsx.to_csv(out_path.joinpath(filename.stem + '.tsv'),
                sep='\t', encoding='utf-8', index=False)
