#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
from datetime import datetime

def df2arff(out_folder, arff_stem, correction_x, correction_y, df_data, column_list,
    header_val, index_val, mode_val, stim_order):
    """Save a DataFrame to a .arff file containing raw or processed gaze data

    Args:
        out_folder (str): path where the resulting .arff file is stored
        arff_stem (str): resulting .arff file stem (filename without extension)
        correction_x (int): correction on the x axis if any
        correction_y (int): correction on the y axis if any
        df_data (DataFrame): eye-tracking data
        column_list (list of str): list of the colums to export
        header_val (bool): True if header is exported
        index_val (bool): True if index is exported
        mode_val (str): Python write mode
        stim_order (int): stimulus number; 0 if no order
    """

    arff_fullpath = Path(out_folder).joinpath(arff_stem + '.arff')

    with open(arff_fullpath, 'w') as out_file:
        out_file.write("%@METADATA width_px 1280.0\n"
                       + "%@METADATA height_px 720.0\n"
                       + "%@METADATA distance_mm 600.0\n"
                       + "%@METADATA width_mm 408.0\n"
                       + "%@METADATA height_mm 230.0\n"
                       + "%@METADATA filename " + str(arff_fullpath) + "\n")
        if (correction_x != 0 or correction_y != 0):
            out_file.write("%@METADATA correctionx_px " + str(correction_x) + "\n"
                       + "%@METADATA correctiony_px " + str(correction_y) + "\n")
        if (stim_order != 0):
            out_file.write("%@METADATA stim_order " + str(int(stim_order)) + "\n")
        out_file.write("@RELATION gaze_recording\n"
                       + "\n"
                       + "@ATTRIBUTE time INTEGER\n"
                       + "@ATTRIBUTE x NUMERIC\n"
                       + "@ATTRIBUTE y NUMERIC\n"
                       + "@ATTRIBUTE confidence NUMERIC\n"
                       + "\n"
                       + "@DATA\n")
    out_file.close()

    df_data.to_csv(arff_fullpath, columns=column_list, sep=",", encoding='utf-8',
                   header=header_val, index=index_val, mode=mode_val)

def save_log(in_filename, out_folder, script_start, to_run=''):
    """Save an uncommented .log file of a .py file in 'logs'
            Used to save a timestamped .log file of the useful content
            of config.py or run_process.py

    Args:
        in_filename (str): name of the .py script to save as a log file
        out_folder (str): folder where the resulting .log file is stored
        timestamp (str): timestamp of the .log file
        to_run (str, optional): running script acronym used to save only
                the relevant lines of run_process.py
        start_time (datetime, optional): start time of the execution
    """
    flag = False

    # https://stackoverflow.com/questions/1706198/
    # python-how-to-ignore-comment-lines-when-reading-in-a-file
    with open(Path.cwd().joinpath(in_filename + '.py'), 'r') as read_config, \
            open(out_folder.joinpath(script_start.strftime('%Y-%m-%d_%H%M%S') \
            + '_' + in_filename + ('_' + to_run if to_run else '') \
            + '.log'), 'a') as save_file:
        for line in read_config:
            line = line.partition('#')[0]
            line = line.rstrip()
            if line != '':
                # Save all lines for config.py
                if to_run == '':
                    print(line, file=save_file)
                # Only keep relevant lines for run_process.py
                if line.startswith('def'):
                    print(line, file=save_file)
                elif to_run in line and (line.startswith('    if to_run') \
                        or line.startswith('    elif to_run')):
                    flag = True
                    print(line.replace('    elif', '    if'), file=save_file)
                elif flag and not (line.startswith('    if') \
                        or line.startswith('    elif')
                        or line.startswith('    else')):
                    print(line, file=save_file)
                elif flag and (line.startswith('    if') \
                        or line.startswith('    elif')):
                    flag = False
                elif not flag and (line.startswith('run_process(')):
                    print(line, file=save_file)
                    flag = True
                elif flag and (line.startswith('})')):
                    print(line, file=save_file)
        if in_filename == 'run_process':
            print('\nExecution time of ' + to_run + ': ' + str(datetime.now() \
                    - script_start), file=save_file)
