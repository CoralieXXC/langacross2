#!/usr/bin/env python
# -*- coding: utf-8 -*-

from config import out_logs_path, timestamp, print_logs, save_logs

def get_import_types(filenames):
    """Get import_type and import_subtype values (based on Tobii Studio
            Data Export).

    Args:
        filenames (list of Path): list of .arff or .tsv files

    Raises:
        SystemExit: files to process are neither '.arff' nor '.tsv'
        SystemExit: filenames don't begin with the specified strings

    Returns:
        import_type (str): 'MediaName' or 'RecordingName'.
                Refers to the option used in Tobii Studio Data Export.
                'RecordingName' (namely 'Select Full Recordings for
                export'):
                    .tsv: data are in 'RecordingName.tsv' files
                    which contain all media for each participant.
                    .arff: data are in 'RecordingName/MediaName.arff'
                'MediaName' (namely 'Select Media for export'):
                    .tsv: data are in 'MediaName.tsv' files which
                    contain all participants for each media.
                    .arff: data are in 'MediaName/RecordingName.arff'.
                Defaults to 'RecordingName'.
        import_subtype (str): 'RecordingName' or 'MediaName'.
    """
    # https://stackoverflow.com/questions/10666163/
    # how-to-check-if-all-elements-of-a-list-matches-a-condition
    if all(filename.suffix == '.arff' for filename in filenames):
        name_parts = [filename.parent.name for filename in filenames]
    elif all(filename.suffix == '.tsv' for filename in filenames):
        name_parts = [filename.stem for filename in filenames]
    else:
        raise SystemExit("Error. Files to process must be '.arff' or '.tsv'.")
    # http://xion.io/post/code/python-startswith-tuple.html
    if all(name_part.startswith(('Ead', 'Ech', 'Fad', 'Fch')) \
            for name_part in name_parts):
        import_type = 'RecordingName'
        import_subtype = 'MediaName'
    elif all(name_part.startswith(('NVCat_', 'P_',
            'NVMem_', 'ProdMem_', 'Mem_')) for name_part in name_parts):
        import_type = 'MediaName'
        import_subtype = 'RecordingName'
    else:
        raise SystemExit("Error. All files must begin with " \
                + "'Ead', 'Ech', 'Fad', 'Fch' " \
                + "for 'RecordingName' " \
                + "or 'NVCat_', 'P_', 'NVMem_', 'ProdMem_', 'Mem_' " \
                + "for 'MediaName'.")
    return import_type, import_subtype

def get_export_type(import_type, export_type=''):
    """Check and assign export_type value.

    Args:
        import_type (str): 'MediaName' or 'RecordingName'
                ('RecordingName' is the participant name).
        export_type (str, optional): 'MediaName' or 'RecordingName'
                ('RecordingName' is the participant name).
                Defaults to ''.

    Raises:
        SystemExit: [description]

    Returns:
        str: export_type
    """
    if not export_type:
        export_type = import_type
    if (export_type != 'MediaName') and (export_type != 'RecordingName'):
        raise SystemExit("Please specify a valid export name: 'MediaName' "
                + "or 'RecordingName'.")
    return export_type

# TODO check is get_arff and get_tsv can be merged
def get_arff_participant_media(import_type, filename):
    """Get media and participant names

    Args:
        import_type (str): 'MediaName' or 'RecordingName'
                ('RecordingName' is the participant name).
        filename (str): name of the file

    Returns:
        participant_name (str): name of the participant
        media_name (str): name of the media
    """
    if import_type == 'MediaName':
        media_name = filename.parent.name
        participant_name = filename.stem
    elif import_type == 'RecordingName':
        media_name = filename.stem
        participant_name = filename.parent.name
    return participant_name, media_name

def get_tsv_participant_media(import_type, filename, subname, data):
    """Get media and participant names

    Args:
        import_type (str): 'MediaName' or 'RecordingName'
                ('RecordingName' is the participant name).
        filename (str): [description]
        subname (str): [description]
        data (DataFrame): [description]

    Returns:
        participant_name (str): name of the participant
        media_name (str): name of the media
    """
    if import_type == 'MediaName':
        # TODO check
        media_name = filename.stem.replace('.avi', '')
        participant_name = data['RecordingName'].iloc[0]
    elif import_type == 'RecordingName':
        media_name = subname.replace('.avi', '')
        participant_name = data['RecordingName'].iloc[0]
    return participant_name, media_name

def label_fixcross(df, timings, media_name):
    """Label cluster's estimation before/after fixation cross end|action
            start
    
    Args:
        df (DataFrame): data from .arff files.
        timings (DataFrame): events times by media.
        media_name (str): name of media.
    
    Returns:
        DataFrame: df data with cluster's estimation labelled
    """

    # Get the end of the fixation cross for the target
    # and convert it to microseconds
    target_action_start = int(
        timings.loc[timings['MovieName'] == media_name] \
                .iloc[0]['TargetActionStart'])*1000
    # https://stackoverflow.com/questions/17071871/
    # select-rows-from-a-dataframe-based-on-values-in-a-column-in-pandas
    df_fixcross = df.loc[(df['time'] >= target_action_start + 100000) & (
            df['time'] <= target_action_start + 400000) & df[
            'EYE_MOVEMENT_TYPE'].isin(['SACCADE']) & (df['x_diff'] <= -50)]
    if len(df_fixcross) == 1:
        time_endfixcross = df_fixcross.iloc[0]['time']
        # https://stackoverflow.com/questions/33769860/
        # pandas-apply-but-only-for-rows-where-a-condition-is-met
        df['fixcross'] = df.apply(lambda row: 'action' if row['time'] \
                > time_endfixcross else 'cross', axis=1)
    else:
        df['fixcross'] = df.apply(lambda row: 'action' if row['time'] \
                > target_action_start + 400000 else 'check', axis=1)
    return df

def print_save_log(message, file_stem, print_logs=True, save_logs=False):
    """Print log message to stdout and optionally to .log file

    Args:
        message (str): output message.
        file_stem (str): output filename.
        print_logs (bool, optional): True to print message to stdout.
        save_logs (bool, optional): True to save message to file.
                Defaults to False.
    """
    if print_logs:
        print(message)
    # https://stackoverflow.com/questions/7152762/
    # how-to-redirect-print-output-to-a-file-using-python
    if save_logs:
        with open(out_logs_path.joinpath(timestamp 
                + '_' + file_stem 
                + '.log'), 'a') as f:
            print(message, file=f)

def keep_interval_data(data, media_name, df_timings,
                       start_name, end_name, strictness='strict', phase=''):
    """Keep data between start and end limits and return start event time

    Args:
        data (DataFrame): eye-tracking data.
        media_name (str): name of media.
        df_timings (DataFrame): event times by media.
        start_name (str): name of the start event.
        end_name (str): name of the end event.
        strictness (str, optional): strictness of the interval. Defaults
                to strict (to not keep the limits).
        phase (str, optional): to not take into accound MouseClicks
                during Prod. May be '' or 'Prod'. Defaults to ''.

    Returns:
        interval_data (DataFrame): data between start and end limits
        start_time (int64): start event time
        mouse_timestamp (None or int64): mouse timestamp
    """
    # Filter data where 'MediaName' contains media name
    # https://stackoverflow.com/questions/60639531/
    # a-value-is-trying-to-be-set-on-a-copy-of-a-slice-from-a-dataframe-using-pandas
    data_part = data[data['MediaName'] == media_name].copy()

    movie_start_time = data_part[data_part['StudioEvent'] \
            == 'MovieStart'].iloc[0]['RecordingTimestamp']

    # Define limits of the time interval
    start_time = movie_start_time \
            + int(df_timings.loc[df_timings['MovieName'] \
            == media_name].iloc[0][start_name])
    if end_name == 'MovieEnd':
        end_time = data_part[data_part['StudioEvent'] == end_name] \
                .iloc[0]['RecordingTimestamp']
    else:
        end_time = min(movie_start_time \
                + int(df_timings.loc[df_timings['MovieName'] \
                == media_name].iloc[0][end_name]), \
                data_part[data_part['StudioEvent'] == 'MovieEnd'] \
                .iloc[0]['RecordingTimestamp'])

    mouseleft_timestamp = data_part[data_part['MouseEvent'] == 'Left']
    mouseright_timestamp = data_part[data_part['MouseEvent'] == 'Right']
    # If there is a left click
    if not mouseleft_timestamp.empty:
        mouse_timestamp = mouseleft_timestamp \
                .iloc[0]['RecordingTimestamp']
    # If there is a right click
    elif not mouseright_timestamp.empty:
        mouse_timestamp = mouseright_timestamp \
                .iloc[0]['RecordingTimestamp']
    # If there is no click
    else:
        mouse_timestamp = None

    # https://stackoverflow.com/questions/21372228/
    # minimum-of-numbers-that-are-not-none-in-python
    # If there is a click
    if mouse_timestamp is not None and phase != 'Prod':
        start_interval = min(mouse_timestamp, start_time)
        end_interval = min(mouse_timestamp, end_time)
        # Get rid off duplicates for end_interval timestamp (keep mouse click
        # and 'MovieEnd')
        # https://stackoverflow.com/questions/50264673/
        # pandas-drop-duplicates-with-multiple-conditions
        data_part = data_part[~data_part.duplicated(['RecordingTimestamp'], keep=False)
                | data_part['MouseEvent'].ne('0')
                | data_part['StudioEvent'].ne('0')]
    else:
        start_interval = start_time
        end_interval = end_time

    # Get rid off duplicates for end_interval timestamp
    # https://stackoverflow.com/questions/60294634/
    # how-to-select-the-first-row-when-there-are-multiple-rows-with-repeated-values-in
    data_part.drop_duplicates(subset=['RecordingTimestamp'],
            keep='first', inplace=True)

    # Keep samples of the time interval
    # If the interval is strict:
    if strictness == 'strict':
        interval_data = data_part[(data_part['RecordingTimestamp'] \
                > start_interval) & (data_part['RecordingTimestamp'] \
                < end_interval)]
    # if the interval is not strict:
    else:
        interval_data = data_part[(data_part['RecordingTimestamp'] \
                >= start_interval) & (data_part['RecordingTimestamp'] \
                <= end_interval)]
    return interval_data, start_time, mouse_timestamp
