#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import pandas as pd

from config import print_logs, save_logs, out_logs_path, timestamp
from common_functions.common_functions import get_import_types, \
        get_tsv_participant_media, print_save_log
from common_functions.load_files import list_files

def check_tsv(data, filename, import_type='RecordingName', \
        import_subtype='MediaName'):
    """Print error and info messages to stdout and optionally to .log file

    Args:
        data (DataFrame): [description]
        filename (str): [description]
        import_type (str, optional): [description]. Defaults to 'RecordingName'.
        import_subtypes (str, optional): [description]. Defaults to 'MediaName'.

    Raises:
        SystemExit: [description]
    """
    # TODO finish docstring and comments
    # Check if the following necessary columns exist:
    columns = ['ParticipantName', 'RecordingName', 'MediaName',
               'RecordingTimestamp', 'StudioEvent', 'GazePointX (MCSpx)',
               'GazePointY (MCSpx)', 'ValidityLeft', 'ValidityRight']
    if not set(columns).issubset(data.columns):
        raise SystemExit("The file '{}' does not contain at least " \
                .format(filename) + "one of the necessary columns:\n{}." \
                .format(columns))

    # Define elements to check film starts and film ends
    log_fullpath = out_logs_path.joinpath(timestamp + '_'
            + Path(__file__).stem + '.log')
    movie_start_id = data.loc[data['StudioEvent']
            == 'MovieStart', 'RecordingTimestamp']
    movie_end_id = data.loc[data['StudioEvent']
            == 'MovieEnd', 'RecordingTimestamp']
    # Check that there is the expected number of stimuli
    if filename.parts[-3] == 'Cat (DC1-DC2)':
        movie_nb = 12
    elif filename.parts[-3] == 'Mem (DC3-DC4)':
        movie_nb = 10
    if len(movie_start_id.index) != movie_nb:
        error_movie_nb = "Error for '{}'.\n" \
                .format(filename) \
                + "There are not " + str(movie_nb) \
                + " stimuli there are {}." \
                .format(len(movie_start_id.index))
        print_save_log(error_movie_nb, Path(__file__).stem, \
                print_logs, save_logs)
    # Check that there are as many film starts as film ends
    if len(movie_start_id.index) != len(movie_end_id.index):
        error_movies = "Error for '{}'.\n" \
                .format(filename) \
                + "One or more film start or end indicators " \
                + "are missing.\n" \
                + "There are {} film starts and {} film ends.".format(
                len(movie_start_id.index),len(movie_end_id.index))
        print_save_log(error_movies, Path(__file__).stem, \
                print_logs, save_logs)

    # Get subnames: media names (for 'RecordingName')
    # or participant names (for 'MediaName')
    subnames = data.loc[data['StudioEvent'] == 'MovieStart', import_subtype]

    for subname in subnames:
        # Define media and participant names
        participant_name, media_name = get_tsv_participant_media(
                import_type, filename, subname, data)

        # Filter data where column import_subtype contains subname
        data_part = data[data[import_subtype] == subname]

        # Initialize the temporal variables of the film start
        # and film end
        movie_start_id_part = data_part.loc[
                data_part['StudioEvent'] == 'MovieStart',
                'RecordingTimestamp']
        movie_end_id_part = data_part.loc[
                data_part['StudioEvent'] == 'MovieEnd',
                'RecordingTimestamp']

        # Check that there is one movie start and one movie end
        if len(movie_start_id_part) != 1 \
                or len(movie_end_id_part) != 1:
            # https://stackoverflow.com/questions/82831/
            # how-do-i-check-whether-a-file-exists-without-exceptions
            if not log_fullpath.is_file():
                error_movie = "'{}' '{}' error. " \
                        .format(participant_name, media_name) \
                        + "There is zero or more than one " \
                        + "film start or end indicator."                                
                print_save_log(error_movie, Path(__file__).stem, \
                        print_logs, save_logs)
            elif log_fullpath.is_file():
                with open(log_fullpath) as f:
                    # First run
                    # https://stackoverflow.com/questions/4940032/
                    # how-to-search-for-a-string-in-text-files
                    if not "'{}' '{}' error. " \
                            .format(participant_name, media_name) in f.read():
                        error_movie = "'{}' '{}' error. " \
                                .format(participant_name, media_name) \
                                + "There is zero or more than one " \
                                + "film start or end indicator."
                        print_save_log(error_movie, Path(__file__).stem, \
                                print_logs, save_logs)
                    # Second run (after correction)
                    else:
                        # Write comment
                        not_corrected_movie = "'{}' '{}' not corrected. " \
                                .format(participant_name, media_name) \
                                + "There is still zero or more than one " \
                                + "film start or end indicator."
                        print_save_log(not_corrected_movie, Path(__file__)
                                .stem, print_logs, save_logs)
        elif log_fullpath.is_file():
            with open(log_fullpath) as f:
                if "'{}' '{}' error. " \
                            .format(participant_name, media_name) in f.read():
                    # Write comment
                    corrected_movie = "'{}' '{}' corrected. " \
                            .format(participant_name, media_name) \
                            + "There is one film start and end indicator."
                    print_save_log(corrected_movie, Path(__file__).stem, \
                            print_logs, save_logs)

def correct_noncompliant_tsv(data, filename,
                             out_folder='0c_Compliant_Tobiitsv'):
    """Correct non-compliant data (missing 'MediaName')
            and create a .tsv file containing the corrected data

    Args:
        data (DataFrame): [description]
        filename (str): [description]
        out_folder (str, optional): [description]. Defaults to '0c_Compliant_Tobiitsv'.

    Returns:
        data (DataFrame): [description]
    """
    # TODO finish docstring and comments
    # Insert missing 'MediaName' at 'MovieEnd' based on 'StudioEventData'
    data['MediaName'] = data.apply(lambda row: row['StudioEventData'] \
            # https://stackoverflow.com/questions/35232705/
            # how-to-test-for-nans-in-an-apply-function-in-pandas
            if (pd.isnull(row['MediaName']) \
                and (row['StudioEvent'] == 'MovieEnd')) \
            else row['MediaName'], axis=1)
    # Replace the np.nan values between the rows containing the same 'MediaName'
    # See 'Ead01_DC1' 'NVCat_B3-8D_MCMTEC' for example
    # https://stackoverflow.com/questions/50596208/
    # conditional-forward-fill-between-integer-values
    media_ffill = data['MediaName'].ffill()
    data['MediaName'] = media_ffill.mask(media_ffill != data['MediaName'].bfill())

    # Create folders to store the resulting .tsv files
    out_path = filename.parents[3].joinpath(out_folder,
            str(filename.parts[-3]).replace('cat (dc1-dc2)',
            'Cat (DC1-DC2)').replace('mem (dc3-dc4)', 'Mem (DC3-DC4)'),
            str(filename.parts[-2]))
    out_path.mkdir(parents=True, exist_ok=True)
    # Write dataframe to .tsv file
    data.to_csv(out_path.joinpath(filename.stem + '.tsv'),
            sep="\t", encoding='utf-8', index=False)

    return data

def TobiiNCtsv2tsv(in_folder='0b_Converted_Tobiixlsx2NCtsv',
                   out_folder='0c_Compliant_Tobiitsv', apply_filter=False):
    """Convert Tobii non-compliant .tsv files to compliant .tsv files

    Args:
        in_folder (str, optional): [description]. Defaults to '0b_Converted_Tobiixlsx2NCtsv'.
        out_folder (str, optional): [description]. Defaults to '0c_Compliant_Tobiitsv'.
        apply_filter (bool, optional): [description]. Defaults to False.
    """
    # TODO finish docstring and comments
    # List the .tsv or .xlsx files to process
    filenames = list_files(in_folder, '.tsv', apply_filter)

    # Get import_type and import_subtype
    import_type, import_subtype = get_import_types(filenames)

    # Browse the files listed in filenames
    for filename in filenames:
        # Load data from the .tsv file
        # https://stackoverflow.com/questions/24251219/
        # pandas-read-csv-low-memory-and-dtype-options
        dict_dtype = {'MediaName': str, 'MouseEvent': str, 'StudioEvent': str,
                'StudioEventData': str}
        with open(filename, 'r'):
            data = pd.read_csv(filename, sep='\t', header=0, encoding='UTF-8',
                               dtype=dict_dtype)

        check_tsv(data, filename, import_type, import_subtype)

        corrected_data = correct_noncompliant_tsv(data, filename, out_folder)

        check_tsv(corrected_data, filename)
