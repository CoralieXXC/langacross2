# Langacross_2

These Python scripts and functions are intended to process, visualize and analyze data of the [Langacross_2 project](http://www.sfl.cnrs.fr/langacross2) (French description).

If you use this code or a modified version of it, please cite:

    @mastersthesis{vincent_oculometrie_2019,
        address = {Paris, France,
        title = {L'utilisation de l'oculom{\'e}trie en psycholinguistique : le cas de la cognition spatiale},
        school = {Conservatoire National des Arts et M{'e}tiers},
        author = {Vincent, Coralie},
        year = {2019},
        howpublished={\url{https://www.sfl.cnrs.fr/sites/default/files/images/memoirecnam_vincent.pdf}}
    }

## Contents

* Production
* Response_time_value
* Eye_tracking

## Authors

* **Coralie Vincent** - *Initial work* - [Webpage](http://www.sfl.cnrs.fr/coralie-vincent)

## License

This project is licensed under the MIT License

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration: based on [PyGazeAnalyser](https://github.com/esdalmaijer/PyGazeAnalyser). Other online sources are most often indicated in the code


